<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<link href="<c:url value="/resources/profile.css" />" rel="stylesheet" type="text/css">
</head>
<body>
<div class="profile-page">
  <div class="form">
    <form:form class="login-form" method="POST" modelAttribute="app" action="profile">
    	<h1>PROFILE CREATION</h1>
    	<table>
    	<tr><td class="t">Email Address: ${ app.email }</td></tr>
    	<tr><td><form:hidden path="email" value="${ app.email }"/></td></tr>
    	<tr><td>&nbsp;</td></tr>
    	<tr><td class="t">First Name:</td></tr>
    	<tr><td><form:input path="f_name" placeholder="First Name" /></td></tr>
    	<tr><td class="t">Last Name:</td></tr>
    	<tr><td><form:input path="l_name" placeholder="Last Name" /></td></tr>
    	<tr><td class="t">Address:</td></tr>
    	<tr><td><form:input path="address" placeholder="Home Address" /></td></tr>
		<tr><td class="t">Current Position:</td></tr>
    	<tr><td><form:input path="current_position" placeholder="Current Position" /></td></tr>
    	<tr><td class="t">Education:</td></tr>
    	<tr><td><form:textarea path="education" placeholder="Education"/></td></tr>
    	<tr><td class="t">Experience:</td></tr>
    	<tr><td><form:textarea name="styled-textarea" path="experience" placeholder="Experience" /></td></tr>
    	</table><br>
    	<button>create</button>
    </form:form>
  </div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-2.2.3.js"   integrity="sha256-laXWtGydpwqJ8JA+X9x2miwmaiKhn8tVmOVEigRNtP4="   crossorigin="anonymous"></script>
</body>

</html>
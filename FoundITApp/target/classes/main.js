/**
 * 
 */
$(document).ready(function(){
	$("#SlideEditProfile").hide();

	$("#EditClick").click(function() {
		$('#YourProfile').slideUp();
		$("#SlideEditProfile").slideDown();
	});
	$('#SaveClick').click(function() {
		$('#YourProfile').slideDown();
		$('#SlideEditProfile').slideUp();
	});
	$('#jobPosting').DataTable({
		"lengthMenu": [10]
	});
    $('#hireTeam').DataTable({
    	"paging":   false,
        "ordering": false,
        "info":     false
    });
    $('#reviewProgress').DataTable({
    	"paging": false
    });
});
$('#showDescriptionModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget) // Button that triggered the modal
	var description = button.data('details')
	var modal = $(this)
	modal.find('.e').text(description)
});
$('#deleteConfirmationModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget) // Button that triggered the modal
	var username = button.data('username')
	var id = button.data('id')
	var modal = $(this)
	modal.find('.e').text(username)
	modal.find('.modal-footer input[name=reviewerID]').val(id)
});
$('#showSkillsModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget) // Button that triggered the modal
	var skills = button.data('skills')
	var modal = $(this)
	modal.find('.e').text(skills)
});
$('#saveConfirmationModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget) // Button that triggered the modal
	var id = button.data('id')
	var modal = $(this)
	modal.find('.modal-footer input[name=jobID]').val(id)
});
$('#applyConfirmationModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget) // Button that triggered the modal
	var title = button.data('title')
	var id = button.data('id')
	var modal = $(this)
	modal.find('.e').text(title)
	modal.find('.modal-footer input[name=jobID]').val(id)
});
$('#deleteSavedPostingModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget) // Button that triggered the modal
	var title = button.data('title')
	var jobID = button.data('id')
	var modal = $(this)
	modal.find('.e').text(title)
	modal.find('.modal-footer input[name=jobID]').val(jobID)
});
$('#editEmployerJobModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget) // Button that triggered the modal
	var title = button.data('title')
	var jobID = button.data('id')
	var modal = $(this)
	modal.find('.e').text(title)
	modal.find('.modal-footer input[name=jobID]').val(jobID)
});
$('#deleteEmployerJobModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget) // Button that triggered the modal
	var title = button.data('title')
	var jobID = button.data('id')
	var modal = $(this)
	modal.find('.e').text(title)
	modal.find('.modal-footer input[name=jobID]').val(jobID)
});
$('#showCoverLetterModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget) // Button that triggered the modal
	var letter = button.data('letter')
	var modal = $(this)
	modal.find('.e').text(letter)
});
$('#withdrawApplicationModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget) // Button that triggered the modal
	var id = button.data('id')
	var modal = $(this)
	modal.find('.modal-footer input[name=jobID]').val(id)
});
$('#archiveApplicationModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget) // Button that triggered the modal
	var id = button.data('id')
	var modal = $(this)
	modal.find('.modal-footer input[name=jobID]').val(id)
});
function goAJAX(d){
	var id = d.getAttribute("data-company");
	$.ajax({
		url: "company",
		data: { companyID:id },

		success: function(response){
			var obj = JSON.parse(response);
			var modal = $('#showCompanyModal');
			modal.modal();
			modal.find('.email').text(obj.email)
			modal.find('.company').text(obj.name)
			modal.find('.website').text(obj.website)
			modal.find('.address').text(obj.address)
			modal.find('.industry').text(obj.industry)
			modal.find('.type').text(obj.typecomp)
			modal.find('.size').text(obj.size)
			modal.find('.description').text(obj.description)
		},
		error: function(response){
			alert("Error: Not working as expected!");
		}
	});
}
function goUserAJAX(d){
	var id = d.getAttribute("data-id");
	$.ajax({
		url: "applicantDetails",
		data: { applicantID:id },
		
		success: function(response){
			var obj = JSON.parse(response);
			var modal = $('#showUserModal');
			modal.modal();
			modal.find('.firstname').text(obj.firstName)
			modal.find('.lastname').text(obj.lastName)
			modal.find('.email').text(obj.email)
			modal.find('.address').text(obj.address)
			modal.find('.position').text(obj.currentPosition)
			modal.find('.education').text(obj.education)
			modal.find('.experience').text(obj.experience)
		},
		error: function(response){
			alert("Error: Not working as expected!");
		}
	});
}
function goLetterAJAX(d){
	var job = d.getAttribute("data-jobID");
	var app = d.getAttribute("data-appID");
	var status = d.getAttribute("data-status");
	$.ajax({
		url: "letter",
		data: {jobID:job, appID:app},
		
		success: function(response){
			var modal = $('#showCoverLetterModal');
			modal.modal();
			modal.find('.letter').text(response)
			$("#coverLetterEdit").hide();

			$("#coverLetterEditButton").click(function() {
				if(status=="open"){
					$('#coverLetterShow').slideUp();
					$("#coverLetterEdit").slideDown();
				}
			});
			modal.find('.modal-body input[name=jobID]').val(job)
		},
		error: function(response){
			alert("Error: Not working as expected!");
		}
	});
}
function resetModalLetter(){
	$('#coverLetterEdit').slideUp();
	$('#coverLetterShow').slideDown();
}
function goLetterEdit(d){
	var job = d.getAttribute("data-jobID");
	var app = d.getAttribute("data-appID");
	var status1 = d.getAttribute("data-status");
	$.ajax({
		url: "letter",
		data: {jobID:job, appID:app, status:status1},
		
		success: function(response){
			var modal = $('#editCoverLetterModal');
			modal.modal();
			modal.find('.letter').text(response)
		},
		error: function(response){
			alert("Error: Not working as expected!");
		}
	});
}
function checkCheckBox(){
	var count = $("[type='checkbox']:checked").length;
	var modal = $('#assignFailModal');
	if(count!=2){
		modal.modal();
	}
	else{
		$('#assign-form').submit();
	}
}
function goApplicationAJAX(d){
	var id = d.getAttribute("data-id");
	$.ajax({
		url: "applicationDetails",
		data: { applicationID:id },
		
		success: function(response){
			var obj = JSON.parse(response);
			var modal = $('#showApplicantModal');
			modal.modal();
			modal.find('.firstname').text(obj.firstName)
			modal.find('.lastname').text(obj.lastName)
			modal.find('.email').text(obj.email)
			modal.find('.address').text(obj.address)
			modal.find('.position').text(obj.currentPosition)
			modal.find('.education').text(obj.education)
			modal.find('.experience').text(obj.experience)
			modal.find('.coverLetter').text(obj.coverLetter)
		},
		error: function(response){
			alert("Error: Not working as expected!");
		}
	});
}
function goStatusAJAX(d){
	var jobid = d.getAttribute("data-jobid");
	var appid = d.getAttribute("data-appid");
	$.ajax({
		url: "applicationStatus",
		data: { applicantID:appid, jobID:jobid},
		
		success: function(response){
			var modal = $('#applicationStatusModal');
			modal.modal();
			modal.find('.e').text(response);
			modal.find('.modal-footer input[name=jobID]').val(jobid)
			modal.find('.modal-footer input[name=appID]').val(appid)
			var accept = $('#acceptInvite').show();
			var reject = $('#rejectInvite').show();
			var archive = $('#archive').show();
			var cancel = $('#cancel').show();
			if(response!='shortlisted'){
				accept.hide();
				reject.hide();
				archive.hide();
				if(response=='rejected'){
					archive.show();
				}
			}
			else {
				cancel.hide();
			}
		},
		error: function(response){
			alert("Error: Not working as expected!");
		}
	});
}
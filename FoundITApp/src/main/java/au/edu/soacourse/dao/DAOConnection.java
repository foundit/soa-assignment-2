package au.edu.soacourse.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DAOConnection {
	private static Connection instance = buildConnection();

	private static Connection buildConnection() {
		try{
			Class.forName("org.sqlite.JDBC");
			instance = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Scott\\MIT Y3 S1 UNSW\\COMP9322\\Assignment 2\\FoundIT-database");
		} catch (Exception e){
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			if(e.getMessage().equals("database connection closed")){
				System.out.println("proceed with connection");
				return instance;
			}
		    System.exit(0);
		}
		return instance;
	}
	
	public static Connection getConnection() {
		return instance;
	}
	
	public static void shutdown() throws SQLException {
		getConnection().close();
	}
}

package au.edu.soacourse.dao;

import java.util.HashMap;

import au.edu.soacourse.service.UserServiceImpl;

public class DAOFactory {
	private static final String USER_DAO = "userDAO";
	private HashMap daos;
	private static DAOFactory instance = new DAOFactory();
	
	private DAOFactory(){
		//daos = new HashMap();
		//daos.put(USER_DAO, new UserServiceImpl(null));
	}
	
	public static DAOFactory getInstance() {
		return instance;
	}
	
	public UserDAO getUseDAO(){
		return (UserDAO)daos.get(USER_DAO);
	}
}

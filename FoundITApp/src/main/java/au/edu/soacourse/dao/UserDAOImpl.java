package au.edu.soacourse.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.apache.tomcat.dbcp.dbcp2.DelegatingConnection;
import org.springframework.stereotype.Repository;

import au.edu.soacourse.dao.Constants;
import au.edu.soacourse.model.JobBean;
import au.edu.soacourse.model.UserBean;

@Repository
public class UserDAOImpl implements UserDAO{

	public int createUser(String email, String password, String role) {
		Connection c = null;
		Statement stmt = null;
		ResultSet r = null;
		int userID = 0;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "INSERT INTO USERS ('email', 'password', 'role') VALUES ('" + email + "', '" + password + "','" + role + "');";
			stmt.executeUpdate(sql);
			c.commit();
			sql = "SELECT * FROM USERS WHERE EMAIL = '" + email + "'";
			r = stmt.executeQuery(sql);
			if(r.next()){
				userID = r.getInt("USERID");
			}
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return userID;
	}
	
	public Boolean verifyUser(String email, String password) {
		Connection c = null;
		Statement stmt = null;
		ResultSet r = null;
		Boolean result = true;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			
			String sql = "SELECT password FROM USERS WHERE email='" + email + "'";
			stmt = c.createStatement();
			r = stmt.executeQuery(sql);
			if(r.next()){
				if(!r.getString("PASSWORD").equals(password)){
					result = false;
				}
			}
			else {
				result = false;
			}
		} catch (SQLException | ClassNotFoundException e){
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return result;
	}

	public UserBean getUser(String email) {
		UserBean user = new UserBean();
		Connection c = null;
		Statement stmt = null;
		ResultSet r = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "SELECT * FROM USERS WHERE email = '" + email +"'";
			r = stmt.executeQuery(sql);
			if (r.next()){
				user.setUserID(r.getInt("USERID")+"");
				user.setEmail(r.getString("EMAIL"));
				user.setPassword(r.getString("PASSWORD"));
				user.setRole(r.getString("ROLE"));
			}
			else{
				user = null;
			}
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			user = null;
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return user;
	}
	
	public Boolean updateUser(UserBean user) {
		Connection c = null;
		Statement stmt = null;
		Boolean result = true;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "UPDATE USERS SET EMAIL = '" + user.getEmail() + "', PASSWORD = '" + user.getPassword() + "' WHERE USERID = '" + user.getUserID() + "'";
			stmt.executeUpdate(sql);
			c.commit();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
	
	public Boolean removeUser(String email) {
		UserBean user = new UserBean();
		Connection c = null;
		Statement stmt = null;
		Boolean result = true;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "DELETE FROM USERS WHERE EMAIL = '" + email +"'";
			stmt.executeUpdate(sql);
			c.commit();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
	
	// Redundant now
	public Boolean setProfileID(String id, String email){
		Connection c = null;
		Statement stmt = null;
		Boolean result = true;
		System.out.println("Set ProfileID: " + id + " " + email);
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "UPDATE USERS SET _profileID = '" + id + "' WHERE email = '" + email +"'";
			stmt.executeUpdate(sql);
			c.commit();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
	
	public Boolean saveJob(String jobID, String appID) {
		Connection c = null;
		Statement stmt = null;
		Boolean result = true;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "INSERT INTO SAVEDJOBS ('JOBID', 'APPLICANTID') VALUES ('" + jobID + "', '" + appID + "')";
			stmt.executeUpdate(sql);
			c.commit();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result=false;
		} finally {
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
	
	public Boolean deleteJob(String jobID, String appID) {
		Connection c = null;
		Statement stmt = null;
		Boolean result = true;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "DELETE FROM SAVEDJOBS WHERE JOBID = '" + jobID + "' AND APPLICANTID = '" + appID + "'";
			stmt.executeUpdate(sql);
			c.commit();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result=false;
		} finally {
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
	
	public List<String> retrieveAllSaved(String appID){
		Connection c = null;
		Statement stmt = null;
		ResultSet r = null;
		List<String> result = new ArrayList<String>();
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "SELECT JOBID FROM SAVEDJOBS WHERE APPLICANTID = '" + appID + "'";
			r = stmt.executeQuery(sql);
			while(r.next()){
				result.add(r.getString("JOBID"));
			}
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result=null;
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
}

package au.edu.soacourse.dao;

public class Constants {
	public final static String ROOT_DIR = System.getProperty("catalina.home")+"/webapps/ROOT";
	public final static String DATABASE_URL = "jdbc:sqlite:"+ROOT_DIR+"/FoundIT-database.db";
}

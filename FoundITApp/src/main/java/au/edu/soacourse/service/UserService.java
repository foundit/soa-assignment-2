package au.edu.soacourse.service;

import java.util.List;

import au.edu.soacourse.model.UserBean;

public interface UserService {
	public Boolean verifyUser(String email, String password);
	public int createUser(String email, String password, String role);
	public UserBean getUser(String email);
	public Boolean removeUser(String email);
	public Boolean updateUser(UserBean user);
	public Boolean setProfileID(String id, String email);
	public Boolean saveJob(String jobID, String appID);
	public Boolean deleteJob(String jobID, String appID);
	public List<String> retrieveAllSaved(String appID);
}

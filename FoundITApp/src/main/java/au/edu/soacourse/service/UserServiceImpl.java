package au.edu.soacourse.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.edu.soacourse.dao.DAOFactory;
import au.edu.soacourse.dao.UserDAO;
import au.edu.soacourse.model.UserBean;

public class UserServiceImpl implements UserService{
	
	private UserDAO userDAO;
	
	@Autowired
	public UserServiceImpl(UserDAO userDAO){
		this.userDAO = userDAO;
	}
	public void setUserDAO(UserDAO userDAO){
		this.userDAO = userDAO;
	}
	public Boolean verifyUser(String email, String password){
		return userDAO.verifyUser(email, password);
	}
	public int createUser(String email, String password, String role) {
		return userDAO.createUser(email, password, role);
	}
	public UserBean getUser(String email) {
		return userDAO.getUser(email);
	}
	public Boolean setProfileID(String id, String email) {
		return userDAO.setProfileID(id, email);
	}
	public Boolean saveJob(String jobID, String appID) {
		return userDAO.saveJob(jobID, appID);
	}
	public Boolean deleteJob(String jobID, String appID) {
		return userDAO.deleteJob(jobID, appID);
	}
	public List<String> retrieveAllSaved(String appID) {
		return userDAO.retrieveAllSaved(appID);
	}
	public Boolean removeUser(String email) {
		return userDAO.removeUser(email);
	}
	public Boolean updateUser(UserBean user) {
		return userDAO.updateUser(user);
	}

}

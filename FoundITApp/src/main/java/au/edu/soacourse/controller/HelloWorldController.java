package au.edu.soacourse.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.edu.soacourse.service.UserService;
import au.edu.soacourse.validator.LoginValidator;
import au.edu.soacourse.validator.RegisterValidator;
import au.edu.soacourse.model.ApplicationBean;
import au.edu.soacourse.model.ApplicationListBean;
import au.edu.soacourse.model.CompanyBean;
import au.edu.soacourse.model.HiringTeam;
import au.edu.soacourse.model.JobBean;
import au.edu.soacourse.model.ProfileBean;
import au.edu.soacourse.model.ReviewBean;
import au.edu.soacourse.model.ReviewListBean;
import au.edu.soacourse.model.ReviewerBean;
import au.edu.soacourse.model.UserBean;
import au.edu.soacourse.model.JobListBean;

@Controller
public class HelloWorldController {
	
	static final String REST_URI = "http://localhost:8080/FoundITServer";
	WebClient client = WebClient.create(REST_URI);
	Response r = null;
	
	@Autowired
	LoginValidator loginValidator;
	@Autowired
	RegisterValidator registerValidator;
	private UserService userService;
	@Autowired
	public HelloWorldController(UserService userService){
		this.userService = userService;
	}
	
	public WebClient getClient(){
		List<Object> providers = new ArrayList<Object>();
        providers.add(new org.codehaus.jackson.jaxrs.JacksonJsonProvider());
		return WebClient.create(REST_URI, providers);
	}
	
	/*****************LOGIN********************************************************************/
	
	@RequestMapping(value="/login", method = RequestMethod.GET)
	public String loginForm(@ModelAttribute("user") UserBean user, HttpSession session) {
		/* Check if user is logged in already, else go to login page */
		if(session.getAttribute("userSession")!=null){
			UserBean userExists = (UserBean) session.getAttribute("userSession");
			if(userExists.getRole().equals("Applicant")){
				return "redirect:/applicant";
			}
			else if(userExists.getRole().equals("Employer")){
				return "redirect:/employer";
			}
			else if(userExists.getRole().equals("Reviewer")){
				return "redirect:/reviewer";
			}
		}
		return "login";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loginUser(@ModelAttribute("user") UserBean user, BindingResult result, Model model, 
			HttpServletRequest request, HttpSession session){
		/* Process login form to validate user */
		loginValidator.validate(user, result);
	    if (result.hasErrors()) {
	        return "login";
	    }
	    UserBean loginUser = userService.getUser(user.getEmail());
	    /* Generate new session */
	    session.invalidate();
	    HttpSession newSession = request.getSession();
		newSession.setAttribute("userSession", loginUser);
		if(loginUser.getRole().equals("Applicant")){
			return "redirect:/applicant";
		}
		else if(loginUser.getRole().equals("Employer")){
			return "redirect:/employer";
		}
		else{
			return "redirect:/reviewer";
		}
	}
	
	/*****************LOGOUT********************************************************************/
	
	@RequestMapping(value="/logout")
	public String logout(@ModelAttribute("user") UserBean user, HttpSession session) {
		session.invalidate();
		return "redirect:/login";
	}
	
	/*****************REGISTER********************************************************************/
	
	@RequestMapping(value="/register", method = RequestMethod.GET)
	public String registerForm(Model model) {
		UserBean user = new UserBean();
		model.addAttribute("roleList", getRole());
		model.addAttribute("user",user);
		return "register";
	}
	
	public List<String> getRole(){
		List<String> roleList = new ArrayList<String>();
		roleList.add("Applicant");
		roleList.add("Employer");
		return roleList;
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String registerUser(@ModelAttribute("user") UserBean user, BindingResult result, Model model){
		registerValidator.validate(user, result);
		int userID = userService.createUser(user.getEmail(), user.getPassword(), user.getRole());
		if(userID==0){
			result.rejectValue("email", "create.email");
		}
		//Check validation errors
	    if (result.hasErrors()) {
	    	model.addAttribute("roleList", getRole());
	        return "register";
	    }
		if(user.getRole().equals("Applicant")){
		    ProfileBean app = new ProfileBean();
		    app.setUserID(userID);
			model.addAttribute("app", app);
			return "applicantProfile";
		}
		CompanyBean emp = new CompanyBean();
		emp.setManagerID(userID);
		model.addAttribute("emp", emp);
		return "employerProfile";
	}
	
	/*****************CREATING PROFILE***********************************************************/
	
	@RequestMapping(value = "/profile", method = RequestMethod.POST)
	public String createAppProfile(@ModelAttribute("app") ProfileBean app, @ModelAttribute("emp") CompanyBean emp,
			@RequestParam(value = "role", required = true) String role, UserBean user, Model model,
			HttpServletRequest request, HttpSession session){
		WebClient client = getClient();
		Response r = null;
		if(role.equals("Applicant")){
			r = client.path("/users/applicant").type(MediaType.APPLICATION_JSON).post(app);
			user.setUserID(app.getUserID()+"");
		}
		else if(role.equals("Employer")){
			r = client.path("/users/employer").type(MediaType.APPLICATION_JSON).post(emp);
			user.setUserID(emp.getManagerID()+"");
		}
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.getLocation().toString());
		String uri = r.getLocation().toString();
		String token[] = uri.split("/");
		String ID = token[token.length-1];
		session.invalidate();
	    HttpSession newSession = request.getSession();
		newSession.setAttribute("userSession", user);
		if(role.equals("Applicant")){
			return "redirect:/applicant";
		}
		else if(role.equals("Employer")){
			HiringTeam hT = new HiringTeam();
			List<ReviewerBean> h = newTeam();
			hT.setTeam(h);
			model.addAttribute("hT", hT);
			model.addAttribute("companyID", ID);
			return "hiringTeam";
		}
		return "failure";
	}
	
	@RequestMapping(value="/save", method = RequestMethod.POST)
	public String edit(@RequestParam(value = "value", required = false) String type, @ModelAttribute("app") ProfileBean app,
			@ModelAttribute("emp") CompanyBean emp,
			Model model, HttpSession session, RedirectAttributes redirect){
		WebClient client = getClient();
		UserBean user = (UserBean) session.getAttribute("userSession");
		if(type.equals("applicant")){
			System.out.println(app.getProfileID());
			// Profile ID not updated from form??
			app.setUserID(Integer.parseInt(user.getUserID()));
			Response r = client.path("/users/applicant/" + app.getUserID()).type(MediaType.APPLICATION_JSON).put(app);
			if(r.getStatusInfo().getReasonPhrase().equals("OK")){
				redirect.addFlashAttribute("editProfileSuccess", "Profile has been updated successfully");
			}
			else{
				redirect.addFlashAttribute("editProfileError", "Profile has not been updated. Problem");
			}
			return "redirect:/applicant#profile";
		}
		// type  = employer
		
		Response r = client.path("/users/employer/" + emp.getCompanyID()).type(MediaType.APPLICATION_JSON).put(emp);
		if(r.getStatusInfo().getReasonPhrase().equals("OK")){
			redirect.addFlashAttribute("editProfileSuccess", "Profile has been updated successfully");
		}
		else{
			redirect.addFlashAttribute("editProfileError", "Profile has not been updated. Problem");
		}
		return "redirect:/employer#profile";
	}
	
	/*****************APPLIANT HOME***********************************************************/
	
	@RequestMapping(value="/applicant", method = RequestMethod.GET)
	public String goapplicantPage(@ModelAttribute("app") ProfileBean app, HttpSession session, Model model) {
		if(session.getAttribute("userSession")==null){
			model.addAttribute("user", new UserBean());
			model.addAttribute("errorHome", "Can't Access home page without login");
			return "/login";
		}
		UserBean user = (UserBean) session.getAttribute("userSession");
		if(user.getUserID()==null){
			// Need to create profile
			model.addAttribute("app", new ProfileBean());
			model.addAttribute("user", user);
			model.addAttribute("errorProfile", "Your Profile has been deleted. Need to create new one");
			return "/applicantProfile";
		}
		WebClient client = getClient();
		ObjectMapper mapper = new ObjectMapper();
		if(user.getRole().equals("Applicant")){
			r = client.path("/users/applicant/" + user.getUserID()).type(MediaType.APPLICATION_JSON).get();
			System.out.println(r.getStatusInfo().getReasonPhrase());
			System.out.println(r.readEntity(String.class));
			ProfileBean newProfile = new ProfileBean();
			try {
				newProfile = mapper.readValue(r.readEntity(String.class), ProfileBean.class);
			} catch (JsonParseException e) { e.printStackTrace();
			} catch (JsonMappingException e) { e.printStackTrace();
			} catch (IOException e) { e.printStackTrace();
			}
			model.addAttribute("app", newProfile);
		}
		// List of saved jobs
		List<String> allSaved = userService.retrieveAllSaved(user.getUserID());
		if(allSaved.size()!=0){
			client.back(true);
			client.path("/users/applicant/jobs").type(MediaType.APPLICATION_JSON);
			for(int i=0; i<allSaved.size(); i++){
				client.query("list", allSaved.get(i));
			}
			r = client.get();
			System.out.println(r.getStatusInfo().getReasonPhrase());
			System.out.println(r.readEntity(String.class));
			JobListBean allJobs = new JobListBean();
			try {
				allJobs = mapper.readValue(r.readEntity(String.class), JobListBean.class);
			} catch (JsonParseException e) { e.printStackTrace();
			} catch (JsonMappingException e) { e.printStackTrace();
			} catch (IOException e) { e.printStackTrace();
			}
			model.addAttribute("savedJobs", allJobs);
		}
		// List of applied jobs
		client.back(true);
		r = client.path("/users/" + user.getUserID() + "/applied").type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		List<String> allApplied = new ArrayList<String>();
		try {
			allApplied = mapper.readValue(r.readEntity(String.class), new TypeReference<List<String>>() {});
		} catch (JsonParseException e) { e.printStackTrace();
		} catch (JsonMappingException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}
		// Now use list of jobID's to retrieve JobListBean of applied jobs
		if(allApplied.size()!=0){
			client.back(true);
			client.resetQuery();
			client.path("/users/applicant/jobs").type(MediaType.APPLICATION_JSON);
			for(int i=0; i<allApplied.size(); i++){
				client.query("list", allApplied.get(i));
			}
			r = client.get();
			System.out.println("Applied JOBS**********");
			System.out.println(r.getStatusInfo().getReasonPhrase());
			System.out.println(r.readEntity(String.class));
			JobListBean allJobs = new JobListBean();
			try {
				allJobs = mapper.readValue(r.readEntity(String.class), JobListBean.class);
			} catch (JsonParseException e) { e.printStackTrace();
			} catch (JsonMappingException e) { e.printStackTrace();
			} catch (IOException e) { e.printStackTrace();
			}
			model.addAttribute("appliedJobs", allJobs);
		}
		return "applicant";
	}
	
	/*****************EMPLOYER / COMPANYs HOME***********************************************************/
	
	@RequestMapping(value="/employer")
	public String goEmployerPage(@ModelAttribute("emp") CompanyBean emp, HttpSession session, Model model) {
		if(session.getAttribute("userSession")==null){
			model.addAttribute("user", new UserBean());
			model.addAttribute("errorHome", "Can't Access home page without login");
			return "/login";
		}
		UserBean user = (UserBean) session.getAttribute("userSession");
		if(user.getUserID()==null){
			// Need to create profile
			model.addAttribute("emp", new CompanyBean());
			model.addAttribute("user", user);
			model.addAttribute("errorProfile", "Your Profile has been deleted. Need to create new one");
			return "/employerProfile";
		}
		WebClient client = getClient();
		r = client.path("/users/employer/" + user.getUserID()).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		CompanyBean newCompany = new CompanyBean();
		ObjectMapper mapper = new ObjectMapper();
		try {
			newCompany = mapper.readValue(r.readEntity(String.class), CompanyBean.class);
		} catch (JsonParseException e) { e.printStackTrace();
		} catch (JsonMappingException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}
		client.back(true);
		System.out.println(newCompany.getCompanyID());
		r = client.path("/users/employer/jobs/" + newCompany.getCompanyID()).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		JobListBean allJobs = new JobListBean();
		try {
			allJobs = mapper.readValue(r.readEntity(String.class), JobListBean.class);
		} catch (JsonParseException e) { e.printStackTrace();
		} catch (JsonMappingException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}
		System.out.println("List size: " + allJobs.getAllPostings().size());
		client.back(true);
		r = client.path("/users/employer/team/" + newCompany.getCompanyID()).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		HiringTeam team = new HiringTeam();
		try {
			team = mapper.readValue(r.readEntity(String.class), HiringTeam.class);
		} catch (JsonParseException e) { e.printStackTrace();
		} catch (JsonMappingException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}
		System.out.println("List size: " + team.getTeam().size());
		model.addAttribute("emp", newCompany);
		model.addAttribute("allJobs", allJobs);
		model.addAttribute("team", team);
		model.addAttribute("hB", new ReviewerBean());
		return "employer";
	}
	
	/*****************REVIEWER HOME***********************************************************/
	
	@RequestMapping(value="/reviewer", method = RequestMethod.GET)
	public String goReviewerPage(HttpSession session, Model model){
		if(session.getAttribute("userSession")==null){
			model.addAttribute("user", new UserBean());
			model.addAttribute("errorHome", "Can't Access home page without login");
			return "/login";
		}
		UserBean user = (UserBean) session.getAttribute("userSession");
		WebClient client = getClient();
		r = client.path("/users/reviewer/" + user.getUserID()).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		ReviewerBean reviewer = new ReviewerBean();
		ObjectMapper mapper = new ObjectMapper();
		try {
			reviewer = mapper.readValue(r.readEntity(String.class), ReviewerBean.class);
		} catch (JsonParseException e) { e.printStackTrace();
		} catch (JsonMappingException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}
		model.addAttribute("rev", reviewer);
		
		client.back(true);
		r = client.path("/users/employer/" + reviewer.getCompanyID()).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		CompanyBean company = new CompanyBean();
		try {
			company = mapper.readValue(r.readEntity(String.class), CompanyBean.class);
		} catch (JsonParseException e) { e.printStackTrace();
		} catch (JsonMappingException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}
		model.addAttribute("emp", company);
		
		client.back(true);
		r = client.path("/users/reviews/" + reviewer.getReviewerID()).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		ReviewListBean reviews = new ReviewListBean();
		try {
			reviews = mapper.readValue(r.readEntity(String.class), ReviewListBean.class);
		} catch (JsonParseException e) { e.printStackTrace();
		} catch (JsonMappingException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}
		model.addAttribute("reviews", reviews);
		return "reviewer";
	}
	
	
	/*****************JOB POSTINGS********************************************************************/
	
	@RequestMapping(value="/newjob", method = RequestMethod.GET)
	public String createNewJob(@RequestParam(value = "id", required=true) int companyID, RedirectAttributes redirect){
		JobBean job = new JobBean();
		job.setCompanyID(companyID);
		redirect.addFlashAttribute("job", job);
		return "redirect:/jobPosting";
	}
	
	@RequestMapping(value="jobPosting", method = RequestMethod.GET)
	public String goToNewJob(@ModelAttribute("job") JobBean job){
		return "jobPosting";
	}
	
	@RequestMapping(value="/newjob", method = RequestMethod.POST)
	public String processNewJob(@ModelAttribute("job") JobBean job, RedirectAttributes redirect){
		WebClient client = getClient();
		Response r = client.path("users/employer/job").type(MediaType.APPLICATION_JSON).post(job);
		if(r.getStatusInfo().getReasonPhrase().equals("Created")){
			redirect.addFlashAttribute("newJobSuccess", "Success, your job posting was created at " + r.getLocation().getPath());
		}
		else {
			redirect.addFlashAttribute("newJobError", "Your job posting couldn't be created");
		}
		return "redirect:/employer#jobs";
	}
	
	@RequestMapping(value="/editJob", method = RequestMethod.GET)
	public String editJob(@RequestParam("jobID") String jobID, RedirectAttributes redirect){
		JobBean job = new JobBean();
		WebClient client = getClient();
		Response r = client.path("/users/employer/job/" + jobID).type(MediaType.APPLICATION_JSON).get();
		ObjectMapper mapper = new ObjectMapper();
		try {
			job = mapper.readValue(r.readEntity(String.class), JobBean.class);
		} catch (JsonParseException e) { e.printStackTrace();
		} catch (JsonMappingException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}
		redirect.addFlashAttribute("job", job);
		return "redirect:/editPosting";
	}
	
	@RequestMapping(value="editPosting", method = RequestMethod.GET)
	public String goToEditJob(@ModelAttribute("job") JobBean job){
		return "editJobPosting";
	}
	
	@RequestMapping(value="/editJob", method = RequestMethod.POST)
	public String processEditJob(@ModelAttribute("job") JobBean job, RedirectAttributes redirect){
		WebClient client = getClient();
		Response r = client.path("users/employer/job").type(MediaType.APPLICATION_JSON).put(job);
		if(r.getStatusInfo().getReasonPhrase().equals("OK")){
			redirect.addFlashAttribute("editJobSuccess", "Success, your job posting was modified");
		}
		else {
			redirect.addFlashAttribute("editJobError", "Your job posting wasn't modified");
		}
		return "redirect:/employer#jobs";
	}
	
	@RequestMapping(value="/deleteJob", method = RequestMethod.POST)
	public String processDeleteJob(HttpServletRequest request, RedirectAttributes redirect){
		WebClient client = getClient();
		Response r = client.path("users/employer/job/" + request.getParameter("jobID")).type(MediaType.APPLICATION_JSON).delete();
		if(r.getStatusInfo().getReasonPhrase().equals("OK")){
			redirect.addFlashAttribute("deleteJobSuccess", "Success, your job posting was deleted");
		}
		else {
			redirect.addFlashAttribute("deleteJobError", "Your job posting wasn't deleted");
		}
		return "redirect:/employer#jobs";
	}
	
	@RequestMapping(value="/search", method = RequestMethod.GET)
	public String searchJobPostings(HttpServletRequest request, RedirectAttributes redirect){
		WebClient client = getClient();
		Response r = client.path("users/job").type(MediaType.APPLICATION_JSON)
				.query("type", request.getParameter("type"))
				.query("status", request.getParameter("status"))
				.query("search", request.getParameter("search")).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		JobListBean search = new JobListBean();
		ObjectMapper mapper = new ObjectMapper();
		try {
			search = mapper.readValue(r.readEntity(String.class), JobListBean.class);
		} catch (JsonParseException e) { e.printStackTrace();
		} catch (JsonMappingException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}
		redirect.addFlashAttribute("search", search);
		return "redirect:/applicant#search";
	}
	
	@RequestMapping(value="/saveJobPosting", method = RequestMethod.POST)
	public String saveJobPosting(HttpServletRequest request, HttpSession session, RedirectAttributes redirect, Model model){
		UserBean user = (UserBean) session.getAttribute("userSession");
		if(!userService.saveJob(request.getParameter("jobID"), user.getUserID())){
			redirect.addFlashAttribute("savePostingError", "Error: couldn't save job posting. Already Saved?");
		}
		else {
			redirect.addFlashAttribute("savePostingSuccess", "Saved job successfully");
		}
		return "redirect:/applicant#saved";
	}
	
	@RequestMapping(value="/deleteSavedJobPosting", method = RequestMethod.POST)
	public String deleteSavedJobPosting(HttpServletRequest request, HttpSession session, RedirectAttributes redirect, Model model){
		if(!userService.deleteJob(request.getParameter("jobID"), request.getParameter("appID"))){
			redirect.addFlashAttribute("deletePostingError", "Error: couldn't delete job posting. Already Deleted?");
		}
		else {
			redirect.addFlashAttribute("deletePostingSuccess", "Deleted job successfully");
		}
		return "redirect:/applicant#saved";
	}
	
	/*****************REVIEWERS********************************************************************/
	
	public List<ReviewerBean> newTeam()
	{
		List<ReviewerBean> teamMembers = new ArrayList<ReviewerBean>();
		teamMembers.add(new ReviewerBean());
		teamMembers.add(new ReviewerBean());
		teamMembers.add(new ReviewerBean());
		teamMembers.add(new ReviewerBean());
		teamMembers.add(new ReviewerBean());
		return teamMembers;
	}
	
	@RequestMapping(value="/hiring", method = RequestMethod.POST)
	public String processHiringTeam(@ModelAttribute("hT") HiringTeam hT, Model model, HttpSession session) {
		List<ReviewerBean> teamMembers = hT.getTeam();
		int numberOfReviewers = 0;
		for(int i=0; i<teamMembers.size(); i++){
			if(!teamMembers.get(i).getUsername().equals("")){
				numberOfReviewers++;
			}
		}
		System.out.println(numberOfReviewers);
		if(numberOfReviewers<2){
			model.addAttribute("errorNumber", "Must select at least 2 reviewers.");
			return "hiringTeam";
		}
		// Check if all users exist or not
		for(int i=0; i<teamMembers.size(); i++){
			if(!teamMembers.get(i).getUsername().equals("")){
				UserBean user = userService.getUser(teamMembers.get(i).getUsername());
				if(user != null){
					model.addAttribute("errorUsername", "Username(s) already exists for entry " + (i+1));
					return "hiringTeam";
				}
			}
		}
		// If it reaches this point, then we can insert entries into the user database table
		for(int i=0; i<teamMembers.size(); i++){
			if(!teamMembers.get(i).getUsername().equals("")){
				teamMembers.get(i).setUserID(userService.createUser(teamMembers.get(i).getUsername(), teamMembers.get(i).getPassword(), "Reviewer"));
			}
		}
		//Update
		hT.setTeam(teamMembers);
		WebClient client = getClient();
		Response r = client.path("users/employer/team").type(MediaType.APPLICATION_JSON).post(hT);
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.getLocation().toString());
		return "redirect:/employer#hired";
	}
	
	
	@RequestMapping(value="/editTeam", method = RequestMethod.GET)
	public String editHiringTeam(@RequestParam(value = "id", required=true) int companyID, RedirectAttributes redirect){
		ReviewerBean reviewers = new ReviewerBean();
		reviewers.setCompanyID(companyID);
		redirect.addFlashAttribute("reviewers", reviewers);
		return "redirect:/hiring";
	}
	
	@RequestMapping(value="hiring", method = RequestMethod.GET)
	public String goToEditTeamPage(@ModelAttribute("reviewers") ReviewerBean reviewerBean, Model model){
		WebClient client = getClient();
		r = client.path("/users/employer/team/" + reviewerBean.getCompanyID()).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		HiringTeam team = new HiringTeam();
		ObjectMapper mapper = new ObjectMapper();
		try {
			team = mapper.readValue(r.readEntity(String.class), HiringTeam.class);
		} catch (JsonParseException e) { e.printStackTrace();
		} catch (JsonMappingException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}
		model.addAttribute("hT", team);
		return "editHiringTeam";
	}
	
	@RequestMapping(value="/editHiring", method = RequestMethod.POST)
	public String editHiringTeam(@ModelAttribute("hT") HiringTeam team, RedirectAttributes redirect, Model model){
		List<ReviewerBean> l = team.getTeam();
		for(int i=0; i<l.size(); i++){
			if(l.get(i).getUsername().equals("")){
				model.addAttribute("errorNullUsername", "Username can't be null for entry " + (i+1));
				return "editHiringTeam";
			}
			UserBean user = userService.getUser(l.get(i).getUsername());
			if(user!=null){
				// Exists but does id match?
				System.out.println(user.getUserID() + l.get(i).getUserID());
				if(!user.getUserID().equals(l.get(i).getUserID()+"")){
					model.addAttribute("errorUsernameExists", "Username already exists for entry " + (i+1));
					return "editHiringTeam";
				}
				else{
					System.out.println("Whatever");
				}
			}
		}
		for(int i=0; i<l.size(); i++){
			UserBean user = new UserBean();
			user.setEmail(l.get(i).getUsername());
			user.setPassword(l.get(i).getPassword());
			user.setUserID(l.get(i).getUserID() + "");
			userService.updateUser(user);
		}
		// Save to update user in database
		
		WebClient client = getClient();
		System.out.println(team.getTeam().size());
		Response r = client.path("users/employer/team/" + team.getTeam().get(0).getCompanyID()).type(MediaType.APPLICATION_JSON).put(team);
		if(r.getStatusInfo().getReasonPhrase().equals("OK")){
			redirect.addFlashAttribute("editHiringSuccess", "Success, your hiring team was modified");
		}
		else {
			redirect.addFlashAttribute("editHiringError", "Your hiring team wasn't modified");
		}
		return "redirect:/employer#hired";
	}
	
	@RequestMapping(value="/addMember", method = RequestMethod.POST)
	public String addNewMember(@ModelAttribute("hB") ReviewerBean alias, RedirectAttributes redirect){
		if(userService.getUser(alias.getUsername())!=null){
			redirect.addFlashAttribute("errorSameUsername", "Error, the username already exists");
			return "redirect:/employer#hired";
		}
		alias.setUserID(userService.createUser(alias.getUsername(), alias.getPassword(), "Reviewer"));
		HiringTeam one = new HiringTeam();
		List<ReviewerBean> l = new ArrayList<ReviewerBean>();
		l.add(alias);
		one.setTeam(l);
		WebClient client = getClient();
		Response r = client.path("users/employer/team").type(MediaType.APPLICATION_JSON).post(one);
		System.out.println(r.getStatusInfo().getReasonPhrase());
		if(r.getStatusInfo().getReasonPhrase().equals("Created")){
			redirect.addFlashAttribute("addHiringSuccess", "Success, you added a member to your hiring team at " + r.getLocation().getPath());
		}
		else {
			redirect.addFlashAttribute("addHiringError", "Your hiring team couldn't be expanded");
		}
		return "redirect:/employer#hired";
	}
	
	@RequestMapping(value="/deleteMember", method = RequestMethod.POST)
	public String deleteMember(HttpServletRequest request, RedirectAttributes redirect){
		if(userService.removeUser(request.getParameter("username"))==false){
			redirect.addFlashAttribute("deleteHiringError", "There was an error removing one of your hiring members");
			return "redirect:/employer#hired";
		}
		WebClient client = getClient();
		Response r = client.path("users/employer/team/" + request.getParameter("reviewerID")).delete();
		if(r.getStatusInfo().getReasonPhrase().equals("OK")){
			redirect.addFlashAttribute("deleteHiringSuccess", "Success, you removed a member of your hiring team");
		}
		else {
			redirect.addFlashAttribute("deleteHiringError", "There was an error removing one of your hiring members");
		}
		return "redirect:/employer#hired";
	}
	
	/*****************APPLICATIONS********************************************************************/
	
	@RequestMapping(value="/apply", method = RequestMethod.GET)
	public String applyForJob(HttpServletRequest request, Model model){
		ProfileBean profile = new ProfileBean();
		WebClient client = getClient();
		Response r = client.path("/users/applicant/" + request.getParameter("appID")).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.readEntity(String.class));
		ObjectMapper mapper = new ObjectMapper();
		try {
			profile = mapper.readValue(r.readEntity(String.class), ProfileBean.class);
		} catch (JsonParseException e) { e.printStackTrace();
		} catch (JsonMappingException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}
		client.back(true);
		r = client.path("/users/employer/job/" + request.getParameter("jobID")).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.readEntity(String.class));
		JobBean job = new JobBean();
		try {
			job = mapper.readValue(r.readEntity(String.class), JobBean.class);
		} catch (JsonParseException e) { e.printStackTrace();
		} catch (JsonMappingException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}
		model.addAttribute("p", profile);
		model.addAttribute("j", job);
		model.addAttribute("jobID", request.getParameter("jobID"));
		return "applyForJob";
	}
	
	@RequestMapping(value="/apply", method = RequestMethod.POST)
	public String submitCoverLetter(HttpServletRequest request, RedirectAttributes redirect){
		ApplicationBean application = new ApplicationBean(0, Integer.parseInt(request.getParameter("jobID")), 
				Integer.parseInt(request.getParameter("appID")), 
				request.getParameter("coverLetter"), "Unreviewed");
		WebClient client = getClient();
		Response r = client.path("/users/applicant/apply").type(MediaType.APPLICATION_JSON).post(application);
		System.out.println(r.getStatusInfo().getReasonPhrase());
		if(r.getStatusInfo().getReasonPhrase().equals("Created")){
			redirect.addFlashAttribute("applySuccess", "You applied successfully. Wait for more information");
		}
		else {
			redirect.addFlashAttribute("applyError", "Problem applying");
		}
		return "redirect:/applicant#apply";
	}
	
	@RequestMapping(value="/company", method = RequestMethod.GET)
	public @ResponseBody String getCompanyDetails(@RequestParam("companyID") String companyID){
		WebClient client = getClient();
		Response r = client.path("/users/employer/" + companyID).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		return r.readEntity(String.class);
	}
	
	@RequestMapping(value="/letter", method = RequestMethod.GET)
	public @ResponseBody String getCoverLetter(@RequestParam("jobID") String jobID, @RequestParam("appID") String appID){
		WebClient client = getClient();
		System.out.println(jobID + " " + appID);
		Response r = client.path("users/letter/" + appID + "/" + jobID).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		return r.readEntity(String.class);
	}
	
	@RequestMapping(value="/editLetter", method = RequestMethod.POST)
	public String saveCoverLetter(HttpServletRequest request, RedirectAttributes redirect){
		ApplicationBean application = new ApplicationBean(0, Integer.parseInt(request.getParameter("jobID")), 
				Integer.parseInt(request.getParameter("appID")), 
				request.getParameter("letter"), request.getParameter("status"));
		WebClient client = getClient();
		r = client.path("users/applicant/apply").type(MediaType.APPLICATION_JSON).put(application);
		if(r.getStatusInfo().getReasonPhrase().equals("OK")){
			redirect.addFlashAttribute("editLetterSuccess", "Success, your cover letter was modified");
		}
		else {
			redirect.addFlashAttribute("editLetterError", "Your cover letter wasn't modified");
		}
		return "redirect:/applicant#apply";
	}
	
	@RequestMapping(value="/withdrawApplication", method = RequestMethod.POST)
	public String withdrawApplication(HttpServletRequest request, RedirectAttributes redirect){
		WebClient client = getClient();
		r = client.path("/users/applicant/" + request.getParameter("applicantID") + "/apply/" + request.getParameter("jobID")).type(MediaType.APPLICATION_JSON).delete();
		if(r.getStatusInfo().getReasonPhrase().equals("OK")){
			redirect.addFlashAttribute("WithdrawSuccess", "Success, you withdrew your application");
		}
		else {
			redirect.addFlashAttribute("WithdrawError", "Error, your application couldn't be withdrawed");
		}
		return "redirect:/applicant#apply";
	}
	
	@RequestMapping(value="/archiveApplication", method = RequestMethod.POST)
	public String archiveApplication(HttpServletRequest request, RedirectAttributes redirect){
		WebClient client = getClient();
		r = client.path("/users/applicant/" + request.getParameter("appID") + "/apply/" + request.getParameter("jobID")).type(MediaType.APPLICATION_JSON).delete();
		if(r.getStatusInfo().getReasonPhrase().equals("OK")){
			redirect.addFlashAttribute("ArchiveSuccess", "Success, you archived your application");
		}
		else {
			redirect.addFlashAttribute("ArchiveError", "Error, your application couldn't be archived");
		}
		return "redirect:/applicant#apply";
	}
	
	/*****************RECRUITMENT PROCESS********************************************************************/
	@RequestMapping(value="startRecruitment", method = RequestMethod.POST)
	public String startRecruitmentProcess(HttpServletRequest request, RedirectAttributes redirect){
		redirect.addFlashAttribute("check", false);
		redirect.addFlashAttribute("jobSelect", request.getParameter("jobSelect"));
		return "redirect:/recruitment";
	}
	
	@RequestMapping(value="recruitment", method = RequestMethod.GET)
	public String RecruitmentProcess(HttpServletRequest request, Model model, @ModelAttribute("check") boolean check, @ModelAttribute("jobSelect") String jobID){
		WebClient client = getClient();
		r = client.path("/users/employer/job/" + jobID).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		JobBean job = new JobBean();
		ObjectMapper mapper = new ObjectMapper();
		try {
			job = mapper.readValue(r.readEntity(String.class), JobBean.class);
		} catch (JsonParseException e) { e.printStackTrace();
		} catch (JsonMappingException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}
		model.addAttribute("job", job);
		
		//All applicants who applied to this job
		client.back(true);
		r = client.path("/users/employer/applications/" + job.getJobID()).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		ApplicationListBean applications = new ApplicationListBean();
		try {
			applications = mapper.readValue(r.readEntity(String.class), ApplicationListBean.class);
		} catch (JsonParseException e) { e.printStackTrace();
		} catch (JsonMappingException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}
		model.addAttribute("applications", applications);
		model.addAttribute("check", check);
		
		// Get Hiring Team
		client.back(true);
		r = client.path("/users/employer/team/" + job.getCompanyID()).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		HiringTeam team = new HiringTeam();
		try {
			team = mapper.readValue(r.readEntity(String.class), HiringTeam.class);
		} catch (JsonParseException e) { e.printStackTrace();
		} catch (JsonMappingException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}
		model.addAttribute("team", team);
		return "assignTeam";
	}
	
	@RequestMapping(value="/applicantDetails", method = RequestMethod.GET)
	public @ResponseBody String getApplicantDetails(@RequestParam("applicantID") String applicantID){
		WebClient client = getClient();
		r = client.path("/users/applicant/" + applicantID).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		return r.readEntity(String.class);
	}
	
	/*****************RECRUITMENT PROCESS -> BACKGROUND PROCESS**********************************************************/
	@RequestMapping(value="background", method = RequestMethod.GET)
	public String startBackgroundCheck(@RequestParam("jobID") String jobID, RedirectAttributes redirect, Model model){
		redirect.addFlashAttribute("jobSelect", jobID);
		redirect.addFlashAttribute("check", true);
		return "redirect:/recruitment";
	}
	
	@RequestMapping(value="/assign", method = RequestMethod.POST)
	public String assignReviewers(HttpServletRequest request){
		String check[] = request.getParameterValues("check");
		String applicationID[] = request.getParameterValues("applicationID");
		List<ReviewBean> reviews = new ArrayList<ReviewBean>();
		for(int i=0; i<check.length; i++){
			for(int j=0; j<applicationID.length; j++){
				ReviewBean r = new ReviewBean();
				r.setReviewerID(Integer.parseInt(check[i]));
				r.setApplicationID(Integer.parseInt(applicationID[j]));
				reviews.add(r);
			}
		}
		ReviewListBean rB = new ReviewListBean();
		rB.setReviews(reviews);
		WebClient client = getClient();
		r = client.path("/users/reviews").type(MediaType.APPLICATION_JSON).post(rB);
		System.out.println(r.getStatusInfo().getReasonPhrase());
		
		// Set status of job posting to 'processing'
		client.back(true);
		r = client.path("/users/employer/job/" + request.getParameter("jobID") + "/status").type(MediaType.APPLICATION_JSON).put("processing");
		System.out.println(r.getStatusInfo().getReasonPhrase());
		return "redirect:/employer";
	}
	
	@RequestMapping(value="/applicationDetails", method = RequestMethod.GET)
	public @ResponseBody String getApplicantionDetails(@RequestParam("applicationID") String applicationID){
		WebClient client = getClient();
		r = client.path("/users/reviewer/application/" + applicationID).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		JSONParser p = new JSONParser();
		JSONObject j = new JSONObject();
		try {
			j = (JSONObject) p.parse(r.readEntity(String.class));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		client.back(true);
		r = client.path("/users/applicant/" + j.get("applicantID")).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		JSONObject profile = new JSONObject();
		try {
			profile = (JSONObject) p.parse(r.readEntity(String.class));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		profile.put("coverLetter", j.get("coverLetter"));
		return profile.toJSONString();
	}
	
	@RequestMapping(value="/applicationStatus", method = RequestMethod.GET)
	public @ResponseBody String getApplicantionStatus(@RequestParam("applicantID") String applicantID, @RequestParam("jobID") String jobID){
		WebClient client = getClient();
		r = client.path("/users/applicant/" + applicantID + "/job/" + jobID + "/status").type(MediaType.TEXT_PLAIN).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		return r.readEntity(String.class);
	}
	
	@RequestMapping(value="saveReview", method = RequestMethod.POST)
	public String updateReview(HttpServletRequest request, RedirectAttributes redirect){
		ReviewBean review = new ReviewBean();
		review.setComments(request.getParameter("comments"));
		review.setReviewID(Integer.parseInt(request.getParameter("reviewID")));
		review.setDecision(request.getParameter("decision"));
		WebClient client = getClient();
		System.out.println("Review ID: " + review.getReviewID());
		System.out.println("Comments:" + request.getParameter("comments"));
		System.out.println("Decision: " + request.getParameter("decision") + request.getParameter("reviewID"));
		if(request.getParameter("decision").equals("rejected")){
			// Set Application Status to rejected immediately as two shortlisted are necessary
			r = client.path("/users/application/" + request.getParameter("applicationID") + "/status").type(MediaType.TEXT_PLAIN).put("rejected");
		}
		client.back(true);
		r = client.path("/users/review/" + review.getReviewID()).type(MediaType.APPLICATION_JSON).put(review);
		System.out.println(r.getStatusInfo().getReasonPhrase());
		redirect.addFlashAttribute("SavedSuccess", "You have successfully proceeded an application");
		return "redirect:/reviewer";
	}
	
	/*****************RECRUITMENT PROCESS -> VIEW PROCESS**********************************************************/
	@RequestMapping(value="progress", method = RequestMethod.GET)
	public String checkApplicationProgress(HttpServletRequest request, Model model){
		WebClient client = getClient();
		r = client.path("/users/employer/applications/" + request.getParameter("jobSelect")).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		ObjectMapper mapper = new ObjectMapper();
		ApplicationListBean applications = new ApplicationListBean();
		try {
			applications = mapper.readValue(r.readEntity(String.class), ApplicationListBean.class);
		} catch (JsonParseException e) { e.printStackTrace();
		} catch (JsonMappingException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}
		model.addAttribute("applications", applications);

		ReviewListBean reviews = new ReviewListBean();
		List<ReviewBean> allReviews = new ArrayList<ReviewBean>();
		List<Integer> applicationID = new ArrayList<Integer>();
		for(int i=0; i<applications.getApplications().size(); i++){
			client.back(true);
			r = client.path("/users/review/applicationID/" + applications.getApplications().get(i).getApplicationID()).type(MediaType.APPLICATION_JSON).get();
			System.out.println(r.readEntity(String.class));
			ReviewListBean reviewL = new ReviewListBean();
			try {
				reviewL = mapper.readValue(r.readEntity(String.class), ReviewListBean.class);
			} catch (JsonParseException e) { e.printStackTrace();
			} catch (JsonMappingException e) { e.printStackTrace();
			} catch (IOException e) { e.printStackTrace();
			}
			for(int j=0; j<reviewL.getReviews().size(); j++){
				ReviewBean n = new ReviewBean();
				n.setApplicationID(reviewL.getReviews().get(j).getApplicationID());
				n.setComments(reviewL.getReviews().get(j).getComments());
				n.setDecision(reviewL.getReviews().get(j).getDecision());
				n.setReviewerID(reviewL.getReviews().get(j).getReviewerID());
				n.setReviewID(reviewL.getReviews().get(j).getReviewID());
				allReviews.add(n);
			}
			applicationID.add(reviewL.getReviews().get(0).getApplicationID());
		}
		reviews.setReviews(allReviews);
		model.addAttribute("reviews", reviews);
		model.addAttribute("applicationID", applicationID);
		
		client.back(true);
		r = client.path("/users/employer/job/" + request.getParameter("jobSelect")).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		JobBean job = new JobBean();
		try {
			job = mapper.readValue(r.readEntity(String.class), JobBean.class);
		} catch (JsonParseException e) { e.printStackTrace();
		} catch (JsonMappingException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}
		model.addAttribute("job", job);
		return "applicationStatus";
	}
	
	/*****************RECRUITMENT PROCESS -> VIEW PROGESS**********************************************************/
	@RequestMapping(value="finalise", method = RequestMethod.POST)
	public String finaliseList(HttpServletRequest request, Model model){
		String[] applicationID = request.getParameterValues("applicationID");
		List<ProfileBean> profiles = new ArrayList<ProfileBean>();
		WebClient client = getClient();
		r = client.path("/users/employer/job/" + request.getParameter("jobID")).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		JobBean job = new JobBean();
		ObjectMapper mapper = new ObjectMapper();
		try {
			job = mapper.readValue(r.readEntity(String.class), JobBean.class);
		} catch (JsonParseException e) { e.printStackTrace();
		} catch (JsonMappingException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}
		model.addAttribute("job", job);
		List<String> shortlisted = new ArrayList<String>();
		for(int i=0; i<applicationID.length; i++){
			client.back(true);
			r = client.path("/users/shortlisted/" + applicationID[i]).type(MediaType.TEXT_PLAIN).get();
			if(!r.getStatusInfo().getReasonPhrase().equals("OK")){
				continue;
			}
			shortlisted.add(applicationID[i]);
			// Successful applicant (shortlisted by both reviewers)
			System.out.println("ApplicationID: " + applicationID[i]);
			client.back(true);
			r = client.path("/users/reviewer/application/" + applicationID[i]).type(MediaType.APPLICATION_JSON).get();
			System.out.println(r.getStatusInfo().getReasonPhrase());
			System.out.println(r.readEntity(String.class));
			ApplicationBean application = new ApplicationBean();
			try {
				application = mapper.readValue(r.readEntity(String.class), ApplicationBean.class);
			} catch (JsonParseException e) { e.printStackTrace();
			} catch (JsonMappingException e) { e.printStackTrace();
			} catch (IOException e) { e.printStackTrace();
			}
			client.back(true);
			r = client.path("/users/applicant/" + application.getApplicantID()).type(MediaType.APPLICATION_JSON).get();
			System.out.println(r.getStatusInfo().getReasonPhrase());
			System.out.println(r.readEntity(String.class));
			ProfileBean profile = new ProfileBean();
			try {
				profile = mapper.readValue(r.readEntity(String.class), ProfileBean.class);
			} catch (JsonParseException e) { e.printStackTrace();
			} catch (JsonMappingException e) { e.printStackTrace();
			} catch (IOException e) { e.printStackTrace();
			}
			profiles.add(profile);
		}
		model.addAttribute("shortlisted", shortlisted);
		model.addAttribute("profiles", profiles);
		return "finalList";
	}
	
	/*****************RECRUITMENT PROCESS -> BEFORE INTERVIEWS -> GET REPLY FROM APPLICANT *************************************/
	@RequestMapping(value="receiveReply", method = RequestMethod.POST)
	public String startInterviews(HttpServletRequest request, Model model){
		String[] applicantID = request.getParameterValues("applicantID");
		String[] applicationID = request.getParameterValues("applicationID");
		System.out.println("applicationID: " + applicationID.length);
		WebClient client = getClient();
		r = client.path("/users/employer/job/" + request.getParameter("jobID") + "/status").type(MediaType.APPLICATION_JSON).put("interviews");
		for(int i=0; i<applicantID.length; i++){
			client.back(true);
			r = client.path("/users/interview/job/" + request.getParameter("jobID") + "/applicant/" + applicantID[i]).type(MediaType.TEXT_PLAIN).post("");
			// Update application status so that the applicant can send a response (accepted / declined)
			client.back(true);
			
			r = client.path("/users/application/" + applicationID[i] + "/status").type(MediaType.TEXT_PLAIN).put("shortlisted");
		}
		return "redirect:/employer";
	}
	
	/*****************RECRUITMENT PROCESS -> FINISHED INTERVIEWS **********************************************************/
	@RequestMapping(value="finishedInterview", method = RequestMethod.POST)
	public String finishedInterviews(HttpServletRequest request, Model model){
		WebClient client = getClient();
		ObjectMapper mapper = new ObjectMapper();
		
		r = client.path("/users/employer/job/" + request.getParameter("jobSelect")).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		JobBean job = new JobBean();
		try {
			job = mapper.readValue(r.readEntity(String.class), JobBean.class);
		} catch (JsonParseException e) { e.printStackTrace();
		} catch (JsonMappingException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}
		model.addAttribute("job", job);
		
		client.back(true);
		r = client.path("/users/interview/job/" + request.getParameter("jobSelect")).type(MediaType.APPLICATION_JSON).get();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		System.out.println(r.readEntity(String.class));
		List<String> application = new ArrayList<String>();
		try {
			application = mapper.readValue(r.readEntity(String.class), new TypeReference<List<String>>() {});
		} catch (JsonParseException e) { e.printStackTrace();
		} catch (JsonMappingException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}
		List<ProfileBean> profiles = new ArrayList<ProfileBean>();
		for(int i=0; i<application.size(); i++){
			client.back(true);
			r = client.path("/users/applicant/" + application.get(i)).type(MediaType.APPLICATION_JSON).get();
			System.out.println(r.getStatusInfo().getReasonPhrase());
			System.out.println(r.readEntity(String.class));
			ProfileBean profile = new ProfileBean();
			try {
				profile = mapper.readValue(r.readEntity(String.class), ProfileBean.class);
			} catch (JsonParseException e) { e.printStackTrace();
			} catch (JsonMappingException e) { e.printStackTrace();
			} catch (IOException e) { e.printStackTrace();
			}
			profiles.add(profile);
		}
		model.addAttribute("profiles", profiles);
		return "selectCandidate";
	}
	
	/*****************ARCHIVE - BOOKKEEPING - CANDIDATE SELECTED **********************************************************/
	@RequestMapping(value="archive", method = RequestMethod.POST)
	public String bookkeeping(HttpServletRequest request, Model model){
		WebClient client = getClient();
		r = client.path("/users/interview/job/" + request.getParameter("jobID")).type(MediaType.APPLICATION_JSON).delete();
		client.back(true);
		r = client.path("/users/employer/job/" + request.getParameter("jobID") + "/status").type(MediaType.APPLICATION_JSON).put("closed");
		return "redirect:/employer";
	}
	
	/*****************ACCEPT INTERVIEW AFTER BEING SHORTLISTED **********************************************************/
	@RequestMapping(value="acceptInvite", method = RequestMethod.POST)
	public String acceptInvite(HttpServletRequest request, RedirectAttributes redirect){
		WebClient client = getClient();
		r = client.path("/users/applicant/" + request.getParameter("appID") + "/job/" + request.getParameter("jobID") + "/status").type(MediaType.TEXT_PLAIN).put("accepted-invitation");
		if(r.getStatusInfo().getReasonPhrase().equals("OK")){
			redirect.addFlashAttribute("inviteSuccess", "You have successfully replied to the interview");
		}
		else{
			redirect.addFlashAttribute("inviteError", "There was an error responding");
		}
		return "redirect:/applicant#apply";
	}
	
	/*****************REJECT INTERVIEW AFTER BEING SHORTLISTED **********************************************************/
	@RequestMapping(value="rejectInvite", method = RequestMethod.POST)
	public String rejectInvite(HttpServletRequest request, RedirectAttributes redirect){
		WebClient client = getClient();
		r = client.path("/users/applicant/" + request.getParameter("appID") + "/job/" + request.getParameter("jobID")).delete();
		System.out.println(r.getStatusInfo().getReasonPhrase());
		client.back(true);
		r = client.path("/users/applicant/" + request.getParameter("appID") + "/job/" + request.getParameter("jobID") + "/status").type(MediaType.TEXT_PLAIN).put("rejected-invitation");
		if(r.getStatusInfo().getReasonPhrase().equals("OK")){
			redirect.addFlashAttribute("inviteSuccess", "You have successfully replied to the interview");
		}
		else{
			redirect.addFlashAttribute("inviteError", "There was an error responding");
		}
		return "redirect:/applicant#apply";
	}
	
	/*****************JOB ALERT ******************************************************************************/
	@RequestMapping(value="jobAlert", method = RequestMethod.POST)
	public String jobAlert(HttpSession session, HttpServletRequest request, RedirectAttributes redirect, Model model){
		if(session.getAttribute("userSession")==null){
			model.addAttribute("user", new UserBean());
			model.addAttribute("errorHome", "Can't Access home page without login");
			return "/login";
		}
		WebClient client = getClient();
		r = client.path("/jobalerts").query("keyword", request.getParameter("keyword")).query("sorted", request.getParameter("sorted")).query("email", request.getParameter("email"))
				.get();
		System.out.println("jobALert test after: " + r.getStatusInfo().getReasonPhrase());
		if(r.getStatusInfo().getReasonPhrase().equals("OK")){
			redirect.addFlashAttribute("dataSuccess", "Successfully created alert");
		}
		else{
			redirect.addFlashAttribute("dataError", "Failed to create alert");
		}
		return "redirect:/applicant";
	}
}
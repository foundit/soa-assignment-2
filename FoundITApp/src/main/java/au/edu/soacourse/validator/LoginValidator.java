package au.edu.soacourse.validator;

import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import au.edu.soacourse.model.UserBean;
import au.edu.soacourse.service.UserService;

public class LoginValidator implements Validator {

	private UserService userService;

	@Autowired
	public LoginValidator(UserService userService){
		this.userService = userService;
	}
	
	public boolean supports(Class<?> arg0) {
		return UserBean.class.isAssignableFrom(arg0);
	}

	public void validate(Object arg0, Errors arg1) {
		UserBean user = (UserBean) arg0;
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "email", "error.email");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "password", "error.password");
		if(!userService.verifyUser(user.getEmail(), user.getPassword())){
			arg1.rejectValue("email", "login.email");
			arg1.rejectValue("password", "login.password");
		}
	}

}

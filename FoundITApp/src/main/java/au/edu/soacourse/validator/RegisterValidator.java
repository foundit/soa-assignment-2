package au.edu.soacourse.validator;

import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import au.edu.soacourse.model.UserBean;
import au.edu.soacourse.service.UserService;

public class RegisterValidator implements Validator {

	private UserService userService;

	@Autowired
	public RegisterValidator(UserService userService){
		this.userService = userService;
	}
	
	@Override
	public boolean supports(Class<?> arg0) {
		return UserBean.class.isAssignableFrom(arg0);
	}

	@Override
	public void validate(Object arg0, Errors arg1) {
		UserBean user = (UserBean) arg0;
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "email", "error.email");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "password", "error.password");
		Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		if(!pattern.matcher(user.getEmail()).matches()){
			arg1.rejectValue("email", "valid.email");
		}
	}

}

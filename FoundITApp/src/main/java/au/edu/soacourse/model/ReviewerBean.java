package au.edu.soacourse.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ReviewerBean{
	/**
	 * 
	 */
	private int userID;
	private int reviewerID;
	private String username;
	private String password;
	private int companyID;
	private String skills;
	
	public ReviewerBean(){
		this.skills="";
	}
	
	public ReviewerBean(int reviewerID, int companyID, int userID, String username, String password, String skills){
		this.companyID = companyID;
		this.userID = userID;
	}
	
	public int getCompanyID() {
		return companyID;
	}
	public void setCompanyID(int companyID) {
		this.companyID = companyID;
	}
	
	public String getSkills() {
		return skills;
	}
	public void setSkills(String skills) {
		this.skills = skills;
	}
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getReviewerID() {
		return reviewerID;
	}

	public void setReviewerID(int reviewerID) {
		this.reviewerID = reviewerID;
	}
}

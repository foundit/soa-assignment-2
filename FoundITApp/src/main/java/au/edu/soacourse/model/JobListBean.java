package au.edu.soacourse.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class JobListBean{
	/**
	 * 
	 */
	private List<JobBean> allPostings;
	
	public List<JobBean> getAllPostings() {
		return allPostings;
	}
	public void setAllPostings(List<JobBean> allPostings) {
		this.allPostings = allPostings;
	}
}

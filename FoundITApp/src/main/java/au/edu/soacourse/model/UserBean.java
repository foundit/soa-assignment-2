package au.edu.soacourse.model;

import java.io.Serializable;


public class UserBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7820612142409801260L;
	private String userID;
	private String email;
	private String password;
	private String role;
	
	public UserBean(){
	}
	
	public UserBean(String userID, String email, String password, String role){
		this.userID = userID;
		this.email = email;
		this.password = password;
		this.role = role;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}

package au.edu.soacourse.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class HiringTeam{
	/**
	 * 
	 */
	private List<ReviewerBean> team;
	
	public List<ReviewerBean> getTeam() {
		return team;
	}
	public void setTeam(List<ReviewerBean> team) {
		this.team = team;
	}
	

}

package au.edu.soacourse.model;

public class ApplicationBean {
	private int applicationID;
	private int jobID;
	private int applicantID;
	private String coverLetter;
	private String status;
	
	public ApplicationBean(){	
	}
	
	public ApplicationBean(int applicationID, int jobID, int applicantID, String coverLetter, String status) {
		this.applicationID = applicationID;
		this.jobID = jobID;
		this.applicantID = applicantID;
		this.coverLetter = coverLetter;
		this.status = status;
	}

	public int getApplicationID() {
		return applicationID;
	}

	public void setApplicationID(int applicationID) {
		this.applicationID = applicationID;
	}

	public int getJobID() {
		return jobID;
	}

	public void setJobID(int jobID) {
		this.jobID = jobID;
	}

	public int getApplicantID() {
		return applicantID;
	}

	public void setApplicantID(int appicantID) {
		this.applicantID = appicantID;
	}

	public String getCoverLetter() {
		return coverLetter;
	}

	public void setCoverLetter(String coverLetter) {
		this.coverLetter = coverLetter;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}

package au.edu.soacourse.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ReviewListBean{
	/**
	 * 
	 */
	private List<ReviewBean> reviews;

	public List<ReviewBean> getReviews() {
		return reviews;
	}

	public void setReviews(List<ReviewBean> reviews) {
		this.reviews = reviews;
	}
	
}

package au.edu.soacourse.model;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CompanyBean{

	private int companyID;
	private int managerID;
	private String email;
	private String name;
	private String description;
	private String website;
	private String address;
	private String industry;
	private String typecomp;
	private String size;
	
	public CompanyBean(){
		this.email="";
		this.name="";
		this.description="";
		this.website="";
		this.address="";
		this.industry="";
		this.typecomp="";
		this.size="";
	}
	
	public CompanyBean(int companyID, int managerID, String email, String name, String description, String website, 
			String address, String industry, String type, String size){
		this.email = email;
		this.companyID = companyID;
		this.managerID = managerID;
		this.name = name;
		this.description = description;
		this.website = website;
		this.address = address;
		this.industry = industry;
		this.typecomp = type;
		this.size = size;
	}

	public int getCompanyID() {
		return companyID;
	}

	public void setCompanyID(int companyID) {
		this.companyID = companyID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getTypecomp() {
		return typecomp;
	}

	public void setTypecomp(String type) {
		this.typecomp = type;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public int getManagerID() {
		return managerID;
	}

	public void setManagerID(int managerID) {
		this.managerID = managerID;
	}
	
}

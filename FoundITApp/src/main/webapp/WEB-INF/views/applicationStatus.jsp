<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Grayscale - Start Bootstrap Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="resources/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="resources/css/grayscale.css" rel="stylesheet">
    
    <!-- Datatables CSS -->
	<link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="resources/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    
    <c:url var="controller" value="/company"/>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
	
    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="<c:url value="/employer"/>" >
                    <i class="fa fa-play-circle"></i>  <span class="light">Go To Home Page: </span> <span class="small">${userSession.email}</span>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                    	<c:if test="${ not empty userSession.email }"><a href="<c:url value="/logout"/>" >Logout</a></c:if>
                   	</li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

	<!-- Profile Section -->
    <section id="profile" class="container content-section text-center">
        <div class="profile-section">
            <div class="col-lg-10 col-lg-offset-1" id="YourProfile">
            	<h3>Recruitment Progress for: <a href="#showJobModal" data-toggle="modal">${job.title}</a></h3>
            	
            	<table id="reviewProgress" class="table" cellspacing="0" width="100%">
            		<thead><tr><th>Review ID</th><th>Reviewer ID</th><th>Application ID</th><th>Comments</th><th>Decision</th></tr></thead>
            		<c:forEach items="${reviews.reviews}" var="r">
            			<tr>
            				<td class="jobValue">${r.reviewID}</td>
            				<td class="jobValue">${r.reviewerID}</td>
            				<td class="jobValue"><a data-toggle="modal" class="d" data-id="${r.applicationID}" onClick="goApplicationAJAX(this);">
            						<i class="fa fa-user fa-1g" style="font-size:175%"></i>
            						</a> &nbsp;&nbsp;${r.applicationID}</td>
            				<td class="jobValue">${r.comments}</td>
            				<td class="jobValue">${r.decision}</td>
            				<c:if test="${r.decision=='Unreviewed'}"><c:set var="finalise" value="false"/></c:if>
            			</tr>
            		</c:forEach>
            	</table>
            	<br><br>
            	<c:if test="${ empty finalise }">
            	<form:form action="finalise" method="POST">
            		<c:forEach items="${applicationID}" var="id">
            			<input type='hidden' name="applicationID" value="${id}">
            		</c:forEach>
            		<input type="hidden" name="jobID" value="${job.jobID}">
            		<button class="btn btn-default btn-lg" >Finalise List</button>
            	</form:form>
            	</c:if>
            </div>
        </div>
    </section>
    
    <!-- Modal for Showing User Details -->
	<div id="showApplicantModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-body">
	      	<h3 style="color:blue">User: <span class="firstname"></span> <span class="lastname"></span></h3>
	      	<table class="table-modal">
	      		<tr><td>EMAIL: </td><td class="email"></td></tr>
	      		<tr><td>ADDRESS: </td><td class="address"></td></tr>
	      		<tr><td>CURRENT POSITION: </td><td class="position"></td></tr>
	      		<tr><td>EDUCATION: </td><td class="education"></td></tr>
	      		<tr><td>EXPERIENCE: </td><td class="experience"></td></tr>
	      		<tr><td>COVER LETTER: </td><td></td></tr>
	      		<tr><td colspan="2">
	      		<div class="coverLetter" style="color:#000;font-family:sans-serif;font-size:80%;white-space: pre-line;padding:5px;height: 300px;overflow:scroll;"></div></td></tr>
	      	</table>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	
	  </div>
	</div>
	
	<!-- Modal for Job Recruitment Process-->
	<div id="showJobModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-body">
	      	<h3 style="color:blue">JobID = ${job.jobID}: </h3>
	      	<table class="table-modal">
	      		<tr><td>TITLE: </td><td>${job.title}</td></tr>
	      		<tr><td>SALARY: </td><td>${job.salary}</td></tr>
	      		<tr><td>DETAILS: </td><td>${job.details}</td></tr>
	      		<tr><td>POSITION: </td><td>${job.typePos}</td></tr>
	      		<tr><td>LOCATION: </td><td>${job.location}</td></tr>
	      		<tr><td>SKILLS REQ.: </td><td>${job.skills}</td></tr>
	      	</table>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	
	  </div>
	</div>
	
    <!-- Footer -->
    <footer>
        <div class="container text-center">
            <p>Copyright &copy; FoundIT App 2016</p>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="resources/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="resources/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="resources/js/jquery.easing.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="resources/js/grayscale.js"></script>
	
	<!--  DataTables Javascript -->
    <script src="resources/js/jquery.dataTables.min.js"></script>
    
    <!--  Initialise any relevant scripts -->
    <script src="resources/main.js"></script>
</body>


</html>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<link href="<c:url value="/resources/job.css" />" rel="stylesheet" type="text/css">
</head>
<body>
<div class="job-page">
  <div class="form">
    <form:form class="login-form" method="POST" modelAttribute="job" action="editJob">
    	<h1>EDIT JOB POSTING</h1>
    	<table>
    	<tr><td class="t">Title:</td></tr>
    	<tr><td><form:input path="title" placeholder="Job Title"/></td></tr>
    	<tr><td class="t">Status:</td></tr>
    	<tr><td><form:input path="status" placeholder="Position Status"/></td></tr>
    	<tr><td class="t">Description:</td></tr>
    	<tr><td><form:textarea path="details" placeholder="Job Description"/></td></tr>
    	<tr><td class="t">Type:</td></tr>
    	<tr><td><form:input path="typePos" placeholder="Job Type" /></td></tr>
    	<tr><td class="t">Salary:</td></tr>
    	<tr><td><form:input path="salary" placeholder="Salary Range" /></td></tr>
		<tr><td class="t">Location:</td></tr>
    	<tr><td><form:input path="location" placeholder="Job Locaion" /></td></tr>
    	<tr><td class="t">Skills:</td></tr>
    	<tr><td><form:textarea path="skills" placeholder="Profession Skills"/></td></tr>
    	</table><br>
    	<form:hidden path="companyID" value="${job.companyID}"/>
    	<form:hidden path="jobID" value="${job.jobID}"/>
    	<button>SAVE</button>&nbsp; <button onClick="goBack()">Cancel</button>
    </form:form>
    
  </div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-2.2.3.js"   integrity="sha256-laXWtGydpwqJ8JA+X9x2miwmaiKhn8tVmOVEigRNtP4="   crossorigin="anonymous"></script>
</body>

</html>
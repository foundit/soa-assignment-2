<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Grayscale - Start Bootstrap Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="resources/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="resources/css/grayscale.css" rel="stylesheet">
    
    <!-- Datatables CSS -->
	<link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="resources/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    
    <c:url var="controller" value="/company"/>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
	
    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">
                    <i class="fa fa-play-circle"></i>  <span class="light">Welcome</span> <span class="small">${userSession.email}</span>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#profile">Profile</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#search">Search Results</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#saved">Saved Jobs</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#apply">Jobs Applied To</a>
                    </li>
                    <li>
                    	<c:if test="${ not empty userSession.email }"><a href="<c:url value="/logout"/>">Logout</a></c:if>
                    	<c:if test="${ empty userSession.email }"><a href="<c:url value="/login"/>">Login</a></c:if>
                   	</li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <!--  <h2 class="brand-heading">FoundIT App</h2>
                        <p class="intro-text">Discover & Apply to Jobs as Applicant<br>Find qualified workers for your company!!</p>
                        a<br>a<br>-->
                        <div class="header-div"><h2 style="color: #000">Search for Job Postings</h2>
                        <span style="color:#0000c0;font-size:18px;font-family:Montserrat">
                        FIELD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        SEARCH&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        STATUS</span>
                        <form:form class="search" action="search" method="GET">
                        	<select name="type">
                        		<option value="keyword">Keyword</option>
                        		<option value="skills">Skills</option>
                        	</select>
                        	<input type="text" name="search" placeholder="Search Here"/>
                        	<select name="status">
                        		<option value="any">ANY</option>
                        		<option value="open">OPEN</option>
                        		<option value="closed">CLOSED</option>
                        	</select>
                        	<div><br><button class="btn btn-default btn-lg"><i class="fa fa-search" aria-hidden="true"></i> Search</button></div>
                        </form:form>
                        <br>
                        <c:if test="${ not empty dataSuccess }"><h4 class="success">${ dataSuccess }</h4></c:if>
                		<c:if test="${ not empty dataError }"><h4 class="error">${ dataError }</h4></c:if>
                        </div>
                        <div>
                        	<br>
                        	
	                        <a href="#addJobAlertModal" data-toggle="modal" class="btn btn-default btn-lg">
            					<i class="fa fa-plus fa-1g"></i><span class="network-name"> &nbsp;Create Job Alert</span></a>
                        </div>
                        <br>
                        <a href="#profile" class="btn btn-circle page-scroll">
                            <i class="fa fa-angle-double-down animated" style="font-size: 125%"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    
    <!-- Modal for Add new Member -->
	<div id="addJobAlertModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	  
	    <!-- Modal content-->
	    <div class="modal-content">
	    <form:form method="POST" class="form-modal" action="jobAlert">
	      <div class="modal-body">
	      	<h3 style="color:blue">Add a new Job Alert:</h3>
	        	<table class="table-modal">
	        		<tr><td>EMAIL:</td><td><input type="text" name="email" value="${userSession.email}"></td></tr>
	        		<tr><td>KEYWORD:</td><td><input type="text" name="keyword"></td></tr>
	        		<tr><td>SORTED?:</td><td><input id=true" type="radio" name="sorted" value="true" checked="checked"><label for="true">True</label>
	        			<input id=false" type="radio" name="sorted" value="false"><label for="false">False</label></td></tr>
	        	</table>
	      </div>
	      <div class="modal-footer">
	      	<button class="btn btn-default">Add</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	      </form:form>
	    </div>
	  </div>
	</div>

	<!-- Profile Section -->
    <section id="profile" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1" id="YourProfile">
                <h2>Your Profile</h2>
                <c:if test="${ not empty editProfileSuccess }"><h4 class="success">${ editProfileSuccess }</h4></c:if>
                <c:if test="${ not empty editProfileError }"><h4 class="error">${ editProfileError }</h4></c:if>
                <div class="row">
				  <div class="col-md-6">
					<table class="table">
						<tbody>
							<tr><td class="profileTitle">USER ID:</td><td class="profileValue">${app.userID}</td></tr>
							<tr><td class="profileTitle">PROFILE ID:</td><td class="profileValue">${app.profileID}</td></tr>
							<tr><td align="left">EMAIL:</td><td class="profileValue">${app.email}</td></tr>
							<tr><td align="left">FIRST NAME:</td><td class="profileValue">${app.firstName}</td></tr>
							<tr><td align="left">LAST NAME:</td><td class="profileValue">${app.lastName}</td></tr>
							<tr><td align="left">ADDRESS:</td><td class="profileValue">${app.address}</td></tr>
						</tbody>
					</table>
				  </div>
				  <div class="col-md-6">
					<table class="table">
						<tbody>
							<tr><td align="left" width="100px">CURRENT POS:</td><td class="profileValue">${app.currentPosition}</td></tr>
							<tr><td align="left">EDUCATION:</td><td class="profileValue">${app.education}</td></tr>
							<tr><td align="left">EXPERIENCE:</td><td class="profileValue">${app.experience}</td></tr>
						</tbody>
					</table>
				  </div>
				</div>
				<div class="row">
	            	<a href="#profile" id="EditClick" class="btn btn-default btn-lg"><i class="fa fa-pencil-square-o"></i> <span class="network-name">Edit Profile</span></a>
	        	</div>
	        	<div class="row">
	        		<br>
		            <a href="#page-top" class="btn btn-circle page-scroll">
						<i class="fa fa-angle-double-up animated" style="font-size: 125%"></i>
		            </a>
		            <a href="#search" class="btn btn-circle page-scroll">
						<i class="fa fa-angle-double-down animated" style="font-size: 125%"></i>
		            </a>
		        </div>
            </div>
            <div class="col-lg-10 col-lg-offset-1" id="SlideEditProfile">
                <h2>Your Profile Edit Mode Activated</h2>
                <form:form method="POST" class="form" modelAttribute="app" action="save?value=applicant">
                <div class="row">
				  <div class="col-md-6">
					<table class="table">
						<tbody>
							<tr><td align="left"  width="150px">USER ID:</td>
								<td align="left"><form:hidden path="userID"/>${app.userID}</td></tr>
							<tr><td align="left">EMAIL:</td>
								<td align="left"><form:hidden path="email"/>${app.email}</td></tr>
							<tr><td align="left">FIRST NAME:</td>
								<td align="left"><form:input path="firstName"/></td></tr>
							<tr><td align="left">LAST NAME:</td>
								<td align="left"><form:input path="lastName"/></td></tr>
							<tr><td align="left">ADDRESS:</td>
								<td align="left"><form:input path="address"/></td></tr>
							<tr><td align="left">CURRENT POS:</td>
								<td align="left"><form:input path="currentPosition"/></td></tr>
						</tbody>
					</table>
				  </div>
				  <div class="col-md-6">
					<table class="table">
						<tbody>
							<tr><td align="left" width="100px">EDUCATION:</td>
								<td align="left"><form:textarea path="education"/></td></tr>
							<tr><td align="left">EXPERIENCE:</td>
								<td align="left"><form:textarea path="experience"/></td></tr>
						</tbody>
					</table>
				  </div>
				</div>
				<div class="row">
	            	<a href="#profile" id="SaveClick" class="btn btn-default btn-lg"><i class="fa fa-ban"></i> <span class="network-name">Cancel</span></a>
	        		&nbsp;
	        		<button class="btn btn-default btn-lg"><i class="fa fa-floppy-o"></i> <span class="network-name">Save Profile</span></button>
	        	</div>
	        	</form:form>
            </div>
        </div>
    </section>
    
    <!-- Search Section -->
    <section id="search" class="container search-section text-center">
        <div class="row">
            <div class="col-lg-12">
            	<h4>Search Again:</h4>
            	<form:form class="search" action="search" method="GET">
					<select name="type">
						<option value="keyword">Keyword</option>
						<option value="skills">Skills</option>
					</select>
					<input type="text" name="search" placeholder="Search Here"/>
					<select name="status">
						<option value="any">ANY</option>
						<option value="open">OPEN</option>
						<option value="closed">CLOSED</option>
					</select>
					<button class="btn btn-default btn-lg"><i class="fa fa-search" aria-hidden="true"></i></button>
				</form:form>
				<br><br>
                <table id="jobPosting" class="table" cellspacing="0" width="100%">
                	<thead><tr>
                		<th style="font-size: 75%">Cmp.</th><th>Title</th><th>Description</th><th>Salary</th><th>Position</th><th>Skills</th><th>Location</th><th>Status</th>
            			<th style="font-size: 75%">Save?</th><th style="font-size: 75%">Apply?</th>
            		</tr></thead>
                	<!--  <tfoot><tr>
                		<th>ID</th><th>Title</th><th>Salary</th><th>Position</th><th>Location</th><th>Status</th>
            		</tr></tfoot>-->
            		<c:forEach items="${search.allPostings}" var="job">
            			<tr>
            				<td class="jobValue"><a data-toggle="modal" class="d" data-company="${job.companyID}" onClick="goAJAX(this);">
            						<i class="fa fa-building-o fa-1g" style="font-size:175%"></i>
            						</a></td>
            				<td class="jobValue">${job.title}</td>
            				<td class="jobValue">
            					<div style="float:left;text-align:left">${fn:substring(job.details,0,10)}...</div>
            					<div style="float:right;text-align:right;padding-right:8px">
            						<a href="#showDescriptionModal" data-toggle="modal" class="d" data-details="${job.details}">
            						<i class="fa fa-caret-square-o-right fa-1g" style="font-size:175%"></i>
            						</a>
            					</div>
            				</td>
            				<td class="jobValue">${job.salary}</td>
            				<td class="jobValue">${job.typePos}</td>
            				<td class="jobValue"><div style="float:left;text-align:left">${fn:substring(job.skills,0,10)}...</div>
            					<div style="float:right;text-align:right;padding-right:8px">
            						<a href="#showSkillsModal" data-toggle="modal" class="d" data-skills="${job.skills}">
            						<i class="fa fa-caret-square-o-right fa-1g" style="font-size:175%"></i>
            						</a>
            					</div></td>
            				<td class="jobValue">${job.location}</td>
            				<td class="jobValue">${job.status}</td>
            				<td width="20px"><c:if test="${job.status!='Job Alert'}"><a href="#saveConfirmationModal" data-toggle="modal" class="d" data-id="${job.jobID}">
            						<i class="fa fa-floppy-o fa-1g" style="font-size:150%"></i></a></c:if></td>
            				<td width="10px"><c:if test="${job.status=='open'}">
            					<a href="#applyConfirmationModal" data-toggle="modal" class="d" data-id="${job.jobID}" data-title="${job.title}">
            					<i class="fa fa-external-link fa-1g" style="font-size:150%"></i></a>
            				</c:if></td>
            			</tr>
            		</c:forEach>
                </table>
            </div>
            <a href="#profile" class="btn btn-circle page-scroll">
				<i class="fa fa-angle-double-up animated" style="font-size: 125%"></i>
            </a>
            <a href="#saved" class="btn btn-circle page-scroll">
				<i class="fa fa-angle-double-down animated" style="font-size: 125%"></i>
            </a>
        </div>
    </section>
    
    <!-- Modal for Description-->
	<div id="showDescriptionModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-body">
	      	<h3 style="color:blue">Description: </h3>
	        <div class="e" style="color:#000;font-family:Montserrat;font-size:150%"></div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	
	  </div>
	</div>
	
	<!-- Modal for Skills Description-->
	<div id="showSkillsModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-body">
	      	<h3 style="color:blue">Skills: </h3>
	        <div class="e" style="color:#000;font-family:Montserrat;font-size:150%"></div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	
	  </div>
	</div>
	
	<!-- Modal for Save Confirmation -->
	<div id="saveConfirmationModal" class="modal fade" role="dialog">
	  <div class="modal-sm">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-body">
	      	<h3 style="color:blue">Confirmation of Request</h3>
	      	<span style="color:#000;font-family:Montserrat;font-size:150%">Are you sure you want to save job posting?</span>
	      </div>
	      <div class="modal-footer">
	      	<form:form action="saveJobPosting" method="POST">
	      		<input type="hidden" name="jobID">
	      		<button class="btn btn-default">Proceeed</button>
	      		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	      	</form:form>
	      </div>
	    </div>
	  </div>
	</div>
	
	<!-- Modal for Save Confirmation -->
	<div id="applyConfirmationModal" class="modal fade" role="dialog">
	  <div class="modal-sm">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-body">
	      	<h3 style="color:blue">Confirmation of Application</h3>
	      	<span style="color:#000;font-family:Montserrat;font-size:150%">Are you sure you want to apply to "<span class="e"></span>"?</span>
	      </div>
	      <div class="modal-footer">
	      	<form:form action="apply" method="GET">
	      		<input type="hidden" name="jobID">
	      		<input type="hidden" name="appID" value="${app.userID}">
	      		<button class="btn btn-default">Proceeed</button>
	      		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	      	</form:form>
	      </div>
	    </div>
	  </div>
	</div>
	
	<!-- Modal for Company Description-->
	<div id="showCompanyModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-body">
	      	<h3 style="color:blue">Company: </h3>
	      	<table class="table-modal">
	      		<tr><td>EMPLOYER EMAIL: </td><td class="email"></td></tr>
	      		<tr><td>COMPANY NAME: </td><td class="company"></td></tr>
	      		<tr><td>WEBSITE: </td><td class="website"></td></tr>
	      		<tr><td>ADDRESS: </td><td class="address"></td></tr>
	      		<tr><td>INDUSTRY: </td><td class="industry"></td></tr>
	      		<tr><td>TYPE: </td><td class="type"></td></tr>
	      		<tr><td>SIZE: </td><td class="size"></td></tr>
	      		<tr><td>DESCRIPTION: </td><td class="description"></td></tr>
	      	</table>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	
	  </div>
	</div>

    <!-- Saved Jobs Section -->
    <section id="saved" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-12">
                <h2>Your Saved Jobs</h2>
                <c:if test="${ not empty savePostingSuccess }"><h4 class="success">${ savePostingSuccess }</h4></c:if>
                <c:if test="${ not empty savePostingError }"><h4 class="error">${ savePostingError }</h4></c:if>
                <c:if test="${ not empty deletePostingSuccess }"><h4 class="success">${ deletePostingSuccess }</h4></c:if>
                <c:if test="${ not empty deletePostingError }"><h4 class="error">${ deletePostingError }</h4></c:if>
                <table id="jobPosting" class="table" cellspacing="0" width="100%">
                	<thead><tr>
                		<th style="font-size: 75%">Cmp.</th><th>Title</th><th>Description</th><th>Salary</th><th>Position</th><th>Skills</th>
                		<th>Location</th><th>Status</th><th style="font-size: 75%">Apply?</th><th style="font-size: 75%">Delete?</th>
            		</tr></thead>
                	<!--  <tfoot><tr>
                		<th>ID</th><th>Title</th><th>Salary</th><th>Position</th><th>Location</th><th>Status</th>
            		</tr></tfoot>-->
            		<c:forEach items="${savedJobs.allPostings}" var="job">
            			<tr>
            				<td class="jobValue"><a data-toggle="modal" class="d" data-company="${job.companyID}" onClick="goAJAX(this);">
            						<i class="fa fa-building-o fa-1g" style="font-size:175%"></i>
            						</a></td>
            				<td class="jobValue">${job.title}</td>
            				<td class="jobValue">
            					<div style="float:left;text-align:left">${fn:substring(job.details,0,20)}...</div>
            					<div style="float:right;text-align:right;padding-right:8px">
            						<a href="#showDescriptionModal" data-toggle="modal" class="d" data-details="${job.details}">
            						<i class="fa fa-caret-square-o-right fa-1g" style="font-size:175%"></i>
            						</a>
            					</div>
            				</td>
            				<td class="jobValue">${job.salary}</td>
            				<td class="jobValue">${job.typePos}</td>
            				<td class="jobValue"><div style="float:left;text-align:left">${fn:substring(job.skills,0,20)}...</div>
            					<div style="float:right;text-align:right;padding-right:8px">
            						<a href="#showSkillsModal" data-toggle="modal" class="d" data-skills="${job.skills}">
            						<i class="fa fa-caret-square-o-right fa-1g" style="font-size:175%"></i>
            						</a>
            					</div></td>
            				<td class="jobValue">${job.location}</td>
            				<td class="jobValue">${job.status}</td>
            				<td><c:if test="${job.status=='open'}">
            					<a href="#applyConfirmationModal" data-toggle="modal" class="d" data-id="${job.jobID}" data-title="${job.title}">
            					<i class="fa fa-external-link fa-1g" style="font-size:150%"></i></a>
            				</c:if></td>
            				<td>
            					<a href="#deleteSavedPostingModal" data-toggle="modal" class="d" data-title="${job.title}" data-id="${job.jobID}">
            						<i class="fa fa-trash fa-1g" style="font-size:150%"></i>
            				</a></td>
            			</tr>
            		</c:forEach>
                </table>
            </div>
            <a href="#search" class="btn btn-circle page-scroll">
				<i class="fa fa-angle-double-up animated" style="font-size: 125%"></i>
            </a>
            <a href="#apply" class="btn btn-circle page-scroll">
				<i class="fa fa-angle-double-down animated" style="font-size: 125%"></i>
            </a>
        </div>
    </section>
    
    <!-- Modal for Save Confirmation -->
	<div id="deleteSavedPostingModal" class="modal fade" role="dialog">
	  <div class="modal-sm">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-body">
	      	<h3 style="color:blue">Delete Saved Job?</h3>
	      	<span style="color:#000;font-family:Montserrat;font-size:150%">Are you sure you want to delete "<span class="e"></span>"?</span>
	      </div>
	      <div class="modal-footer">
	      	<form:form action="deleteSavedJobPosting" method="POST">
	      		<input type="hidden" name="jobID">
	      		<input type="hidden" name="appID" value="${app.userID}">
	      		<button class="btn btn-default">Proceeed</button>
	      		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	      	</form:form>
	      </div>
	    </div>
	  </div>
	</div>
    
    <!-- Applied For Jobs Section -->
    <section id="apply" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-12">
                <h2>Your Currently Applied For Jobs</h2>
                <c:if test="${ not empty editLetterSuccess }"><h4 class="success">${ editLetterSuccess }</h4></c:if>
                <c:if test="${ not empty editLetterError }"><h4 class="error">${ editLetterError }</h4></c:if>
                <c:if test="${ not empty applySuccess }"><h4 class="success">${ applySuccess }</h4></c:if>
                <c:if test="${ not empty applyError }"><h4 class="error">${ applyError }</h4></c:if>
                <c:if test="${ not empty WithdrawSuccess }"><h4 class="success">${ WithdrawSuccess }</h4></c:if>
                <c:if test="${ not empty WithdrawError }"><h4 class="error">${ WithdrawError }</h4></c:if>
                <c:if test="${ not empty ArchiveSuccess }"><h4 class="success">${ ArchiveSuccess }</h4></c:if>
                <c:if test="${ not empty ArchiveError }"><h4 class="error">${ ArchiveError }</h4></c:if>
                <c:if test="${ not empty inviteSuccess }"><h4 class="success">${ inviteSuccess }</h4></c:if>
                <c:if test="${ not empty inviteError }"><h4 class="error">${ inviteError }</h4></c:if>
                <table id="jobPosting" class="table" cellspacing="0" width="100%">
                	<thead><tr>
                		<th style="font-size: 75%">Cmp.</th><th>Title</th><th>Description</th><th>Salary</th><th>Position</th><th>Skills</th>
                		<th>Location</th><th>Status</th><th style="font-size: 75%">Cover Let.</th><th style="font-size: 75%">Withdraw</th>
                		<th style="font-size: 75%">App. Status</th>
            		</tr></thead>
                	<!--  <tfoot><tr>
                		<th>ID</th><th>Title</th><th>Salary</th><th>Position</th><th>Location</th><th>Status</th>
            		</tr></tfoot>-->
            		<c:forEach items="${appliedJobs.allPostings}" var="job">
            			<tr>
            				<td class="jobValue"><a data-toggle="modal" class="d" data-company="${job.companyID}" onClick="goAJAX(this);">
            						<i class="fa fa-building-o fa-1g" style="font-size:175%"></i>
            						</a></td>
            				<td class="jobValue">${job.title}</td>
            				<td class="jobValue">
            					<div style="float:left;text-align:left">${fn:substring(job.details,0,20)}...</div>
            					<div style="float:right;text-align:right;padding-right:4px">
            						<a href="#showDescriptionModal" data-toggle="modal" class="d" data-details="${job.details}">
            						<i class="fa fa-caret-square-o-right fa-1g" style="font-size:175%"></i>
            						</a>
            					</div>
            				</td>
            				<td class="jobValue">${job.salary}</td>
            				<td class="jobValue">${job.typePos}</td>
            				<td class="jobValue"><div style="float:left;text-align:left">${fn:substring(job.skills,0,20)}...</div>
            					<div style="float:right;text-align:right;padding-right:8px">
            						<a href="#showSkillsModal" data-toggle="modal" class="d" data-skills="${job.skills}">
            						<i class="fa fa-caret-square-o-right fa-1g" style="font-size:175%"></i>
            						</a>
            					</div></td>
            				<td class="jobValue">${job.location}</td>
            				<td class="jobValue">${job.status}</td>
            				<td width="10px"><a data-jobID="${job.jobID}" data-appID="${app.userID}" data-status="${job.status}" onClick="goLetterAJAX(this);">
            					<i class="fa fa-envelope fa-1g" style="font-size:150%"></i></a></td>
            				<td width="10px"><c:if test="${job.status=='open'}"><a href="#withdrawApplicationModal" data-toggle="modal" class="d" data-id="${job.jobID}">
            					<i class="fa fa-trash fa-1g" style="font-size:150%"></i></a></c:if></td>
            				<td width="10px"><a href="#applicationStatusModal" data-toggle="modal" class="d" data-jobid="${job.jobID}" data-appid="${app.userID}" onClick="goStatusAJAX(this);">
            					<i class="fa fa-question-circle fa-1g" style="font-size:150%"></i></a></td>
            			</tr>
            		</c:forEach>
                </table>
            </div>
            <a href="#saved" class="btn btn-circle page-scroll">
				<i class="fa fa-angle-double-up animated" style="font-size: 125%"></i>
            </a>
        </div>
    </section>
    
    <!-- Modal for Cover Letter to Show -->
	<div id="showCoverLetterModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <!-- Modal content-->
	    <div id="coverLetterShow">
		    <div class="modal-content">
		      <div class="modal-body">
		      	<h3 style="color:blue">Cover Letter: </h3>
				<div class="letter" style="color:#000;font-family:sans-serif;font-size:125%;white-space: pre-line;padding:5px;height: 300px;overflow:scroll;"></div>
		      </div>
		      <div class="modal-footer">
		      	<a class="btn btn-default" id="coverLetterEditButton">Edit?</a>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>
		    </div>
	    </div>
	    <div id="coverLetterEdit">
	    	<div class="modal-content">
	    	<form:form action="editLetter" method="POST" class="form-letter">
	    		<div class="modal-body">
		      		<h3 style="color:blue">Edit: </h3>
		      			<input type="hidden" name="jobID">
		      			<input type="hidden" name="status">
	      				<input type="hidden" name="appID" value="${app.userID}">
		      			<textarea class="letter" name="letter" style="color:#000;font-family:sans-serif;font-size:125%;white-space: pre-line;border:2px solid #000;padding:5px;width:100%;height:300px"></textarea>
		      	</div>
		      	<div class="modal-footer">
		      		<button class="btn btn-default" id="coverLetterShowButton">Save</button>
		        	<button type="button" class="btn btn-default" onClick="resetModalLetter();">Cancel</button>
		      	</div>
		    </form:form>
	    	</div>
	    </div>
	  </div>
	</div>
	
	<!-- Modal for Cover Letter to Edit -->
	<div id="editCoverLetterModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-body">
	      	<h3 style="color:blue">Cover Letter Edit: </h3>
			<div class="letter" style="color:#000;font-family:sans-serif;font-size:125%;white-space: pre-line;border:2px solid #000;padding: 5px"></div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>
	
	<!-- Modal for Save Confirmation -->
	<div id="withdrawApplicationModal" class="modal fade" role="dialog">
	  <div class="modal-sm">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-body">
	      	<h3 style="color:blue">Confirmation of Withdrawel</h3>
	      	<span style="color:#000;font-family:Montserrat;font-size:150%">Are you sure you want to withdraw?</span>
	      </div>
	      <div class="modal-footer">
	      	<form:form action="withdrawApplication" method="POST">
	      		<input type="hidden" name="jobID">
	      		<input type="hidden" name="applicantID" value="${app.userID}">
	      		<button class="btn btn-default">Proceeed</button>
	      		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	      	</form:form>
	      </div>
	    </div>
	  </div>
	</div>
	
	<!-- Modal for Save Confirmation -->
	<div id="applicationStatusModal" class="modal fade" role="dialog">
	  <div class="modal-sm">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-body">
	      	<h3 style="color:blue">Application Status: </h3>
	      	<span style="color:#000;font-family:Montserrat;font-size:150%" class="e"></span>
	      </div>
	      <div class="modal-footer" style="width: 100%">
	      <form:form method="POST" action="acceptInvite">
	      	<input type="hidden" name="appID">
	      	<input type="hidden" name="jobID">
			<button id="acceptInvite" class="btn btn-default">Accept</button>
		  </form:form>
		  <form:form method="POST" action="rejectInvite">
		  	<input type="hidden" name="appID">
	      	<input type="hidden" name="jobID">
			<button id="rejectInvite" class="btn btn-default">Reject</button>
		  </form:form>
		  <form:form method="POST" action="archiveApplication">
		  	<input type="hidden" name="appID">
	      	<input type="hidden" name="jobID">
		  	<button id="archive" class="btn btn-default">Archive</button>
		  </form:form>
	      	<button id="cancel" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	      </div>
	    </div>
	  </div>
	</div>
    
    <!-- Footer -->
    <footer>
        <div class="container text-center">
            <p>Copyright &copy; FoundIT App 2016</p>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="resources/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="resources/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="resources/js/jquery.easing.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="resources/js/grayscale.js"></script>
	
	<!--  DataTables Javascript -->
    <script src="resources/js/jquery.dataTables.min.js"></script>
    
    <!--  Initialise any relevant scripts -->
    <script src="resources/main.js"></script>
</body>


</html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Grayscale - Start Bootstrap Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="resources/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="resources/css/grayscale.css" rel="stylesheet">
    
    <!-- Datatables CSS -->
	<link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="resources/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    
    <c:url var="controller" value="/company"/>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
	
    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                    	<c:if test="${ not empty userSession.email }"><a href="<c:url value="/logout"/>" >Logout</a></c:if>
                   	</li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

	<!-- Profile Section -->
    <section id="profile" class="container content-section text-center">
        <div class="profile-section">
            <div class="col-lg-10 col-lg-offset-1" id="YourProfile">
            	<h3>Final Selection Before Interviews for Job: <a href="#showJobModal" data-toggle="modal">${job.title}</a></h3>
            	<form:form action="receiveReply" method="POST">
            	<table class="table">
            		<tr><th>User ID</th><th>User</th><th>Address</th><th>Cur. Pos.</th><th>Education</th><th>Experience</th></tr>
            		<c:forEach items="${profiles}" var="a">
            			<tr>
            				<td class="jobValue">${a.userID}</td>
            				<td class="jobValue">${a.firstName} ${a.lastName}<br>${a.email}</td>
            				<td class="jobValue">${a.address}</td>
            				<td class="jobValue">${a.currentPosition}</td>
            				<td class="jobValue">${a.education}</td>
            				<td class="jobValue">${a.experience}</td>
            			</tr>
            			<input type="hidden" name="applicantID" value="${a.userID}">
            		</c:forEach>
            	</table>
            		<c:forEach items="${shortlisted}" var="s">
            			<input type="hidden" name="applicationID" value="${s}">
            		</c:forEach>
            		<input type="hidden" name="jobID" value="${job.jobID}">
            		<button class="btn btn-default btn-lg">Send Notifications</button>
            	</form:form>
            </div>
        </div>
    </section>
    
    <!-- Modal for Job Recruitment Process-->
	<div id="showJobModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-body">
	      	<h3 style="color:blue">JobID = ${job.jobID}: </h3>
	      	<table class="table-modal">
	      		<tr><td>TITLE: </td><td>${job.title}</td></tr>
	      		<tr><td>SALARY: </td><td>${job.salary}</td></tr>
	      		<tr><td>DETAILS: </td><td>${job.details}</td></tr>
	      		<tr><td>POSITION: </td><td>${job.typePos}</td></tr>
	      		<tr><td>LOCATION: </td><td>${job.location}</td></tr>
	      		<tr><td>SKILLS REQ.: </td><td>${job.skills}</td></tr>
	      	</table>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	
	  </div>
	</div>
	
    <!-- Footer -->
    <footer>
        <div class="container text-center">
            <p>Copyright &copy; FoundIT App 2016</p>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="resources/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="resources/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="resources/js/jquery.easing.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="resources/js/grayscale.js"></script>
	
	<!--  DataTables Javascript -->
    <script src="resources/js/jquery.dataTables.min.js"></script>
    
    <!--  Initialise any relevant scripts -->
    <script src="resources/main.js"></script>
</body>


</html>

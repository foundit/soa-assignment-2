<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<link href="<c:url value="/resources/hire.css" />" rel="stylesheet" type="text/css">
</head>
<body>
<div class="hiring-page">
  <div class="form">
    <form:form class="login-form" method="POST" modelAttribute="hT" action="hiring">
    	<h1>SELECT YOUR HIRING TEAM</h1>
    	<c:if test="${ not empty errorNumber }"><h4 style="color: red">${ errorNumber }</h4></c:if>
    	<c:if test="${ not empty errorUsername }"><h4 style="color: red">${ errorUsername }</h4></c:if>
    	<table>
    		<tr><th></th><th>USERNAME</th><th>PASSWORD</th><th>PROFESSIONAL SKILLS</th></tr>
    		<c:forEach items="${ hT.team }" varStatus="i">
    			<tr>
    				<td class="small">${ i.index+1 }<form:hidden path="team[${i.index}].companyID" value="${companyID}"/></td>
    				<td width="20%"><form:input path="team[${i.index}].username"/></td>
    				<td width="20%"><form:input path="team[${i.index}].password"/></td>
    				<td><form:input path="team[${i.index}].skills"/></td>
    			</tr>
    		</c:forEach>
    	</table>
    	<button>create</button>
    </form:form>
  </div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-2.2.3.js"   integrity="sha256-laXWtGydpwqJ8JA+X9x2miwmaiKhn8tVmOVEigRNtP4="   crossorigin="anonymous"></script>
</body>

</html>
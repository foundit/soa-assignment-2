<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<link href="<c:url value="/resources/profile.css" />" rel="stylesheet" type="text/css">
</head>
<body>
<div class="profile-page">
  <div class="form">
    <form:form class="login-form" method="POST" modelAttribute="emp" action="profile?role=${user.role}">
    	<h1>PROFILE CREATION EMPLOYER</h1>
    	<c:if test="${ not empty errorProfile }"><span style="color:red">${ errorProfile }</span></c:if>
    	<table>
    	<tr><td class="t">Email Address: ${ user.email }</td></tr>
    	<tr><td><form:hidden path="managerID" value="${ emp.managerID }"/><form:hidden path="email" value="${ user.email }"/></td></tr>
    	<tr><td>&nbsp;</td></tr>
    	<tr><td class="t">Company Name:</td></tr>
    	<tr><td><form:input path="name" placeholder="Company Name" /></td></tr>
    	<tr><td class="t">Website:</td></tr>
    	<tr><td><form:input path="website" placeholder="WebSite" /></td></tr>
    	<tr><td class="t">Address:</td></tr>
    	<tr><td><form:input path="address" placeholder="Company Address" /></td></tr>
		<tr><td class="t">Industry:</td></tr>
    	<tr><td><form:input path="industry" placeholder="Associated Industry" /></td></tr>
    	<tr><td class="t">Type:</td></tr>
    	<tr><td><form:input path="typecomp" placeholder="Type of Business"/></td></tr>
    	<tr><td class="t">Size:</td></tr>
    	<tr><td><form:input path="size" placeholder="Size" /></td></tr>
    	<tr><td class="t">Description:</td></tr>
    	<tr><td><form:textarea path="description" placeholder="Description" /></td></tr>
    	</table><br>
    	<button>create</button>
    </form:form>
  </div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-2.2.3.js"   integrity="sha256-laXWtGydpwqJ8JA+X9x2miwmaiKhn8tVmOVEigRNtP4="   crossorigin="anonymous"></script>
</body>

</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Grayscale - Start Bootstrap Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="resources/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="resources/css/grayscale.css" rel="stylesheet">
    
    <!-- Datatables CSS -->
	<link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="resources/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
	
    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">
                    <i class="fa fa-play-circle"></i>  <span class="light">Welcome </span> <span class="small">${userSession.email}</span>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#profile">Profile</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#jobs">Job Postings</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#hired">Hiring Team</a>
                    </li>
                    <li>
                    	<c:if test="${ not empty userSession.email }"><a href="<c:url value="/logout"/>">Logout</a></c:if>
                    	<c:if test="${ empty userSession.email }"><a href="<c:url value="/login"/>">Login</a></c:if>
                   	</li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="header-div"><h1 style="color: #000;font-size: 60px">${emp.name}</h1>
                        <div style="color:#0000c0;font-size:24px;font-family:Montserrat">Find qualified workers for your company!!<br>Start by Creating Job Posting:</div><br>
                        <a href="<c:url value="/newjob?id=${emp.companyID}"/>" class="btn btn-default btn-lg"><i class="fa fa-plus-square"></i> <span class="network-name">Create Job Posting</span></a><br>
                        <a href="#startRecruitmentProcess" data-toggle="modal" class="btn btn-default btn-lg"><i class="fa fa-star"></i> <span class="network-name">Start Recruitment Process</span></a><br>
                        <a href="#checkRecruitmentProcess" data-toggle="modal" class="btn btn-default btn-lg"><i class="fa fa-star"></i> <span class="network-name">Check Progress of Reviews</span></a><br>
                        <a href="#postRecruitmentProcess" data-toggle="modal" class="btn btn-default btn-lg"><i class="fa fa-star"></i> <span class="network-name">Select Final Candidate</span></a>
                        </div>
                        <br>
                        <a href="#profile" class="btn btn-circle page-scroll">
                            <i class="fa fa-angle-double-down animated"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    
    <!--  Start Recruitment Process -->
    <div id="startRecruitmentProcess" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	  
	    <!-- Modal content-->
	    <div class="modal-content">
	    <form:form method="POST" class="recruit" action="startRecruitment">
	      <div class="modal-body">
	      	<h3 style="color:blue">Select Which Job to Process:</h3>
	        	<select name="jobSelect">
	        	<c:forEach items="${allJobs.allPostings}" var="job">
	        		<c:if test="${job.status=='open'}">
	        			<option value="${job.jobID}">${job.title}</option>
	        		</c:if>
	        	</c:forEach>
	        	</select>
	        	<input type="hidden" name="check" value="false">
	      </div>
	      <div class="modal-footer">
	      	<button class="btn btn-default">Start</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	      </form:form>
	    </div>
	  </div>
	</div>
	
	<!--  Start Recruitment Process -->
    <div id="checkRecruitmentProcess" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	  
	    <!-- Modal content-->
	    <div class="modal-content">
	    <form:form method="GET" class="recruit" action="progress">
	      <div class="modal-body">
	      	<h3 style="color:blue">Check Which Job to View Process:</h3>
	        	<select name="jobSelect">
	        	<c:forEach items="${allJobs.allPostings}" var="job">
	        		<c:if test="${job.status=='processing'}">
	        			<option value="${job.jobID}">${job.title}</option>
	        		</c:if>
	        	</c:forEach>
	        	</select>
	      </div>
	      <div class="modal-footer">
	      	<button class="btn btn-default">View</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	      </form:form>
	    </div>
	  </div>
	</div>
	
	<!--  Start Recruitment Process -->
    <div id="postRecruitmentProcess" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	  
	    <!-- Modal content-->
	    <div class="modal-content">
	    <form:form method="POST" class="recruit" action="finishedInterview">
	      <div class="modal-body">
	      	<h3 style="color:blue">Check Which Job to View Process:</h3>
	        	<select name="jobSelect">
	        	<c:forEach items="${allJobs.allPostings}" var="job">
	        		<c:if test="${job.status=='interviews'}">
	        			<option value="${job.jobID}">${job.title}</option>
	        		</c:if>
	        	</c:forEach>
	        	</select>
	      </div>
	      <div class="modal-footer">
	      	<button class="btn btn-default">View</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	      </form:form>
	    </div>
	  </div>
	</div>
	
	<!-- Profile Section -->
    <section id="profile" class="container content-section text-center">
        	<div class="row">
            <div class="col-lg-12" id="YourProfile">
                <h2>Your Profile</h2>
                <c:if test="${ not empty editProfileSuccess }"><h4 class="success">${ editProfileSuccess }</h4></c:if>
                <c:if test="${ not empty editProfileError }"><h4 class="error">${ editProfileError }</h4></c:if>
                <div class="row">
				  <div class="col-md-6">
					<table class="table">
						<tbody>
							<tr><td class="profileTitle">USER ID:</td><td class="profileValue">${emp.managerID}</td></tr>
							<tr><td class="profileTitle">COMPANY ID:</td><td class="profileValue">${emp.companyID}</td></tr>
							<tr><td align="left">EMAIL:</td><td class="profileValue">${emp.email}</td></tr>
							<tr><td align="left">COMPANY NAME:</td><td class="profileValue">${emp.name}</td></tr>
							<tr><td align="left">WEBSITE:</td><td class="profileValue">${emp.website}</td></tr>
							<tr><td align="left">ADDRESS:</td><td class="profileValue">${emp.address}</td></tr>
							<tr><td align="left">INDUSTRY:</td><td class="profileValue">${emp.industry}</td></tr>
						</tbody>
					</table>
				  </div>
				  <div class="col-md-6">
					<table class="table">
						<tbody>
							<tr><td align="left" width="100px">DESCRIPTION:</td><td class="profileValue">${emp.description}</td></tr>
							<tr><td align="left">TYPE:</td><td class="profileValue">${emp.typecomp}</td></tr>
							<tr><td align="left">SIZE:</td><td class="profileValue">${emp.size}</td></tr>
						</tbody>
					</table>
				  </div>
				</div>
				<div class="row">
	            	<a href="#profile" id="EditClick" class="btn btn-default btn-lg"><i class="fa fa-pencil-square-o"></i> <span class="network-name">Edit Profile</span></a>
	        	</div>
            </div>
            <div class="col-lg-12" id="SlideEditProfile">
                <h2>Your Profile Edit Mode Activated</h2>
                <form:form method="POST" class="form" modelAttribute="emp" action="save?value=employer">
                <div class="row">
				  <div class="col-md-6">
					<table class="table">
						<tbody>
							<tr><td align="left"  width="150px">PROFILE ID:</td>
								<td align="left"><form:hidden path="companyID"/>${emp.companyID}</td></tr>
							<tr><td align="left">EMAIL:</td>
								<td align="left"><form:hidden path="email"/>${emp.email}</td></tr>
							<tr><td align="left">COMPANY NAME:</td>
								<td align="left"><form:input path="name"/></td></tr>
							<tr><td align="left">WEBSITE:</td>
								<td align="left"><form:input path="website"/></td></tr>
							<tr><td align="left">ADDRESS:</td>
								<td align="left"><form:input path="address"/></td></tr>
							<tr><td align="left"  width="100px">INDUSTRY:</td>
								<td align="left"><form:input path="industry"/></td></tr>
						</tbody>
					</table>
				  </div>
				  <div class="col-md-6">
					<table class="table">
						<tbody>
							<tr><td align="left">DESCRIPTION:</td>
								<td align="left"><form:textarea path="description"/></td></tr>
							<tr><td align="left">TYPE:</td>
								<td align="left"><form:input path="typecomp"/></td></tr>
							<tr><td align="left">SIZE:</td>
								<td align="left"><form:input path="size"/></td></tr>
						</tbody>
					</table>
					<form:hidden path="managerID" value="${emp.managerID}"/>
				  </div>
				</div>
				<div class="row">
	            	<a href="#profile" id="SaveClick" class="btn btn-default btn-lg"><i class="fa fa-ban"></i> <span class="network-name">Cancel</span></a>
	        		&nbsp;
	        		<button class="btn btn-default btn-lg"><i class="fa fa-floppy-o"></i> <span class="network-name">Save Profile</span></button>
	        	</div>
	        	</form:form>
            </div>
            </div>
				<a href="#page-top" class="btn btn-circle page-scroll">
						<i class="fa fa-angle-double-up animated" style="font-size: 125%"></i>
		            </a>
		            <a href="#jobs" class="btn btn-circle page-scroll">
						<i class="fa fa-angle-double-down animated" style="font-size: 125%"></i>
		            </a>
        </div>
    </section>
    
    <!-- Job Postings -->
    <section id="jobs" class="container search-section text-center">
        <div class="row">
            <div class="col-lg-12">
            	<h2>Job Postings</h2>
            	<c:if test="${ not empty newJobSuccess }"><h4 class="success">${ newJobSuccess }</h4></c:if>
                <c:if test="${ not empty newJobError }"><h4 class="error">${ newJobError }</h4></c:if>
            	<c:if test="${ not empty editJobSuccess }"><h4 class="success">${ editJobSuccess }</h4></c:if>
                <c:if test="${ not empty editJobError }"><h4 class="error">${ editJobError }</h4></c:if>
                <c:if test="${ not empty deleteJobSuccess }"><h4 class="success">${ deleteJobSuccess }</h4></c:if>
                <c:if test="${ not empty deleteJobError }"><h4 class="error">${ deleteJobError }</h4></c:if>
                <table id="jobPosting" class="table" cellspacing="0" width="100%">
                	<thead><tr>
                		<th>ID</th><th>Title</th><th>Description</th><th>Salary</th><th>Position</th><th>Location</th><th>Status</th>
                		<th style="font-size: 75%">Edit?</th><th style="font-size: 75%">Delete?</th>
            		</tr></thead>
                	<!--  <tfoot><tr>
                		<th>ID</th><th>Title</th><th>Salary</th><th>Position</th><th>Location</th><th>Status</th>
            		</tr></tfoot>-->
            		<c:forEach items="${allJobs.allPostings}" var="job">
            			<tr>
            				<td class="jobValue">${job.jobID}</td>
            				<td class="jobValue">${job.title}</td>
            				<td class="jobValue">
            					<div style="float:left;text-align:left">${fn:substring(job.details,0,20)}...</div>
            					<div style="float:right;text-align:right;padding-right:8px">
            						<a href="#showDescriptionModal" data-toggle="modal" class="d" data-details="${job.details}">
            						<i class="fa fa-caret-square-o-right fa-1g" style="font-size:175%"></i>
            						</a>
            					</div>
            				</td>
            				<td class="jobValue">${job.salary}</td>
            				<td class="jobValue">${job.typePos}</td>
            				<td class="jobValue">${job.location}</td>
            				<td class="jobValue">${job.status}</td>
            				<td><c:if test="${job.status=='open'}"><a href="#editEmployerJobModal" data-toggle="modal" class="d" data-title="${job.title}" data-id="${job.jobID}">
            					<i class="fa fa-pencil-square-o fa-1g" style="font-size:150%"></i>
            				</a></c:if></td>
            				<td><c:if test="${job.status=='open'}"><a href="#deleteEmployerJobModal" data-toggle="modal" class="d" data-title="${job.title}" data-id="${job.jobID}">
            					<i class="fa fa-trash fa-1g" style="font-size:150%"></i>
            				</a></c:if></td>
            			</tr>
            		</c:forEach>
                </table>
                <div>
                <a href="<c:url value="/newjob?id=${emp.companyID}"/>" class="btn btn-default btn-lg"><i class="fa fa-plus-square"></i> <span class="network-name">Create Job Posting</span></a>
                </div>
            </div>
            <a href="#profile" class="btn btn-circle page-scroll">
				<i class="fa fa-angle-double-up animated" style="font-size: 125%"></i>
            </a>
            <a href="#hired" class="btn btn-circle page-scroll">
				<i class="fa fa-angle-double-down animated" style="font-size: 125%"></i>
            </a>
        </div>
    </div>
    </section>
    
    <!-- Modal for Description-->
	<div id="showDescriptionModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-body">
	      	<h3 style="color:blue">Description: </h3>
	        <div class="e" style="color:#000;font-family:Montserrat;font-size:150%"></div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	
	  </div>
	</div>

    <!-- Hired Team Section -->
    <section id="hired" class="container content-section text-center">
        <div class="profile-section">
            <div class="row">
            <div class="col-lg-12">
            	<h2>Your Hiring Team</h2>
            	<c:if test="${ not empty editHiringSuccess }"><h4 class="success">${ editHiringSuccess }</h4></c:if>
                <c:if test="${ not empty editHiringError }"><h4 class="error">${ editHiringError }</h4></c:if>
                <c:if test="${ not empty addHiringSuccess }"><h4 class="success">${ addHiringSuccess }</h4></c:if>
                <c:if test="${ not empty addHiringError }"><h4 class="error">${ addHiringError }</h4></c:if>
                <c:if test="${ not empty deleteHiringSuccess }"><h4 class="success">${ deleteHiringSuccess }</h4></c:if>
                <c:if test="${ not empty deleteHiringError }"><h4 class="error">${ deleteHiringError }</h4></c:if>
                <c:if test="${ not empty errorSameUsername }"><h4 class="error">${ errorSameUsername }</h4></c:if>
                <table id="hireTeam" class="table" cellspacing="0" width="100%">
                	<thead><tr>
                		<th>ReviewerID</th><th>UserID</th><th>Username</th><th>Password</th><th>Professional Skills</th><th>Delete?</th>
            		</tr></thead>
                	<!--  <tfoot><tr>
                		<th>ID</th><th>Title</th><th>Salary</th><th>Position</th><th>Location</th><th>Status</th>
            		</tr></tfoot>-->
            		<c:set var="size" value="${fn:length(team.team)}"/>
            		<c:forEach items="${team.team}" var="team">
            			<tr>
            				<td class="jobValue">${team.reviewerID}</td>
            				<td class="jobValue">${team.userID}</td>
            				<td class="jobValue">${team.username}</td>
            				<td class="jobValue">${team.password}</td>
            				<td class="jobValue">${team.skills}</td>
            				<td width="25px">
            				<c:if test="${size gt 2 }">
            					<a href="#deleteConfirmationModal" class="d" data-toggle="modal" data-id="${team.reviewerID}" data-username="${team.username}">
            					<i class="fa fa-trash fa-1g" style="font-size:150%"></i></a></c:if>
            				</td>
            			</tr>
            		</c:forEach>
                </table>
            </div>
           </div>
            <div class="row"><br><br>
            <a href="<c:url value="/editTeam?id=${emp.companyID}"/>" class="btn btn-default btn-lg"><i class="fa fa-edit"></i> <span class="network-name">Edit Your Hiring Team</span></a>
            <c:if test="${fn:length(team.team) le 4}">
            	&nbsp;<a href="#addHireMemberModal" data-toggle="modal" class="btn btn-default btn-lg">
            	<i class="fa fa-plus fa-1g"></i><span class="network-name">Add Member</span></a>
            </c:if>
            </div>
            <a href="#jobs" class="btn btn-circle page-scroll">
				<i class="fa fa-angle-double-up animated" style="font-size: 125%"></i>
            </a>
            <a href="#contact" class="btn btn-circle page-scroll">
				<i class="fa fa-angle-double-down animated" style="font-size: 125%"></i>
            </a>
        </div>
    </section>
    
     <!-- Modal for Add new Member -->
	<div id="addHireMemberModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	  
	    <!-- Modal content-->
	    <div class="modal-content">
	    <form:form method="POST" class="form-modal" modelAttribute="hB" action="addMember">
	      <div class="modal-body">
	      	<h3 style="color:blue">Add new Member:</h3>
	        	<table class="table-modal">
	        		<tr><td>USERNAME:</td><td><form:input path="username"/></td></tr>
	        		<tr><td>PASSWORD:</td><td><form:input path="password"/></td></tr>
	        		<tr><td>SKILLS:</td><td><form:textarea path="skills"/></td></tr>
	        		<form:hidden path="companyID" value="${emp.companyID}"/>
	        	</table>
	      </div>
	      <div class="modal-footer">
	      	<button class="btn btn-default">Add</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	      </form:form>
	    </div>
	  </div>
	</div>
	
	<!-- Modal for Delete -->
	<div id="deleteConfirmationModal" class="modal fade" role="dialog">
	  <div class="modal-sm">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-body">
	      	<h3 style="color:blue">Confirmation of Request</h3>
	      	<span style="color:#000;font-family:Montserrat;font-size:150%">Are you sure you want to delete user<span class="e"></span>?</span>
	      </div>
	      <div class="modal-footer">
	      	<form:form action="deleteMember" method="POST">
	      		<input type="hidden" name="reviewerID">
	      		<button class="btn btn-default">Proceeed</button>
	      		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	      	</form:form>
	      </div>
	    </div>
	  </div>
	</div>
	
	<!-- Modal for Edit Job for Employer -->
	<div id="editEmployerJobModal" class="modal fade" role="dialog">
	  <div class="modal-sm">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-body">
	      	<h3 style="color:blue">Confirmation of Edit?</h3>
	      	<span style="color:#000;font-family:Montserrat;font-size:150%">Are you sure you want to edit job "<span class="e"></span>"?</span>
	      </div>
	      <div class="modal-footer">
	      	<form:form action="editJob" method="GET">
	      		<input type="hidden" name="jobID">
	      		<button class="btn btn-default">Proceeed</button>
	      		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	      	</form:form>
	      </div>
	    </div>
	  </div>
	</div>
	
	<!-- Modal for Edit Job for Employer -->
	<div id="deleteEmployerJobModal" class="modal fade" role="dialog">
	  <div class="modal-sm">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-body">
	      	<h3 style="color:blue">Confirmation of Delete?</h3>
	      	<span style="color:#000;font-family:Montserrat;font-size:150%">Are you sure you want to delete job "<span class="e"></span>"?</span>
	      </div>
	      <div class="modal-footer">
	      	<form:form action="deleteJob" method="POST">
	      		<input type="hidden" name="jobID">
	      		<button class="btn btn-default">Proceeed</button>
	      		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	      	</form:form>
	      </div>
	    </div>
	  </div>
	</div>
	
    <!-- Footer -->
    <footer>
        <div class="container text-center">
            <p>Copyright &copy; FoundIT Co App 2016</p>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="resources/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="resources/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="resources/js/jquery.easing.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="resources/js/grayscale.js"></script>
    
    <!--  DataTables Javascript -->
    <script src="resources/js/jquery.dataTables.min.js"></script>
    
    <!--  Initialise any relevant scripts -->
    <script src="resources/main.js"></script>
</body>


</html>

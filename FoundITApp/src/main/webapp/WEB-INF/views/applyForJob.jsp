<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<link href="<c:url value="/resources/profile.css" />" rel="stylesheet" type="text/css">
</head>
<body>
<div class="profile-page">
  <div class="form-apply">
    <form:form class="login-form" method="POST" action="apply">
    	<h1>APPLY FOR JOB:</h1>
    	<h3>Your Profile Information</h3>
    	<table>
    	<tr><td class="t">Email Address:</td><td>${ p.email }</td></tr>
    	<tr><td class="t">Name:</td><td>${ p.firstName } ${ p.lastName }</td></tr>
    	<tr><td class="t">Address:</td><td>${ p.address }</td></tr>
    	<tr><td class="t">Current Position:</td><td>${ p.currentPosition }</td></tr>
    	<tr><td class="t">Prev. Education:</td><td>${ p.education }</td></tr>
    	<tr><td class="t">Prev. Experience:</td><td>${ p.experience }</td></tr>
    	<tr><td colspan="2" align="center"><h3>Job Information</h3></td></tr>
    	<tr><td class="t">Job Title:</td><td>${ j.title }</td></tr>
    	<tr><td class="t">Salary:</td><td>${ j.salary }</td></tr>
    	<tr><td class="t">Description:</td><td>${ j.details }</td></tr>
    	<tr><td class="t">Position:</td><td>${ j.typePos }</td></tr>
    	<tr><td class="t">Location:</td><td>${ j.location }</td></tr>
    	<tr><td class="t">Skills Requ.:</td><td>${ j.skills }</td></tr>
    	<tr><td class="t">Cover Letter:</td></tr>
    	<tr><td colspan="2"><textarea name="coverLetter"></textarea>
    	</table><br>
    	<input type="hidden" name="appID" value="${ p.userID }">
    	<input type="hidden" name="jobID" value="${ jobID }">
    	<button>Apply</button>
    </form:form>
  </div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-2.2.3.js"   integrity="sha256-laXWtGydpwqJ8JA+X9x2miwmaiKhn8tVmOVEigRNtP4="   crossorigin="anonymous"></script>
</body>
</html>
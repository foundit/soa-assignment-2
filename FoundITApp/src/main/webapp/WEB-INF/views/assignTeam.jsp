<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Grayscale - Start Bootstrap Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="resources/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="resources/css/grayscale.css" rel="stylesheet">
    
    <!-- Datatables CSS -->
	<link href="resources/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="resources/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    
    <c:url var="controller" value="/company"/>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
	
    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <c:if test="${ check==false }">
                <a class="navbar-brand page-scroll" href="<c:url value="/employer"/>" >
                    <i class="fa fa-play-circle"></i>  <span class="light">Go To Home Page: </span> <span class="small">${userSession.email}</span>
                </a></c:if>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                    	<c:if test="${ not empty userSession.email }"><a href="<c:url value="/logout"/>" >Logout</a></c:if>
                   	</li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

	<!-- Profile Section -->
    <section id="profile" class="container content-section text-center">
        <div class="profile-section">
            <div class="col-lg-10 col-lg-offset-1" id="YourProfile">
            	<c:if test="${ check==false }">
            	<h3>1) Start background Checks: &nbsp;&nbsp;<a href="<c:url value="/background?jobID=${job.jobID}"/>" class="btn btn-default btn-lg"><i class="fa fa-play" aria-hidden="true"></i>
	                        <span class="network-name">Start</span></a></h3>
	            </c:if>
            	<c:if test="${ check==true }">
            	
            	<h3>2) Assign Team for Job: &nbsp;&nbsp;<button class="btn btn-default btn-lg" onClick="checkCheckBox();"><i class="fa fa-play"></i>
	                        <span class="network-name">Assign</span></button></h3>
            	<table id="" class="table" cellspacing="0" width="100%">
                	<thead><tr>
                		<th>ReviewerID</th><th>UserID</th><th>Username</th><th>Password</th><th>Professional Skills</th><th>Select</th>
            		</tr></thead>
            		<c:set var="size" value="${fn:length(team.team)}"/>
            		<form:form action="assign" method="POST" class="checkbox" id="assign-form">
            		<c:forEach items="${team.team}" var="team">
            			<tr>
            				<td class="jobValue">${team.reviewerID}</td>
            				<td class="jobValue">${team.userID}</td>
            				<td class="jobValue">${team.username}</td>
            				<td class="jobValue">${team.password}</td>
            				<td class="jobValue">${team.skills}</td>
            				<td><input type="checkbox" value="${team.reviewerID}" name="check" style="width:20px;height:20px"></td>
            			</tr>
            		</c:forEach>
            		<c:forEach items="${applications.applications}" var="a">
            			<input type="hidden" name="applicationID" value="${a.applicationID}">
            		</c:forEach>
            			<input type="hidden" name="jobID" value="${job.jobID}">
            		</form:form>
                </table>
            	</c:if>
            	<br><br>
            	<h3>Recruitment Process for: <a href="#showJobModal" data-toggle="modal">${job.title}</a></h3>
            	<table class="table">
            		<tr><th>Application ID</th><th>Applicant ID</th><th>Cover Letter</th><th>Status</th></tr>
            		<c:forEach items="${applications.applications}" var="a">
            			<tr>
            				<td class="jobValue">${a.applicationID}</td>
            				<td class="jobValue"><a data-toggle="modal" class="d" data-id="${a.applicantID}" onClick="goUserAJAX(this);">
            						<i class="fa fa-user fa-1g" style="font-size:175%"></i>
            						</a> &nbsp;&nbsp;${a.applicantID}</td>
            				<td class="jobValue"><a href="#showCoverLetterModal" data-toggle="modal" data-letter="${a.coverLetter}" onClick="goLetterAJAX(this);">
            					<i class="fa fa-envelope fa-1g" style="font-size:150%"></i></a> &nbsp;&nbsp;${fn:substring(a.coverLetter,0,50)} ...</td>
            				<td class="jobValue">${a.status}</td>
            			</tr>
            		</c:forEach>
            	</table>
            </div>
        </div>
    </section>
    
    <!-- Modal for Save Confirmation -->
	<div id="assignFailModal" class="modal fade" role="dialog">
	  <div class="modal-sm">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-body">
	      	<h3 style="color:blue">ERROR</h3>
	      	<h4 style="color:black">Only 2 reviewers can be selected!!</h4>
	      </div>
	      <div class="modal-footer">
	      	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	      </div>
	    </div>
	  </div>
	</div>
    
    <!-- Modal for Job Recruitment Process-->
	<div id="showJobModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-body">
	      	<h3 style="color:blue">JobID = ${job.jobID}: </h3>
	      	<table class="table-modal">
	      		<tr><td>TITLE: </td><td>${job.title}</td></tr>
	      		<tr><td>SALARY: </td><td>${job.salary}</td></tr>
	      		<tr><td>DETAILS: </td><td>${job.details}</td></tr>
	      		<tr><td>POSITION: </td><td>${job.typePos}</td></tr>
	      		<tr><td>LOCATION: </td><td>${job.location}</td></tr>
	      		<tr><td>SKILLS REQ.: </td><td>${job.skills}</td></tr>
	      	</table>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	
	  </div>
	</div>
	
	<!-- Modal for Showing User Details -->
	<div id="showUserModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-body">
	      	<h3 style="color:blue">User: <span class="firstname"></span> <span class="lastname"></span></h3>
	      	<table class="table-modal">
	      		<tr><td>EMAIL: </td><td class="email"></td></tr>
	      		<tr><td>ADDRESS: </td><td class="address"></td></tr>
	      		<tr><td>CURRENT POSITION: </td><td class="position"></td></tr>
	      		<tr><td>EDUCATION: </td><td class="education"></td></tr>
	      		<tr><td>EXPERIENCE: </td><td class="experience"></td></tr>
	      	</table>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	
	  </div>
	</div>
    
    <!-- Modal for Showing Cover Letter-->
	<div id="showCoverLetterModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-body">
	      	<h3 style="color:blue">Applicants Cover Letter: </h3>
	        <div class="e" style="color:#000;font-family:Montserrat;font-size:150%"></div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	
	  </div>
	</div>
	
    <!-- Footer -->
    <footer>
        <div class="container text-center">
            <p>Copyright &copy; FoundIT App 2016</p>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="resources/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="resources/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="resources/js/jquery.easing.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="resources/js/grayscale.js"></script>
	
	<!--  DataTables Javascript -->
    <script src="resources/js/jquery.dataTables.min.js"></script>
    
    <!--  Initialise any relevant scripts -->
    <script src="resources/main.js"></script>
</body>


</html>

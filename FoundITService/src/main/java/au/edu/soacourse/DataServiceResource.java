package au.edu.soacourse;

import java.net.URI;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;

import au.edu.soacourse.dao.ApplicationDAO;
import au.edu.soacourse.dao.CompanyDAO;
import au.edu.soacourse.dao.JobDAO;
import au.edu.soacourse.dao.ProfileDAO;
import au.edu.soacourse.dao.ReviewDAO;
import au.edu.soacourse.dao.ReviewerDAO;
import au.edu.soacourse.dataservice.JobAlerts;
import au.edu.soacourse.model.Application;
import au.edu.soacourse.model.ApplicationList;
import au.edu.soacourse.model.Company;
import au.edu.soacourse.model.HiringTeam;
import au.edu.soacourse.model.Job;
import au.edu.soacourse.model.JobList;
import au.edu.soacourse.model.Profile;
import au.edu.soacourse.model.Review;
import au.edu.soacourse.model.ReviewList;
import au.edu.soacourse.model.Reviewer;

@Path("/")
public class DataServiceResource {
	// Allows to insert contextual objects into the class, 
	// e.g. ServletContext, Request, Response, UriInfo
	@Context
	UriInfo uriInfo;
	@Context
	Request request;

	private JobAlerts jobAlerts;
	
	@Autowired
	public DataServiceResource(JobAlerts jobAlerts){
		this.jobAlerts = jobAlerts;
	}
	
	@GET
	@Path("/jobalerts")
	public Response getAlert(@QueryParam("keyword") String searchKeyword, @QueryParam("sorted") boolean sorted, @QueryParam("email") String email){
		jobAlerts.searchAndEmailResults(searchKeyword, sorted, email);
		return Response.ok().build();
	}
	
}
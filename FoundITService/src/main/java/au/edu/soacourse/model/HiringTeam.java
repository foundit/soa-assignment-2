package au.edu.soacourse.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class HiringTeam {
	/**
	 * 
	 */
	private List<Reviewer> team;
	
	public List<Reviewer> getTeam() {
		return team;
	}
	public void setTeam(List<Reviewer> team) {
		this.team = team;
	}
	

}

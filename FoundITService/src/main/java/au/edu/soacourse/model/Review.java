package au.edu.soacourse.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Review {
	private int reviewID;
	private int applicationID;
	private int reviewerID;
	private String comments;
	private String decision;
	
	public Review(int reviewID, int applicationID, int reviewerID, String comments, String decision){
		this.reviewID = reviewID;
		this.applicationID = applicationID ;
		this.reviewerID = reviewerID;
		this.comments = comments;
		this.decision = decision;
	}

	public Review() {
	}

	public int getReviewID() {
		return reviewID;
	}

	public void setReviewID(int reviewID) {
		this.reviewID = reviewID;
	}

	public int getApplicationID() {
		return applicationID;
	}

	public void setApplicationID(int applicationID) {
		this.applicationID = applicationID;
	}

	public int getReviewerID() {
		return reviewerID;
	}

	public void setReviewerID(int reviewerID) {
		this.reviewerID = reviewerID;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getDecision() {
		return decision;
	}

	public void setDecision(String decision) {
		this.decision = decision;
	}

}

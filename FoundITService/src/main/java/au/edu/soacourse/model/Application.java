package au.edu.soacourse.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Application {
	private int applicationID;
	private int jobID;
	private int applicantID;
	private String coverLetter;
	private String status;
	
	public Application() {
	}
	public Application(int applicationID, int jobID, int appicantID, String coverLetter, String status) {
		this.applicationID = applicationID;
		this.jobID = jobID;
		this.applicantID = appicantID;
		this.coverLetter = coverLetter;
		this.status = status;
	}

	public int getApplicationID() {
		return applicationID;
	}

	public void setApplicationID(int applicationID) {
		this.applicationID = applicationID;
	}

	public int getJobID() {
		return jobID;
	}

	public void setJobID(int jobID) {
		this.jobID = jobID;
	}

	public int getApplicantID() {
		return applicantID;
	}

	public void setApplicantID(int applicantID) {
		this.applicantID = applicantID;
	}

	public String getCoverLetter() {
		return coverLetter;
	}

	public void setCoverLetter(String coverLetter) {
		this.coverLetter = coverLetter;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}

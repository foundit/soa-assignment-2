package au.edu.soacourse.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class JobList{
	/**
	 * 
	 */
	private List<Job> allPostings;
	
	public List<Job> getAllPostings() {
		return allPostings;
	}
	public void setAllPostings(List<Job> allPostings) {
		this.allPostings = allPostings;
	}
}

package au.edu.soacourse.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Job{
	
	private int jobID;
	private int companyID;
	private String title;
	private String salary;
	private String details;
	private String typePos;
	private String location;
	private String status;
	private String skills;
	
	public Job(){	
	}
	
	public Job(int jobID, int companyID, String title, String salary, String details, 
			String type, String location, String status, String skills){
		this.jobID = jobID;
		this.companyID = companyID;
		this.title = title;
		this.salary = salary;
		this.details = details;
		this.typePos = type;
		this.location = location;
		this.status = status;
		this.skills = skills;
	}
	
	public int getJobID() {
		return jobID;
	}
	public void setJobID(int jobID) {
		this.jobID = jobID;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSalary() {
		return salary;
	}
	public void setSalary(String salary) {
		this.salary = salary;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getCompanyID() {
		return companyID;
	}
	public void setCompanyID(int companyID) {
		this.companyID = companyID;
	}

	public String getTypePos() {
		return typePos;
	}

	public void setTypePos(String typePos) {
		this.typePos = typePos;
	}

	public String getSkills() {
		return skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}
}

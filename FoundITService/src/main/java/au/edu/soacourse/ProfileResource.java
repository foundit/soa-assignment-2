package au.edu.soacourse;

import java.net.URI;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;

import au.edu.soacourse.dao.ApplicationDAO;
import au.edu.soacourse.dao.CompanyDAO;
import au.edu.soacourse.dao.JobDAO;
import au.edu.soacourse.dao.ProfileDAO;
import au.edu.soacourse.dao.ReviewDAO;
import au.edu.soacourse.dao.ReviewerDAO;
import au.edu.soacourse.model.Application;
import au.edu.soacourse.model.ApplicationList;
import au.edu.soacourse.model.Company;
import au.edu.soacourse.model.HiringTeam;
import au.edu.soacourse.model.Job;
import au.edu.soacourse.model.JobList;
import au.edu.soacourse.model.Profile;
import au.edu.soacourse.model.Review;
import au.edu.soacourse.model.ReviewList;
import au.edu.soacourse.model.Reviewer;

@Path("/users")
public class ProfileResource {
	// Allows to insert contextual objects into the class, 
	// e.g. ServletContext, Request, Response, UriInfo
	@Context
	UriInfo uriInfo;
	@Context
	Request request;

	private ProfileDAO profileDAO;
	private CompanyDAO companyDAO;
	private JobDAO jobDAO;
	private ApplicationDAO applicationDAO;
	private ReviewerDAO reviewerDAO;
	private ReviewDAO reviewDAO;
	
	@Autowired
	public ProfileResource(ProfileDAO profileDAO, CompanyDAO companyDAO, JobDAO jobDAO, ApplicationDAO applicationDAO, ReviewerDAO reviewerDAO, ReviewDAO reviewDAO){
		this.profileDAO = profileDAO;
		this.companyDAO = companyDAO;
		this.jobDAO = jobDAO;
		this.applicationDAO = applicationDAO;
		this.reviewerDAO = reviewerDAO;
		this.reviewDAO = reviewDAO;
	}
	
	@GET
	@Path("applicant/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProfileA(@PathParam("id") int id){
		Profile profile = new Profile();
		try {
			profile = profileDAO.getProfile(id);
		} catch (SQLException e) {
			return Response.noContent().build();
		}
		return Response.ok(profile).build();
	}
	
	@GET
	@Path("employer/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProfileE(@PathParam("id") int id){
		Company company = new Company();
		try {
			company = companyDAO.getCompany(id);
		} catch (SQLException e) {
			return Response.noContent().build();
		}
		return Response.ok(company).build();
	}
	
	@GET
	@Path("reviewer/{userID}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReviewerDetails(@PathParam("userID") int userID){
		Reviewer reviewer = new Reviewer();
		try {
			reviewer = reviewerDAO.getReviewer(userID);
		} catch (SQLException e) {
			return Response.noContent().build();
		}
		return Response.ok(reviewer).build();
	}
	
	
	@GET
	@Path("employer/jobs/{companyID}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllJobPostings(@PathParam("companyID") int companyID){
		JobList all = new JobList();
		List<Job> l = new ArrayList<Job>();
		l = jobDAO.getJobs(companyID);
		if(l==null){
			return Response.noContent().build();
		}
		all.setAllPostings(l);
		return Response.ok(all).build();
	}
	
	@GET
	@Path("employer/job/{jobID}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSingleJob(@PathParam("jobID") int jobID){
		Job job = new Job();
		try {
			job = jobDAO.getJob(jobID);
		} catch (SQLException e) {
			return Response.noContent().build();
		}
		return Response.ok(job).build();
	}
	
	@GET
	@Path("employer/team/{companyID}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getHiringTeam(@PathParam("companyID") int companyID){
		HiringTeam all = new HiringTeam();
		all = reviewerDAO.getReviewers(companyID);
		if(all==null){
			return Response.noContent().build();
		}
		return Response.ok(all).build();
	}
	
	@GET
	@Path("job")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchJobPostings(@QueryParam("type") String type, @QueryParam("status") String status, @QueryParam("search") String search){
		JobList result = new JobList();
		List<Job> j = jobDAO.searchJobs(type, search, status);
		if(j==null){
			return Response.noContent().build();
		}
		result.setAllPostings(j);
		return Response.ok(result).build();
	}
	
	@GET
	@Path("applicant/jobs")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getJobPostingsForUser(@QueryParam("list") List<String> list){
		JobList result = new JobList();
		List<Job> j = jobDAO.getSavedJobs(list);
		if(j==null){
			return Response.noContent().build();
		}
		result.setAllPostings(j);
		return Response.ok(result).build();
	}
	
	@GET
	@Path("{id}/applied")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getAllAppliedJobs(@PathParam("id") int applicantID){
		List<String> result = new ArrayList<String>();
		result = applicationDAO.getAppliedJobID(applicantID);
		if(result==null){
			return Response.noContent().build();
		}
		return Response.ok(result).build();
	}
	
	@GET
	@Path("employer/applications/{jobID}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getApplications(@PathParam("jobID") int jobID){
		ApplicationList applications = new ApplicationList();
		List<Application> result = new ArrayList<Application>();
		result = applicationDAO.getApplications(jobID);
		if(result==null){
			return Response.noContent().build();
		}
		applications.setApplications(result);
		return Response.ok(applications).build();
	}
	
	@GET
	@Path("letter/{appID}/{jobID}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getCoverLetter(@PathParam("appID") String appID, @PathParam("jobID") String jobID){
		String coverLetter = applicationDAO.getCoverLetter(jobID, appID);
		if(coverLetter==null){
			return Response.noContent().build();
		}
		return Response.ok(coverLetter).build();
	}
	
	@GET
	@Path("applicant/{appID}/job/{jobID}/status")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getApplicationStatus(@PathParam("appID") String appID, @PathParam("jobID") String jobID){
		String status = applicationDAO.getApplicationStatus(jobID, appID);
		if(status==null){
			return Response.noContent().build();
		}
		return Response.ok(status).build();
	}
	
	@GET
	@Path("reviews/{reviewerID}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReviews(@PathParam("reviewerID") int reviewerID){
		ReviewList reviews = new ReviewList();
		List<Review> l = reviewDAO.getReviews(reviewerID);
		if(l==null){
			return Response.noContent().build();
		}
		reviews.setReviews(l);
		return Response.ok(reviews).build();
	}
	
	@GET
	@Path("reviewer/application/{applicationID}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getApplication(@PathParam("applicationID") int applicationID){
		Application application;
		try {
			application = applicationDAO.getApplication(applicationID);
		} catch (SQLException e) {
			return Response.noContent().build();
		}
		return Response.ok(application).build();
	}
	
	@GET
	@Path("review/applicationID/{applicationID}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReviewsByApplicationID(@PathParam("applicationID") int applicationID){
		ReviewList r = new ReviewList();
		List<Review> reviews = new ArrayList<Review>();
		reviews = reviewDAO.getReviewByApplicationID(applicationID);
		if(reviews == null){
			return Response.noContent().build();
		}
		r.setReviews(reviews);
		return Response.ok(r).build();
	}
	
	@GET
	@Path("shortlisted/{applicationID}")
	@Produces(MediaType.TEXT_HTML)
	public Response confirmIfShortlisted(@PathParam("applicationID") int applicationID){
		if(reviewDAO.confirmIfShortlisted(applicationID)==false){
			return Response.noContent().build();
		}
		return Response.ok().build();
	}
	
	@GET
	@Path("/interview/job/{jobID}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInterview(@PathParam("jobID") int jobID){
		List<Integer> interviews = reviewDAO.getInterviewedList(jobID);
		if(interviews == null){
			return Response.noContent().build();
		}
		return Response.ok(interviews).build();
	}

	@POST
	@Path("/applicant")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createProfileA(Profile profile){
		String id = "";
		try {
			id = profileDAO.createProfile(profile) + "";
		} catch (SQLException e) {
			e.printStackTrace();
			Response.notModified().build();
		}
		URI uri = uriInfo.getAbsolutePathBuilder().path(id).build();
		System.out.println(uri.getPath());
		return Response.created(uri).build();
	}
	
	@POST
	@Path("/employer")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createProfileE(Company company){
		String id;
		try {
			id = companyDAO.createCompany(company) + "";
		} catch (SQLException e) {
			return Response.notModified().build();
		}
		URI uri = uriInfo.getAbsolutePathBuilder().path(id).build();
		return Response.created(uri).build();
	}
	
	@POST
	@Path("/employer/team")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveTeam(HiringTeam team){
		String id = reviewerDAO.createHiringTeam(team) + "";
		if(id.equals("0")){
			return Response.notModified().build();
		}
		URI uri = uriInfo.getAbsolutePathBuilder().path(id).build();
		return Response.created(uri).build();
	}
	
	@POST
	@Path("/employer/job")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveJob(Job job){
		System.out.println("Save new job posting");
		String id;
		try {
			id = jobDAO.createJob(job) + "";
		} catch (SQLException e) {
			return Response.notModified().build();
		}
		URI uri = uriInfo.getAbsolutePathBuilder().path(id).build();
		return Response.created(uri).build();
	}
	
	@POST
	@Path("/applicant/apply")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveApplication(Application application){
		String id;
		try {
			id = applicationDAO.createApplication(application) + "";
		} catch (SQLException e) {
			return Response.notModified().build();
		}
		URI uri = uriInfo.getAbsolutePathBuilder().path(id).build();
		return Response.created(uri).build();
	}
	
	@POST
	@Path("/reviews")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveInitialReviews(ReviewList reviews){
		int i[];
		try {
			i = reviewDAO.createReviews(reviews);
		} catch (SQLException e) {
			return Response.notModified().build();
		}
		URI uri = uriInfo.getAbsolutePath();
		return Response.created(uri).build();
	}
	
	@POST
	@Path("/interview/job/{jobID}/applicant/{applicantID}")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response insertInterview(@PathParam("jobID") int jobID, @PathParam("applicantID") int applicantID, String empty){
		if(reviewDAO.createInterviewList(jobID, applicantID)==false){
			return Response.notModified().build();
		}
		return Response.ok().build();
	}
	
	@PUT
	@Path("/applicant/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateUser(@PathParam("id") String id, Profile profile) {
		if(profileDAO.updateProfile(profile)==false){
			return Response.notModified().build();
		}
		return Response.ok().build();
	}
	
	@PUT
	@Path("/employer/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateCompany(@PathParam("id") String id, Company company) {
		if(companyDAO.updateCompany(company)==false){
			return Response.notModified().build();
		}
		return Response.ok().build();
	}
	
	@PUT
	@Path("/employer/job")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateJob(Job job){
		if(jobDAO.updateJob(job)==false){
			return Response.notModified().build();
		}
		return Response.ok().build();
	}
	
	@PUT
	@Path("/employer/job/{jobID}/status")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateJobStatus(@PathParam("jobID") int jobID, String status){
		if(jobDAO.updateJobStatus(status, jobID)==false){
			return Response.notModified().build();
		}
		return Response.ok().build();
	}
	
	@PUT
	@Path("/employer/team/{employerID}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateTeam(@PathParam("employerID") String employerID, HiringTeam team){
		if(reviewerDAO.updateReviewers(team)==false){
			return Response.notModified().build();
		}
		return Response.ok().build();
	}
	
	@PUT
	@Path("applicant/apply")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateCoverLetter(Application application){
		if(applicationDAO.updateApplication(application)==false){
			return Response.notModified().build();
		}
		return Response.ok().build();
	}
	
	@PUT
	@Path("application/{applicationID}/status")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response updateApplicationStatus(@PathParam("applicationID") int applicationID, String status){
		if(applicationDAO.updateApplicationStatus(applicationID, status)==false){
			return Response.notModified().build();
		}
		return Response.ok().build();
	}
	
	@PUT
	@Path("applicant/{applicationID}/job/{jobID}/status")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response updateApplicationStatusByJob(@PathParam("applicationID") int applicationID, @PathParam("jobID") int jobID, String status){
		if(applicationDAO.updateApplicationStatusByJob(applicationID, jobID, status)==false){
			return Response.notModified().build();
		}
		return Response.ok().build();
	}
	
	@PUT
	@Path("review/{reviewID}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateReview(Review review){
		if(reviewDAO.updateReview(review)==false){
			return Response.notModified().build();
		}
		return Response.ok().build();
	}
	
	@DELETE
	@Path("/employer/team/{reviewerID}")
	public Response deleteHiredMemeber(@PathParam("reviewerID") int reviewerID){
		if(reviewerDAO.deleteReviewer(reviewerID)==false){
			return Response.notModified().build();
		}
		return Response.ok().build();
	}
	
	@DELETE
	@Path("/employer/job/{jobID}")
	public Response deleteJob(@PathParam("jobID") int jobID){
		if(jobDAO.deleteJob(jobID)==false){
			return Response.notModified().build();
		}
		return Response.ok().build();
	}
	
	@DELETE
	@Path("/applicant/{applicantID}/apply/{jobID}")
	public Response deleteJob(@PathParam("applicantID") int applicantID, @PathParam("jobID") int jobID){
		if(applicationDAO.deleteApplication(jobID, applicantID)==false){
			return Response.notModified().build();
		}
		return Response.ok().build();
	}
	
	@DELETE
	@Path("/interview/job/{jobID}")
	public Response deleteInterviewList(@PathParam("jobID") int jobID){
		if(reviewDAO.deleteInterviewList(jobID)==false){
			return Response.notModified().build();
		}
		return Response.ok().build();
	}
	
	@DELETE
	@Path("/interview/applicant/{applicantID}/job/{jobID}")
	public Response deleteInterviewList(@PathParam("applicantID") int applicantID, @PathParam("jobID") int jobID){
		if(reviewDAO.rejectInterview(applicantID, jobID)==false){
			return Response.notModified().build();
		}
		return Response.ok().build();
	}
}
package au.edu.soacourse.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;

import au.edu.soacourse.Constants;
import au.edu.soacourse.model.Review;
import au.edu.soacourse.model.ReviewList;

public class ReviewDAOImpl implements ReviewDAO {

	@Override
	public int createReview(Review review) throws SQLException {
		Connection c = null;
		PreparedStatement s = null;
		String SQL_INSERT = "INSERT INTO REVIEWS "
				+ "(APPLICATIONID,"
				+ " REVIEWERID,"
				+ " COMMENTS,"
				+ " DECISION)"
				+ "VALUES (?, ?, ?, ?)";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_INSERT,
					Statement.RETURN_GENERATED_KEYS);
			s.setInt(1, review.getApplicationID());
			s.setInt(2, review.getReviewerID());
			s.setString(3, review.getComments());
			s.setString(4, review.getDecision());

			int affectedRows = s.executeUpdate();

			if (affectedRows == 0) {
				throw new SQLException("Creating review failed, no rows affected.");
			}

			try (ResultSet generatedKeys = s.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					review.setReviewID(generatedKeys.getInt(1));
				} else {
					throw new SQLException("Creating review failed, no ID obtained.");
				}
			} finally {
				DbUtils.closeQuietly(s);
				DbUtils.closeQuietly(c);
			}

			c.commit();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		} finally {
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		System.out.println("review created successfully");
		return review.getReviewID();
	}
	
	public int[] createReviews(ReviewList reviews) throws SQLException {
		Connection c = null;
		PreparedStatement s = null;
		List<Review> review = reviews.getReviews();
		int m[];
		String SQL_INSERT = "INSERT INTO REVIEWS "
				+ "(APPLICATIONID,"
				+ " REVIEWERID,"
				+ " COMMENTS,"
				+ " DECISION)"
				+ "VALUES (?, ?, ?, ?)";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_INSERT,
					Statement.RETURN_GENERATED_KEYS);
			for(int i=0; i<review.size(); i++){
				s.setInt(1, review.get(i).getApplicationID());
				s.setInt(2, review.get(i).getReviewerID());
				s.setString(3, "");
				s.setString(4, "Unreviewed");
				s.addBatch();
			}
			m = s.executeBatch();
			c.commit();
		} catch ( Exception e ) {
			e.printStackTrace();
			throw new SQLException();
		} finally {
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return m;
	}

	@Override
	public Review getReview(int reviewID) throws SQLException {
		Review review = new Review();
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		String SQL_SELECT = "SELECT * FROM REVIEWS WHERE REVIEWID = ? ";
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_SELECT);
			s.setInt(1, reviewID);
			
			r = s.executeQuery();
			if(r.next()) {
				review.setReviewID(reviewID);
				review.setApplicationID(r.getInt("APPLICATIONID"));
				review.setReviewerID(r.getInt("REVIEWERID"));
				review.setComments(r.getString("COMMENTS"));
				review.setDecision(r.getString("DECISION"));
			}
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			throw new SQLException("no job found with specified companyID"); 
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return review;
	}

	@Override
	public boolean updateReview(Review review) {
		Connection c = null;
		PreparedStatement s = null;
		Boolean result = true;

		String SQL_UPDATE = "UPDATE REVIEWS SET"
				+ " COMMENTS = ?,"
				+ " DECISION = ? "
				+ " WHERE REVIEWID = ?";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_UPDATE);
			s.setString(1, review.getComments());
			s.setString(2, review.getDecision());
			s.setInt(3, review.getReviewID());
			
			int affectedRows = s.executeUpdate();
			if (affectedRows == 0) {
				result = false;
			}
			c.commit();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return result;	
	}

	@Override
	public List<Review> getReviews(int reviewerID) {
		List<Review> reviews = new ArrayList<Review>();
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		String SQL_SELECT = "SELECT * FROM REVIEWS WHERE REVIEWERID = ? ";
		System.out.println(reviewerID);
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_SELECT);
			
			s.setInt(1, reviewerID);
			
			r = s.executeQuery();
			while(r.next()) {
				reviews.add(getReview(r.getInt("REVIEWID")));
			}
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			reviews = null;
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return reviews;
	}
	
	@Override
	public List<Review> getReviewByApplicationID(int applicationID) {
		List<Review> reviews = new ArrayList<Review>();
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		String SQL_SELECT = "SELECT * FROM REVIEWS WHERE APPLICATIONID = ? ";
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_SELECT);
			s.setInt(1, applicationID);
			r = s.executeQuery();
			while(r.next()) {
				reviews.add(getReview(r.getInt("REVIEWID")));
			}
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			reviews = null;
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return reviews;
	}
	
	public boolean confirmIfShortlisted(int applicationID){
		boolean result = false;
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		String SQL_SELECT = "SELECT * FROM REVIEWS WHERE APPLICATIONID = ? AND DECISION = ?";
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_SELECT);
			s.setInt(1, applicationID);
			s.setString(2, "shortlisted");
			
			r = s.executeQuery();
			int size=0;
			while(r.next()) {
				size++;
			}
			if(size==2){
				result = true;
			}
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
	
	public List<Integer> getInterviewedList(int jobID){
		List<Integer> interviews = new ArrayList<Integer>();
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		String SQL_SELECT = "SELECT * FROM INTERVIEWS WHERE JOBID = ?";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_SELECT);
			s.setInt(1, jobID);
			r = s.executeQuery();
			while(r.next()){
				interviews.add(r.getInt("APPLICANTID"));
			}
		} catch ( Exception e ) {
			e.printStackTrace();
			interviews = null;
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return interviews;
	}
	
	public boolean createInterviewList(int jobID, int applicantID){
		Connection c = null;
		PreparedStatement s = null;
		boolean result = true;
		String SQL_INSERT = "INSERT INTO INTERVIEWS (APPLICANTID, JOBID) VALUES (?,?)";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_INSERT,
					Statement.RETURN_GENERATED_KEYS);
			s.setInt(1, applicantID);
			s.setInt(2, jobID);
			s.executeUpdate();
			c.commit();
		} catch ( Exception e ) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
	
	public boolean deleteInterviewList(int jobID){
		Connection c = null;
		PreparedStatement s = null;
		boolean result = true;
		String SQL_DELETE = "DELETE FROM INTERVIEWS WHERE JOBID = ?";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_DELETE);
			s.setInt(1, jobID);
			s.executeUpdate();
			c.commit();
		} catch ( Exception e ) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
	
	public boolean rejectInterview(int applicantID, int jobID){
		Connection c = null;
		PreparedStatement s = null;
		boolean result = true;
		String SQL_DELETE = "DELETE FROM INTERVIEWS WHERE APPLICANTID = ? AND JOBID = ?";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_DELETE);
			s.setInt(1, applicantID);
			s.setInt(2, jobID);
			s.executeUpdate();
			c.commit();
		} catch ( Exception e ) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
}

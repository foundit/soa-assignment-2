package au.edu.soacourse.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.dbutils.DbUtils;
import au.edu.soacourse.Constants;
import au.edu.soacourse.model.Application;

public class ApplicationDAOImpl implements ApplicationDAO {

	@Override
	public int createApplication(Application application) throws SQLException {
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		int result = 0;
		String SQL_INSERT = "INSERT INTO APPLICATIONS "
				+ "(JOBID,"
				+ " APPLICANTID,"
				+ " COVERLETTER,"
				+ " STATUS) "
				+ "VALUES (?, ?, ?, ?)";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_INSERT,
					Statement.RETURN_GENERATED_KEYS);
			s.setInt(1, application.getJobID());
			s.setInt(2, application.getApplicantID());
			s.setString(3, application.getCoverLetter());
			s.setString(4, application.getStatus());
			System.out.println(SQL_INSERT);
			int affectedRows = s.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating application failed, no rows affected.");
			}
			r = s.getGeneratedKeys();
			if(r.next()){
				result = r.getInt(1);
			}
			c.commit();
		} catch ( Exception e ) {
			throw new SQLException("Error");
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return result;
	}

	@Override
	public Application getApplication(int applicationID) throws SQLException {
		Application application = new Application();
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		String SQL_SELECT = "SELECT * FROM APPLICATIONS WHERE APPLICATIONID = ? ";
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_SELECT);
			s.setInt(1, applicationID);
			
			r = s.executeQuery();
			if(r.next()) {
				application.setApplicationID(applicationID);
				application.setJobID(r.getInt("JOBID"));
				application.setApplicantID(r.getInt("APPLICANTID"));
				application.setCoverLetter(r.getString("COVERLETTER"));
				application.setStatus(r.getString("STATUS"));
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new SQLException("no application found with specified applicationID"); 
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return application;
	}


	@Override
	public boolean updateApplication(Application application) {
		Connection c = null;
		PreparedStatement s = null;
		Boolean result = true;

		String SQL_UPDATE = "UPDATE APPLICATIONS SET COVERLETTER = ?,"
				+ " STATUS = ? "
				+ "WHERE APPLICANTID = ? AND JOBID = ?";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_UPDATE);
			s.setString(1, application.getCoverLetter());
			s.setString(2, application.getStatus());
			s.setInt(3, application.getApplicantID());
			s.setInt(4, application.getJobID());
			int affectedRows = s.executeUpdate();
			if (affectedRows == 0) {
				result = false;
			}
			c.commit();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return result;
	}

	@Override
	public boolean deleteApplication(int jobID, int applicantID) {
		Connection c = null;
		PreparedStatement s = null;
		String SQL_DELETE = "DELETE FROM APPLICATIONS WHERE JOBID = ? AND APPLICANTID = ?" ;
		Boolean result = true;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_DELETE);
			s.setInt(1, jobID);
			s.setInt(2, applicantID);
			
			int affectedRows = s.executeUpdate();
			if (affectedRows == 0) {
				result = false;
			}
			c.commit();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return result;
	}

	@Override
	public List<Application> getApplications(int jobID) {
		List<Application> applications = new ArrayList<Application>();
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		String SQL_SELECT = "SELECT * FROM APPLICATIONS WHERE JOBID = ? ";
		
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_SELECT);
			
			s.setInt(1, jobID);
			
			r = s.executeQuery();
			while(r.next()) {				
				applications.add(getApplication(r.getInt("APPLICATIONID")));		
			}
		} catch (SQLException | ClassNotFoundException e) {
			return applications; 
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return applications;
	}
	
	public List<String> getAppliedJobID(int appID){
		Connection c = null;
		Statement stmt = null;
		ResultSet r = null;
		List<String> result = new ArrayList<String>();
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "SELECT JOBID FROM APPLICATIONS WHERE APPLICANTID = '" + appID + "'";
			r = stmt.executeQuery(sql);
			while(r.next()){
				result.add(r.getString("JOBID"));
			}
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result=null;
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
	
	public String getCoverLetter(String jobID, String appID){
		Connection c = null;
		Statement stmt = null;
		ResultSet r = null;
		String result = null;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "SELECT COVERLETTER FROM APPLICATIONS WHERE APPLICANTID = '" + appID + "' AND JOBID = '" + jobID + "'";
			r = stmt.executeQuery(sql);
			if(r.next()){
				result = r.getString("COVERLETTER");
			}
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result=null;
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
	
	public String getApplicationStatus(String jobID, String appID){
		Connection c = null;
		Statement stmt = null;
		ResultSet r = null;
		String result = null;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "SELECT STATUS FROM APPLICATIONS WHERE APPLICANTID = '" + appID + "' AND JOBID = '" + jobID + "'";
			r = stmt.executeQuery(sql);
			if(r.next()){
				result = r.getString("STATUS");
			}
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result=null;
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
	
	public boolean updateApplicationStatus(int applicationID, String status) {
		Connection c = null;
		PreparedStatement s = null;
		Boolean result = true;

		String SQL_UPDATE = "UPDATE APPLICATIONS SET STATUS = ? WHERE APPLICATIONID = ?";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_UPDATE);
			s.setString(1, status);
			s.setInt(2, applicationID);
			int affectedRows = s.executeUpdate();
			if (affectedRows == 0) {
				result = false;
			}
			c.commit();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
	
	public boolean updateApplicationStatusByJob(int applicantID, int jobID, String status) {
		Connection c = null;
		PreparedStatement s = null;
		Boolean result = true;

		String SQL_UPDATE = "UPDATE APPLICATIONS SET STATUS = ? WHERE APPLICANTID = ? AND JOBID = ?";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_UPDATE);
			s.setString(1, status);
			s.setInt(2, applicantID);
			s.setInt(3, jobID);
			int affectedRows = s.executeUpdate();
			if (affectedRows == 0) {
				result = false;
			}
			c.commit();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
}

package au.edu.soacourse.dao;

import java.sql.SQLException;

import au.edu.soacourse.model.HiringTeam;
import au.edu.soacourse.model.Reviewer;

public interface ReviewerDAO {
	public int createReviewer(Reviewer reviewer) throws SQLException;
	public Reviewer getReviewer(int reviewerID) throws SQLException;
	public boolean updateReviewers(HiringTeam team);
	public HiringTeam getReviewers(int companyID);
	public boolean deleteReviewer(int reviewerID);
	public int createHiringTeam(HiringTeam team);
}

package au.edu.soacourse.dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.dbutils.DbUtils;
import org.springframework.stereotype.Repository;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import au.edu.soacourse.Constants;
import au.edu.soacourse.model.Job;

@Repository
public class JobDAOImpl implements JobDAO {

	public int createJob(Job job) throws SQLException {

		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		int result = 0;
		String SQL_INSERT = "INSERT INTO JOBS "
				+ "(TITLE,"
				+ " SALARY,"
				+ " DETAILS,"
				+ " POSITION,"
				+ " LOCATION,"
				+ " STATUS,"
				+ " SKILLS,"
				+ " COMPANYID)"
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_INSERT,
					Statement.RETURN_GENERATED_KEYS);
			s.setString(1, job.getTitle());
			s.setString(2, job.getSalary());
			s.setString(3, job.getDetails());
			s.setString(4, job.getTypePos());
			s.setString(5, job.getLocation());
			s.setString(6, "open");
			s.setString(7, job.getSkills());
			s.setInt(8, job.getCompanyID());

			int affectedRows = s.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating job failed, no rows affected.");
			}
			r = s.getGeneratedKeys();
			if(r.next()){
				result = r.getInt(1);
			}
			c.commit();
		} catch ( Exception e ) {
			throw new SQLException();
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return result;
	}

	@Override
	public Job getJob(int jobID) throws SQLException {
		
		Job job = new Job();
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		String SQL_SELECT = "SELECT * FROM JOBS WHERE JOBID = ? ";
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_SELECT);
			s.setInt(1, jobID);
			
			r = s.executeQuery();
			if(r.next()) {
				job.setJobID(jobID);
				job.setTitle(r.getString("TITLE"));
				job.setSalary(r.getString("SALARY"));
				job.setDetails(r.getString("DETAILS"));
				job.setTypePos(r.getString("POSITION"));
				job.setLocation(r.getString("LOCATION"));
				job.setStatus(r.getString("STATUS"));
				job.setSkills(r.getString("SKILLS"));
				job.setCompanyID(r.getInt("COMPANYID"));
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new SQLException("no job found with specified companyID"); 
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return job;
	}
	
	@Override
	public boolean updateJob(Job job) {
		Connection c = null;
		PreparedStatement s = null;
		Boolean result = true;

		String SQL_UPDATE = "UPDATE JOBS SET TITLE = ?,"
				+ " SALARY = ?,"
				+ " DETAILS = ?,"
				+ " POSITION = ?,"
				+ " LOCATION = ?,"
				+ " STATUS = ?,"
				+ " SKILLS = ?,"
				+ " COMPANYID = ?"
				+ " WHERE JOBID = ?";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_UPDATE);
			s.setString(1, job.getTitle());
			s.setString(2, job.getSalary());
			s.setString(3, job.getDetails());
			s.setString(4, job.getTypePos());
			s.setString(5, job.getLocation());
			s.setString(6, job.getStatus());
			s.setString(7, job.getSkills());
			s.setInt(8, job.getCompanyID());
			s.setInt(9, job.getJobID());

			int affectedRows = s.executeUpdate();
			if (affectedRows == 0) {
				result = false;
			}
			c.commit();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return result;	
	}
	
	public boolean updateJobStatus(String status, int jobID) {
		Connection c = null;
		PreparedStatement s = null;
		Boolean result = true;

		String SQL_UPDATE = "UPDATE JOBS SET STATUS = ? WHERE JOBID = ?";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_UPDATE);
			s.setString(1, status);
			s.setInt(2, jobID);

			int affectedRows = s.executeUpdate();
			if (affectedRows == 0) {
				result = false;
			}
			c.commit();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return result;	
	}

	@Override
	public boolean deleteJob(int jobID) {
		Connection c = null;
		PreparedStatement s = null;
		String SQL_DELETE = "DELETE FROM JOBS WHERE JOBID = ?" ;
		Boolean result = true;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_DELETE);
			s.setInt(1, jobID);
			
			int affectedRows = s.executeUpdate();
			if (affectedRows == 0) {
				result = false;
			}
			c.commit();
			c.close();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
	
	@Override
	public List<Job> getJobs(int companyID) {
		
		List<Job> jobs = new ArrayList<Job>();
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		String SQL_SELECT = "SELECT * FROM JOBS WHERE COMPANYID = ? ";
		
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_SELECT);
			s.setInt(1, companyID);
			r = s.executeQuery();
			while(r.next()) {				
				jobs.add(getJob(r.getInt("JOBID")));
			}
		} catch (SQLException | ClassNotFoundException e) {
			jobs = null; 
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return jobs;

	}
	
	public List<Job> searchJobs(String type, String search, String status){
		
		List<Job> jobs = new ArrayList<Job>();
		Connection c = null;
		Statement s = null;
		ResultSet r = null;
		String SQL_SELECT = "";
		
		if (type.equals("skills")) {
			SQL_SELECT = "SELECT * FROM JOBS WHERE (SKILLS LIKE '%" + search + "%')";
		} else if (type.equals("keyword")) {
			SQL_SELECT = "SELECT * FROM JOBS WHERE (TITLE LIKE '%" + search + "%' OR " + 
					"DETAILS LIKE '%" + search + "%' OR " +
					"LOCATION LIKE '%" + search + "%' OR " +
					"SKILLS LIKE '%" + search + "%' OR " +
					"POSITION LIKE '%" + search + "%')";
		}
		if (!status.equals("any")) {
			SQL_SELECT += " AND STATUS = '" + status + "'";
		}
		System.out.println(SQL_SELECT);
		try { 
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.createStatement();
			r = s.executeQuery(SQL_SELECT);
			while(r.next()) {				
				jobs.add(getJob(r.getInt("JOBID")));		
			}
		} catch (SQLException | ClassNotFoundException e) {
			jobs = null; 
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		// now query data services
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
		    builder = builderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) { e.printStackTrace(); }
		Document document = null;
		try {
		   document = builder.parse(new FileInputStream(Constants.JOBALERTS_PATH));
		} catch (SAXException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}
		XPath xPath =  XPathFactory.newInstance().newXPath();
		String expression = "/jobalerts/item[title[contains(text(),'" + search + "')] or description[contains(text(), '" + search.toLowerCase() + "')]]";
		NodeList nodeList;
		try {
			nodeList = (NodeList) xPath.compile(expression).evaluate(document, XPathConstants.NODESET);
			for(int i=0; i<nodeList.getLength(); i++){
				Node item = nodeList.item(i);
				System.out.println(item.getNodeName());
				if(item.getNodeType() == Node.ELEMENT_NODE){
					Element e = (Element) item;
					Job j = new Job();
					j.setTitle(e.getElementsByTagName("title").item(0).getTextContent());
					j.setDetails(e.getElementsByTagName("description").item(0).getTextContent());
					j.setStatus("Job Alert");
					jobs.add(j);
				}
			}
		} catch (XPathExpressionException e) { e.printStackTrace();
		}
		return jobs;
	}
	
	public List<Job> getSavedJobs(List<String> list) {
		List<Job> all = new ArrayList<Job>();
		Connection c = null;
		Statement stmt = null;
		ResultSet r = null;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "SELECT * FROM JOBS WHERE JOBID = '" + list.get(0) + "'";
			for(int i=1; i<list.size(); i++){
				sql += " OR JOBID = '" + list.get(i) + "'";
			}
			r = stmt.executeQuery(sql);
			while(r.next()){
				Job job = new Job();
				job.setJobID(r.getInt("JOBID"));
				job.setTitle(r.getString("TITLE"));
				job.setLocation(r.getString("LOCATION"));
				job.setDetails(r.getString("DETAILS"));
				job.setTypePos(r.getString("POSITION"));
				job.setSalary(r.getString("SALARY"));
				job.setStatus(r.getString("STATUS"));
				job.setSkills(r.getString("SKILLS"));
				job.setCompanyID(r.getInt("COMPANYID"));
				all.add(job);
			}
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			all = null;
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return all;
	}

}

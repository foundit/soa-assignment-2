package au.edu.soacourse.dao;

import java.sql.SQLException;
import au.edu.soacourse.model.Company;

public interface CompanyDAO {
	public int createCompany(Company company) throws SQLException;
	public Company getCompany(int companyID) throws SQLException;
	public boolean updateCompany(Company company);
}

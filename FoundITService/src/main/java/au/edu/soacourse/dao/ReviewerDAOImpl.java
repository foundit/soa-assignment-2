package au.edu.soacourse.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;

import au.edu.soacourse.Constants;
import au.edu.soacourse.model.HiringTeam;
import au.edu.soacourse.model.Review;
import au.edu.soacourse.model.Reviewer;

public class ReviewerDAOImpl implements ReviewerDAO {

	@Override
	public int createReviewer(Reviewer reviewer) throws SQLException {
		Connection c = null;
		PreparedStatement s = null;
		String SQL_INSERT = "INSERT INTO REVIEWERS "
				+ "(USERID,"
				+ " COMPANYID,"
				+ " SKILLS)"
				+ "VALUES (?, ?, ?)";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_INSERT,
					Statement.RETURN_GENERATED_KEYS);
			s.setInt(1, reviewer.getUserID());
			s.setInt(2, reviewer.getCompanyID());
			s.setString(3, reviewer.getSkills());

			int affectedRows = s.executeUpdate();

			if (affectedRows == 0) {
				throw new SQLException("Creating reviewer failed, no rows affected.");
			}

			try (ResultSet generatedKeys = s.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					reviewer.setReviewerID(generatedKeys.getInt(1));
				} else {
					throw new SQLException("Creating reviewer failed, no ID obtained.");
				}
			} finally {
				DbUtils.closeQuietly(s);
				DbUtils.closeQuietly(c);
			}

			c.commit();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		} finally {
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		System.out.println("reviewer created successfully");
		return reviewer.getReviewerID();
	}

	public Reviewer getReviewer(int userID) throws SQLException {
		Reviewer reviewer = new Reviewer();
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		String SQL_SELECT = "SELECT * FROM REVIEWERS WHERE USERID = ? ";
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_SELECT);
			s.setInt(1, userID);
			
			r = s.executeQuery();
			if(r.next()) {
				reviewer.setReviewerID(r.getInt("REVIEWER"));
				reviewer.setUserID(r.getInt("USERID"));
				reviewer.setUsername(r.getString("USERNAME"));
				reviewer.setPassword(r.getString("PASSWORD"));
				reviewer.setCompanyID(r.getInt("COMPANYID"));
				reviewer.setSkills(r.getString("SKILLS"));
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new SQLException("no job found with specified companyID"); 
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return reviewer;
	}

	/*
	@Override
	public boolean updateReviewer(Reviewer reviewer) {
		Connection c = null;
		PreparedStatement s = null;
		Boolean result = true;

		String SQL_UPDATE = "UPDATE REVIEWERS SET USERID = ?,"
				+ " COMPANYID = ?,"
				+ " SKILLS = ?,"
				+ " WHERE REVIEWERID = ?";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_UPDATE);
			s.setInt(1, reviewer.getUserID());
			s.setInt(2, reviewer.getCompanyID());
			s.setString(3, reviewer.getSkills());
			s.setInt(4, reviewer.getReviewerID());
			
			int affectedRows = s.executeUpdate();
			if (affectedRows == 0) {
				result = false;
			}
			c.commit();
			c.close();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return result;	
	}
	*/
	
	public boolean updateReviewers(HiringTeam team){
		Connection c = null;
		PreparedStatement stmt = null;
		Boolean result = true;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			
			String SQL_UPDATE = "UPDATE REVIEWERS SET USERID = ?,"
					+ " USERNAME = ?,"
					+ " PASSWORD = ?,"
					+ " SKILLS = ?"
					+ " WHERE REVIEWER = ?";
			stmt = c.prepareStatement(SQL_UPDATE);
			List<Reviewer> newTeam = team.getTeam();
			for(int j=0; j<newTeam.size(); j++){
				stmt.setInt(1, newTeam.get(j).getUserID());
				stmt.setString(2, newTeam.get(j).getUsername());
				stmt.setString(3, newTeam.get(j).getPassword());
				stmt.setString(4, newTeam.get(j).getSkills());
				stmt.setInt(5, newTeam.get(j).getReviewerID());
				stmt.addBatch();
			}
			stmt.executeBatch();
			c.commit();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return result;
	}

	/*
	@Override
	public List<Reviewer> getReviewers(int companyID) {
		List<Reviewer> reviewers = new ArrayList<Reviewer>();
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		String SQL_SELECT = "SELECT * FROM REVIEWERS WHERE COMPANYID = ? ";
		
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_SELECT);
			
			s.setInt(1, companyID);
			
			r = s.executeQuery();
			if(r.next()) {				
				reviewers.add(getReviewer(r.getInt("REVIEWERID")));		
			}
		} catch (SQLException | ClassNotFoundException e) {
			return reviewers; 
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return reviewers;
	}
	*/
	
	public HiringTeam getReviewers(int companyID) {
		HiringTeam j = new HiringTeam();
		List<Reviewer> all = new ArrayList<Reviewer>();
		Connection c = null;
		Statement stmt = null;
		ResultSet r = null;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "SELECT * FROM REVIEWERS WHERE COMPANYID = '" + companyID + "'";
			r = stmt.executeQuery(sql);
			while(r.next()){
				Reviewer reviewer = new Reviewer();
				reviewer.setReviewerID(r.getInt("REVIEWER"));
				reviewer.setUsername(r.getString("USERNAME"));
				reviewer.setPassword(r.getString("PASSWORD"));
				reviewer.setUserID(r.getInt("USERID"));
				reviewer.setCompanyID(r.getInt("COMPANYID"));
				reviewer.setSkills(r.getString("SKILLS"));
				all.add(reviewer);
			}
			j.setTeam(all);
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			j = null;
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return j;
	}
	
	public boolean deleteReviewer(int reviewerID){
		Connection c = null;
		Statement stmt = null;
		Boolean result = true;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "DELETE FROM REVIEWERS WHERE REVIEWER='" + reviewerID + "'";
			stmt.executeUpdate(sql);
			c.commit();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
	
	public int createHiringTeam(HiringTeam team) {
		int result = 0;
		Connection c = null;
		PreparedStatement stmt = null;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			String sql = "INSERT INTO REVIEWERS (USERID, COMPANYID, USERNAME, PASSWORD, SKILLS) VALUES (?,?,?,?,?)";
			stmt = c.prepareStatement(sql);
			List<Reviewer> newTeam = team.getTeam();
			for(int j=0; j<newTeam.size(); j++){
				if(!newTeam.get(j).getUsername().equals("")){
					stmt.setInt(1, newTeam.get(j).getUserID());
					stmt.setInt(2, newTeam.get(j).getCompanyID());
					stmt.setString(3, newTeam.get(j).getUsername());
					stmt.setString(4, newTeam.get(j).getPassword());
					stmt.setString(5, newTeam.get(j).getSkills());
					stmt.addBatch();
					result = newTeam.get(j).getCompanyID();
				}
			}
			stmt.executeBatch();
			c.commit();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = 0;
		} finally {
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
}

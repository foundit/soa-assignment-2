package au.edu.soacourse.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.springframework.stereotype.Repository;

import au.edu.soacourse.Constants;
import au.edu.soacourse.model.Company;
import au.edu.soacourse.model.Reviewer;
import au.edu.soacourse.model.HiringTeam;

@Repository
public class CompanyDAOImp{

	public int saveCompany(Company company) {
		int i=0;
		Connection c = null;
		Statement stmt = null;
		ResultSet r = null;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "INSERT INTO COMPANIES ('managerID', 'name', 'email', 'description', 'website', 'address', 'industry', 'typecomp', 'size') VALUES ('"
					+ company.getManagerID() + "', '"
					+ company.getName() + "', '"
					+ company.getEmail() + "', '"
					+ company.getDescription() + "', '"
					+ company.getWebsite() + "', '"
					+ company.getAddress() + "', '"
					+ company.getIndustry() + "', '"
					+ company.getTypecomp() + "', '"
					+ company.getSize() + "');";
			stmt.executeUpdate(sql);
			c.commit();	
			// Assume employer only has one company use email as foreign key
			sql = "SELECT COMPANYID FROM COMPANIES WHERE email='" + company.getEmail () + "'";
			r = stmt.executeQuery(sql);
			if(r.next()){
				i = r.getInt("COMPANYID");
			}
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return i;
	}

	public Company getCompany(String id) {
		Company company = new Company();
		Connection c = null;
		Statement stmt = null;
		ResultSet r = null;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "SELECT * FROM COMPANIES WHERE USERID = '" + id + "'";
			r = stmt.executeQuery(sql);
			if(r.next()){
				company.setCompanyID(r.getInt("COMPANYID"));
				company.setManagerID(r.getInt("MANAGERID"));
				company.setAddress(r.getString("ADDRESS"));
				company.setEmail(r.getString("EMAIL"));
				company.setName(r.getString("NAME"));
				company.setDescription(r.getString("DESCRIPTION"));
				company.setWebsite(r.getString("WEBSITE"));
				company.setIndustry(r.getString("INDUSTRY"));
				company.setTypecomp(r.getString("TYPECOMP"));
				company.setSize(r.getString("SIZE"));
			}
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			company = null;
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return company;
	}
	
	public Boolean updateCompany(String id, Company company){
		Connection c = null;
		Statement stmt = null;
		Boolean result = true;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "UPDATE COMPANIES SET " +
					"ADDRESS = '" + company.getAddress() + "'" + 
					",DESCRIPTION = '" + company.getDescription() + "'" + 
					",EMAIL = '" + company.getEmail() + "'" + 
					",INDUSTRY = '" + company.getIndustry() + "'" + 
					",NAME = '" + company.getName() + "'" + 
					",SIZE = '" + company.getSize() + "'" + 
					",TYPECOMP = '" + company.getTypecomp() + "'" + 
					",WEBSITE = '" + company.getWebsite() + "'" + 
					" WHERE COMPANYID = '" + id + "'";
			stmt.executeUpdate(sql);
			c.commit();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
	/*
	@Override
	public int saveHiringTeam(HiringTeam team) {
		int result = 0;
		Connection c = null;
		PreparedStatement stmt = null;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			String sql = "INSERT INTO REVIEWERS (USERID, COMPANYID, USERNAME, PASSWORD, SKILLS) VALUES (?,?,?,?,?)";
			stmt = c.prepareStatement(sql);
			List<Reviewer> newTeam = team.getTeam();
			for(int j=0; j<newTeam.size(); j++){
				if(!newTeam.get(j).getUsername().equals("")){
					stmt.setInt(1, newTeam.get(j).getUserID());
					stmt.setInt(2, newTeam.get(j).getCompanyID());
					stmt.setString(3, newTeam.get(j).getUsername());
					stmt.setString(4, newTeam.get(j).getPassword());
					stmt.setString(5, newTeam.get(j).getSkills());
					stmt.addBatch();
					result = newTeam.get(j).getCompanyID();
				}
			}
			stmt.executeBatch();
			c.commit();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = 0;
		} finally {
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
	*/
	/*
	public HiringTeam getHiringTeam(String companyID) {
		HiringTeam j = new HiringTeam();
		List<Reviewer> all = new ArrayList<Reviewer>();
		Connection c = null;
		Statement stmt = null;
		ResultSet r = null;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "SELECT * FROM REVIEWERS WHERE COMPANYID = '" + companyID + "'";
			r = stmt.executeQuery(sql);
			while(r.next()){
				Reviewer reviewer = new Reviewer();
				reviewer.setUsername(r.getString("USERNAME"));
				reviewer.setPassword(r.getString("PASSWORD"));
				reviewer.setUserID(r.getInt("USERID"));
				reviewer.setCompanyID(r.getInt("COMPANYID"));
				reviewer.setSkills(r.getString("SKILLS"));
				all.add(reviewer);
			}
			j.setTeam(all);
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			j = null;
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return j;
	}
	*/
	/*
	public Boolean updateHiringTeam(String companyID, HiringTeam team){
		Connection c = null;
		PreparedStatement stmt = null;
		Boolean result = true;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			String sql = "UPDATE REVIEWERS SET USERNAME=?,PASSWORD=?,SKILLS=? WHERE COMPANYID='" + companyID + "' and USERID=?";
			stmt = c.prepareStatement(sql);
			List<Reviewer> newTeam = team.getTeam();
			for(int j=0; j<newTeam.size(); j++){
				stmt.setString(1, newTeam.get(j).getUsername());
				stmt.setString(2, newTeam.get(j).getPassword());
				stmt.setString(3, newTeam.get(j).getSkills());
				stmt.setInt(4, newTeam.get(j).getUserID());
				stmt.addBatch();
			}
			stmt.executeBatch();
			c.commit();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
	*/
	/*
	public Boolean deleteHiringMember(String companyID, String username){
		Connection c = null;
		Statement stmt = null;
		Boolean result = true;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "DELETE FROM REVIEWERS WHERE USERNAME='" + username + "' AND COMPANYID='" + companyID + "'";
			stmt.executeUpdate(sql);
			c.commit();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
	*/
	/*
	public Reviewer getReviewer(String userID) {
		Reviewer result = new Reviewer();
		Connection c = null;
		Statement stmt = null;
		ResultSet r = null;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "SELECT * FROM REVIEWERS WHERE USERID = '" + userID + "'";
			r = stmt.executeQuery(sql);
			if(r.next()){
				result.setCompanyID(r.getInt("COMPANYID"));
				result.setPassword(r.getString("PASSWORD"));
				result.setSkills(r.getString("SKILLS"));
				result.setUserID(r.getInt("USERID"));
				result.setUsername(r.getString("USERNAME"));
			}
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = null;
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(stmt);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
	*/
}

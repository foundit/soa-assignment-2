package au.edu.soacourse.dao;

import java.sql.SQLException;
import java.util.List;

import au.edu.soacourse.model.Job;

public interface JobDAO {
	public int createJob(Job job) throws SQLException;
	public Job getJob(int jobID) throws SQLException;
	public boolean updateJob(Job job);
	public boolean updateJobStatus(String status, int jobID);
	public boolean deleteJob(int jobID);
	public List<Job> getJobs(int companyID);
	public List<Job> searchJobs(String type, String search, String status);
	public List<Job> getSavedJobs(List<String> list);
}

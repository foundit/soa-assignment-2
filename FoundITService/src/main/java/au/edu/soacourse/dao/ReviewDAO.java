package au.edu.soacourse.dao;

import java.sql.SQLException;
import java.util.List;

import au.edu.soacourse.model.Review;
import au.edu.soacourse.model.ReviewList;

public interface ReviewDAO {
	public int createReview(Review review) throws SQLException;
	public int[] createReviews(ReviewList reviews) throws SQLException;
	public Review getReview(int reviewID) throws SQLException;
	public boolean updateReview(Review review);
	public List<Review> getReviews(int reviewerID);
	public List<Review> getReviewByApplicationID(int applicationID);
	public boolean confirmIfShortlisted(int applicationID);
	public List<Integer> getInterviewedList(int jobID);
	public boolean createInterviewList(int jobID, int applicantID);
	public boolean deleteInterviewList(int jobID);
	public boolean rejectInterview(int applicantID, int jobID);
}

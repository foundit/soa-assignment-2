package au.edu.soacourse.dao;

import java.sql.SQLException;
import java.util.List;

import au.edu.soacourse.model.Application;

public interface ApplicationDAO {
	public int createApplication(Application application) throws SQLException;
	public Application getApplication(int applicationID) throws SQLException;
	public boolean updateApplication(Application application);
	public boolean deleteApplication(int jobID, int applicantID);
	public List<String> getAppliedJobID(int applicantID);
	public List<Application> getApplications(int applicantID);
	public String getCoverLetter(String jobID, String appID);
	public String getApplicationStatus(String jobID, String appID);
	public boolean updateApplicationStatus(int applicationID, String status);
	public boolean updateApplicationStatusByJob(int applicantID, int jobID, String status);
}

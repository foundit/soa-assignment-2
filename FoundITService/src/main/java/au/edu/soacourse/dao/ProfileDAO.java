package au.edu.soacourse.dao;

import java.sql.SQLException;
import au.edu.soacourse.model.Profile;

public interface ProfileDAO {
	public int createProfile(Profile profile) throws SQLException;
	public Profile getProfile(int profileID) throws SQLException;
	public boolean updateProfile(Profile profile);
}

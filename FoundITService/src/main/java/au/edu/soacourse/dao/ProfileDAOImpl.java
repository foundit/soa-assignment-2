package au.edu.soacourse.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import au.edu.soacourse.Constants;
import org.apache.commons.dbutils.DbUtils;
import org.springframework.stereotype.Repository;
import au.edu.soacourse.model.Profile;

@Repository
public class ProfileDAOImpl implements ProfileDAO{
	
	public int createProfile(Profile profile) throws SQLException {
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		int result = 0;
		String SQL_INSERT = "INSERT INTO PROFILES "
				+ "(USERID,"
				+ " FIRSTNAME,"
				+ " LASTNAME,"
				+ " EMAIL,"
				+ " ADDRESS,"
				+ " POSITION,"
				+ " EDUCATION,"
				+ " EXPERIENCE) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_INSERT,
					Statement.RETURN_GENERATED_KEYS);
			s.setInt(1, profile.getUserID());
			s.setString(2, profile.getFirstName());
			s.setString(3, profile.getLastName());
			s.setString(4, profile.getEmail());
			s.setString(5, profile.getAddress());
			s.setString(6, profile.getCurrentPosition());
			s.setString(7, profile.getEducation());
			s.setString(8, profile.getExperience());

			int affectedRows = s.executeUpdate();

			if (affectedRows == 0) {
				throw new SQLException("Creating job failed, no rows affected.");
			}
			r = s.getGeneratedKeys();
			if(r.next()){
				result = r.getInt(1);
			}
			c.commit();
		} catch ( Exception e ) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		System.out.println(result);
		return result;
	}
	
	public Profile getProfile(int userID) throws SQLException {
		Profile profile = new Profile();
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		String SQL_SELECT = "SELECT * FROM PROFILES WHERE USERID = ? ";
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_SELECT);
			s.setInt(1, userID);
			
			r = s.executeQuery();
			if(r.next()) {
				profile.setProfileID(r.getInt("PROFILEID"));
				profile.setUserID(r.getInt("USERID"));
				profile.setFirstName(r.getString("FIRSTNAME"));
				profile.setLastName(r.getString("LASTNAME"));
				profile.setEmail(r.getString("EMAIL"));
				profile.setAddress(r.getString("ADDRESS"));
				profile.setCurrentPosition(r.getString("POSITION"));
				profile.setEducation(r.getString("EDUCATION"));
				profile.setExperience(r.getString("EXPERIENCE"));
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new SQLException("no job found with specified companyID"); 
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return profile;
	}
	
	public boolean updateProfile(Profile profile) {
		Connection c = null;
		PreparedStatement s = null;
		Boolean result = true;

		String SQL_UPDATE = "UPDATE PROFILES SET "
				+ " FIRSTNAME = ?,"
				+ " LASTNAME = ?,"
				+ " EMAIL = ?,"
				+ " ADDRESS = ?,"
				+ " POSITION = ?,"
				+ " EDUCATION = ?,"
				+ " EXPERIENCE = ?"
				+ " WHERE USERID = ?";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_UPDATE);
			s.setString(1, profile.getFirstName());
			s.setString(2, profile.getLastName());
			s.setString(3, profile.getEmail());
			s.setString(4, profile.getAddress());
			s.setString(5, profile.getCurrentPosition());
			s.setString(6, profile.getEducation());
			s.setString(7, profile.getExperience());
			s.setInt(8, profile.getUserID());

			int affectedRows = s.executeUpdate();
			if (affectedRows == 0) {
				result = false;
			}
			c.commit();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return result;
	}
}

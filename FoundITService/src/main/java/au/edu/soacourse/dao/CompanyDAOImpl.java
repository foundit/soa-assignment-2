package au.edu.soacourse.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.commons.dbutils.DbUtils;
import org.springframework.stereotype.Repository;
import au.edu.soacourse.Constants;
import au.edu.soacourse.model.Company;

@Repository
public class CompanyDAOImpl implements CompanyDAO {

	public int createCompany(Company company) throws SQLException {
		int result = 0;
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		String SQL_INSERT = "INSERT INTO COMPANIES "
				+ "(MANAGERID,"
				+ " NAME,"
				+ " EMAIL,"
				+ " DESCRIPTION,"
				+ " WEBSITE,"
				+ " ADDRESS,"
				+ " INDUSTRY,"
				+ " TYPECOMP,"
				+ " SIZE) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_INSERT,
					Statement.RETURN_GENERATED_KEYS);
			s.setInt(1, company.getManagerID());
			s.setString(2, company.getName());
			s.setString(3, company.getEmail());
			s.setString(4, company.getDescription());
			s.setString(5, company.getWebsite());
			s.setString(6, company.getAddress());
			s.setString(7, company.getIndustry());
			s.setString(8, company.getTypecomp());
			s.setString(9, company.getSize());

			int affectedRows = s.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Creating company failed, no rows affected.");
			}
			r = s.getGeneratedKeys();
			if(r.next()){
				result = r.getInt(1);
			}
			c.commit();
		} catch ( Exception e ) {
			throw new SQLException();
		} finally {
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return result;
	}

	@Override
	public Company getCompany(int userID) throws SQLException {
		
		Company company = new Company();
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		String SQL_SELECT = "SELECT * FROM COMPANIES WHERE MANAGERID = ? ";
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_SELECT);
			s.setInt(1, userID);
			
			r = s.executeQuery();
			if(r.next()) {
				company.setCompanyID(r.getInt("COMPANYID"));
				company.setManagerID(r.getInt("MANAGERID"));
				company.setName(r.getString("NAME"));
				company.setEmail(r.getString("EMAIL"));
				company.setDescription(r.getString("DESCRIPTION"));
				company.setWebsite(r.getString("WEBSITE"));
				company.setAddress(r.getString("ADDRESS"));
				company.setIndustry(r.getString("INDUSTRY"));
				company.setTypecomp(r.getString("TYPECOMP"));
				company.setSize(r.getString("SIZE"));
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new SQLException("no company found with specified companyID"); 
		} finally {
			DbUtils.closeQuietly(r);
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return company;
	}
	
	public boolean updateCompany(Company company) {
		
		Connection c = null;
		PreparedStatement s = null;
		Boolean result = true;
		System.out.println(company.getWebsite());
		String SQL_UPDATE = "UPDATE COMPANIES SET MANAGERID = ?,"
				+ " NAME = ?,"
				+ " EMAIL = ?,"
				+ " DESCRIPTION = ?,"
				+ " WEBSITE = ?,"
				+ " ADDRESS = ?,"
				+ " INDUSTRY = ?,"
				+ " TYPECOMP = ?,"
				+ " SIZE = ? "
				+ " WHERE COMPANYID = ?";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(Constants.DATABASE_URL);
			c.setAutoCommit(false);
			s = c.prepareStatement(SQL_UPDATE);
			s.setInt(1, company.getManagerID());
			s.setString(2, company.getName());
			s.setString(3, company.getEmail());
			s.setString(4, company.getDescription());
			s.setString(5, company.getWebsite());
			s.setString(6, company.getAddress());
			s.setString(7, company.getIndustry());
			s.setString(8, company.getTypecomp());
			s.setString(9, company.getSize());
			s.setInt(10, company.getCompanyID());

			int affectedRows = s.executeUpdate();
			if (affectedRows == 0) {
				result = false;
			}
			c.commit();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			result = false;
		} finally {
			DbUtils.closeQuietly(s);
			DbUtils.closeQuietly(c);
		}
		return result;		
	}
}

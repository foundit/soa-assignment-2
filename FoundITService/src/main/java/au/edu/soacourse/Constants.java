package au.edu.soacourse;

public class Constants {
	public final static String ROOT_DIR = System.getProperty("catalina.home")+"/webapps/ROOT";
	public final static String DATABASE_URL = "jdbc:sqlite:"+ROOT_DIR+"/FoundIT-server.db";
	public final static String JOBALERTS_PATH = ROOT_DIR + "/jobalerts.xml";
}

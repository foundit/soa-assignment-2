package au.edu.soacourse.dataservice;

import java.util.Comparator;

public class FeedMessage implements Comparable<FeedMessage> {

	  private String title;
	  private String description;
	  
	  public FeedMessage(String title, String description) {
		  this.title = title;
		  this.description = description;
	  }
	  
	  public String getTitle() {
	    return title;
	  }

	  public void setTitle(String title) {
	    this.title = title;
	  }

	  public String getDescription() {
	    return description;
	  }

	  public void setDescription(String description) {
	    this.description = description;
	  }

	  @Override
	  public String toString() {
	    return "FeedMessage [title=" + title + ", description=" + description + "]";
	  }
	  
	  public int compareTo(FeedMessage compareFeedMessage) {
						
			return this.title.compareTo(compareFeedMessage.getTitle());	
	  }
		
		public static Comparator<FeedMessage> FeedMessageTitleComparator 
	                          = new Comparator<FeedMessage>() {

		    public int compare(FeedMessage feedMessage1, FeedMessage feedMessage2) {
		    	
		      String feedMessageTitle1 = feedMessage1.getTitle().toUpperCase();
		      String feedMessageTitle2 = feedMessage2.getTitle().toUpperCase();
		      
		      return feedMessageTitle1.compareTo(feedMessageTitle2);
		   		   
		    }

		};

} 


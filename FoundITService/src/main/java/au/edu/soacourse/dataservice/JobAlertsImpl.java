package au.edu.soacourse.dataservice;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

import au.edu.soacourse.Constants;

public class JobAlertsImpl implements JobAlerts {

	public JobAlertsImpl() {
		
	}
	@Override
	public void searchAndEmailResults(String searchKeyword, boolean sorted,
			String email) {
		
		ArrayList<FeedMessage> results = search(searchKeyword);
		if (sorted) {
			Collections.sort(results);
		}
		
		 String copyright = "Copyright hold by FoundIT";
		    String title = "FoundIT Job Alert";
		    String description = "Job Alerts provided by FoundIT";
		    String language = "en";
		    String link = "http://www.FoundIT.com";
		    Calendar cal = new GregorianCalendar();
		    Date creationDate = cal.getTime();
		    SimpleDateFormat date_format = new SimpleDateFormat("EEE', 'dd' 'MMM' 'yyyy' 'HH:mm:ss' 'Z", Locale.US);
		    String pubdate = date_format.format(creationDate);
		    Feed rssFeeder = new Feed(title, link, description, language,
		        copyright, pubdate);
		    
		    for (FeedMessage feed : results) {
			    rssFeeder.getMessages().add(feed);
		    }
		    
		    String fileName =  "/" + searchKeyword + (sorted ? "sorted" : "") + ".rss";
		    String filePath = Constants.ROOT_DIR+fileName;
		    // now write the file
		    RSSFeedWriter writer = new RSSFeedWriter(rssFeeder, filePath);
		    try {
		      writer.write();
		      sendEmail(email, searchKeyword, filePath, results.isEmpty());
		    } catch (Exception e) {
		      e.printStackTrace();
		    }	
	}
	
	private void sendEmail(String recipientEmail, String searchKeyword, String filePath, boolean noResult) {
	      String to = recipientEmail;
	      
	      final String username = "comp932216s1architects@gmail.com";
			final String password = "unswcse2016";

			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");

			Session session = Session.getInstance(props,
			  new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			  });


	      try{
	         MimeMessage message = new MimeMessage(session);

	         message.setFrom(new InternetAddress(username));

	         message.addRecipient(Message.RecipientType.TO,
	                                  new InternetAddress(to));

	         message.setSubject("FoundIT Job Alert RSSFeed");

	         BodyPart messageBodyPart = new MimeBodyPart();

	         String body = noResult ? "Hey, there was no matched result for search keyword: " + searchKeyword : "Hey, attached is your job alert RSS feed for search keyword: " + searchKeyword;
	         messageBodyPart.setText(body);
	         
	         // Create a multipar message
	         Multipart multipart = new MimeMultipart();

	         // Set text message part
	         multipart.addBodyPart(messageBodyPart);

	         if (!noResult) {
	        	  // Part two is attachment
		         messageBodyPart = new MimeBodyPart();
		         DataSource source = new FileDataSource(filePath);
		         messageBodyPart.setDataHandler(new DataHandler(source));
		         messageBodyPart.setFileName("FoundIT_RSS_Feed.rss");
		         multipart.addBodyPart(messageBodyPart);   
	         }
	         // Send the complete message parts
	         message.setContent(multipart );

	         // Send message
	         Transport.send(message);
	         System.out.println("Sent message successfully....");
	      }catch (MessagingException mex) {
	         mex.printStackTrace();
	      }
	}
	
	
	private ArrayList<FeedMessage> search(String searchKeyword) {
		
		ArrayList<FeedMessage> feeds = new ArrayList<FeedMessage>();
		try {	
	         File inputFile = new File(Constants.JOBALERTS_PATH);
	         DocumentBuilderFactory dbFactory 
	            = DocumentBuilderFactory.newInstance();
	         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	         Document doc = dBuilder.parse(inputFile);
	         doc.getDocumentElement().normalize();
	         System.out.println("Root element :" 
	            + doc.getDocumentElement().getNodeName());
	         NodeList nList = doc.getElementsByTagName("item");
	         System.out.println("----------------------------");
	         for (int temp = 0; temp < nList.getLength(); temp++) {
	            Node nNode = nList.item(temp);
	            System.out.println("\nCurrent Element :" 
	               + nNode.getNodeName());
	            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	               Element eElement = (Element) nNode;
	               
	               String title = eElement
	 	                  .getElementsByTagName("title")
		                  .item(0)
		                  .getTextContent();
	               
	               String description = eElement
		 	                  .getElementsByTagName("description")
			                  .item(0)
			                  .getTextContent();
	               
	               if (title.toLowerCase().contains(searchKeyword.toLowerCase()) 
	            		   || description.toLowerCase().contains(searchKeyword.toLowerCase())) {
	            	   
	            	   
	            	   feeds.add(new FeedMessage(title, description));
	               }
	            }
	         }
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
		
		return feeds;
	}
}

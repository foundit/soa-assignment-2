package au.edu.soacourse.dataservice;

public interface JobAlerts {
	void searchAndEmailResults(String searchKeyword, boolean sorted, String email);
}
